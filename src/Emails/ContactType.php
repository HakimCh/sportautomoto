<?php

declare(strict_types=1);

namespace App\Emails;

use App\Services\Mailer\AbstractEmailType;

class ContactType extends AbstractEmailType
{
    /**
     * @var string
     */
    protected $templatePath = 'users/contact';
}
