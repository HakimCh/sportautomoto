<?php

declare(strict_types=1);

namespace App\Emails;

use App\Services\Mailer\AbstractEmailType;

class CustomerType extends AbstractEmailType
{
    /**
     * @var string
     */
    protected $subject = 'Votre insciption';
    /**
     * @var string
     */
    protected $templatePath = 'customers/inscription';
}
