<?php

declare(strict_types=1);

namespace App\Normalizers;

/**
 * Class ArticleNormalizer.
 */
final class ArticleNormalizer extends AbstractPostNormalizer
{
    /**
     * @var array
     */
    protected $formats = [
        'title' => "Toute l'actualité {{category}}",
        'description' => "Retrouvez toute l'actualité {{category}} sur le site sportautomoto.ma à travers des nouveautés, des évenements, de l'occasion, du pratique et de l'insolite.",
    ];
}
