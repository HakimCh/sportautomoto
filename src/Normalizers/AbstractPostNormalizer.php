<?php

declare(strict_types=1);

namespace App\Normalizers;

use HakimCh\SeoBundle\Normalizers\AbstractNormalizer;

/**
 * Class PostNormalizer.
 */
abstract class AbstractPostNormalizer extends AbstractNormalizer
{
    /**
     * @param array  $parameters
     * @param string $key
     *
     * @return string
     */
    public function format(array $parameters, string $key): string
    {
        return $this->printFormat($this->formats[$key], ['category' => $parameters['category']]);
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function normalize(array $parameters): array
    {
        if (\array_key_exists('category', $parameters)) {
            $parameters['category'] = str_replace('-', ' ', ucfirst($parameters['category']));
        } elseif (\array_key_exists('user', $parameters)) {
            $parameters['category'] = 'par '.ucfirst($parameters['user']);
        } else {
            $parameters['category'] = 'Auto et Moto';
        }

        return $parameters;
    }
}
