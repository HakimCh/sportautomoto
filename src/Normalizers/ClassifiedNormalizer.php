<?php

declare(strict_types=1);

namespace App\Normalizers;

use HakimCh\SeoBundle\Normalizers\AbstractNormalizer;

/**
 * Class AnnonceNormalizer.
 */
final class ClassifiedNormalizer extends AbstractNormalizer
{
    /**
     * @var array
     */
    protected $formats = [
        'title' => '{{first}}: annonces achat-vente de véhicules {{second}}',
        'description' => "Trouvez votre {{first}} d'occasion parmi les annonces gratuites de véhicules à vendre {{second}} sur sportautomoto.ma.",
    ];

    /**
     * @param array  $parameters
     * @param string $key
     *
     * @return string
     */
    public function format(array $parameters, string $key): string
    {
        if ('title' == $key) {
            $first = $parameters['category'];
            $second = $parameters['city'];
        } else {
            $first = $parameters['category'];
            $second = $parameters['city'];
        }

        return $this->printFormat($this->formats[$key], compact('first', 'second'));
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function normalize(array $parameters): array
    {
        $parameters['isVehicle'] = true;
        $parameters['current_type'] = '';

        if (\array_key_exists('category', $parameters)) {
            $parameters['current_type'] = substr($parameters['category'], 0, 4);
            $parameters['isVehicle'] = \in_array($parameters['current_type'], ['auto', 'moto']);
            $parameters['category'] = str_replace('-', ' ', ucfirst($parameters['category']));
        } else {
            $parameters['category'] = 'Auto et Moto';
        }
        if (\array_key_exists('city_slug', $parameters)) {
            $parameters['city'] = 'à '.ucfirst($parameters['city_slug']);
        } else {
            $parameters['city'] = 'au Maroc';
        }

        return $parameters;
    }
}
