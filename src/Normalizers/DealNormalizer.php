<?php

declare(strict_types=1);

namespace App\Normalizers;

/**
 * Class ArticleNormalizer.
 */
final class DealNormalizer extends AbstractPostNormalizer
{
    /**
     * @var array
     */
    protected $formats = [
        'title' => 'Deals et Promos {{category}}',
        'description' => 'Retrouvez toute les deals et promos {{category}} sur le site sportautomoto.ma',
    ];
}
