<?php

declare(strict_types=1);

namespace App\Normalizers;

use HakimCh\SeoBundle\Normalizers\AbstractNormalizer;

/**
 * Class CustomerNormalizer.
 */
final class CustomerNormalizer extends AbstractNormalizer
{
    /**
     * @var array
     */
    protected $formats = [
        'title' => 'Annuaire des {{first}} {{second}} au maroc',
        'description' => 'Trouvez votre {{first}} parmi la liste des meilleurs {{second}} sur sportautomoto.ma',
    ];

    /**
     * @param array  $parameters
     * @param string $key
     *
     * @return string
     */
    public function format(array $parameters, string $key): string
    {
        if ('title' == $key) {
            $first = $parameters['customerType'];
            $second = 'Auto et Moto';
        } else {
            $first = $parameters['customerTypeSingular'];
            $second = $parameters['customerType'].' Auto et Moto';
        }
        if (isset($parameters['categoryTitle'])) {
            $second = $parameters['customerType'].' de '.$parameters['categoryTitle'];
        }

        return $this->printFormat($this->formats[$key], compact('first', 'second'));
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function normalize(array $parameters): array
    {
        if (\array_key_exists('customerType', $parameters)) {
            $parameters['customerTypeSingular'] = substr($parameters['customerType'], 0, -1);
        }
        if (\array_key_exists('category', $parameters)) {
            $parameters['categoryTitle'] = ucfirst(str_replace('-', ' ', $parameters['category']));
        }

        return $parameters;
    }
}
