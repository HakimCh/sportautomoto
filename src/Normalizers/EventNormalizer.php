<?php

declare(strict_types=1);

namespace App\Normalizers;

/**
 * Class EventNormalizer.
 */
final class EventNormalizer extends AbstractPostNormalizer
{
    /**
     * @var array
     */
    protected $formats = [
        'title' => 'Liste des évenements {{category}}',
        'description' => "Trouvez votre événement sport {{category}} parmi une grande liste d'événements organiser partout dans le monde.",
    ];
}
