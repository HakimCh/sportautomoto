<?php

declare(strict_types=1);

namespace App\Exceptions;

class UploaderExtensionException extends \Exception
{
    public function __construct(string $message = '', int $code = 0, \Throwable $previous = null)
    {
        parent::__construct(sprintf('Extension not allowed (%s)', $message), $code, $previous);
    }
}
