<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class UploadException extends Exception
{
    // Redéfinissez l'exception ainsi le message n'est pas facultatif
    public function __construct($message = '', $code = 0, Exception $previous = null)
    {
        // traitement personnalisé que vous voulez réaliser ...
        $message = $this->codeToMessage($code);

        // assurez-vous que tout a été assigné proprement
        parent::__construct($message, $code, $previous);
    }

    // chaîne personnalisée représentant l'objet
    public function __toString()
    {
        return __CLASS__.": [{$this->code}]: {$this->message}\n";
    }

    private function codeToMessage($code)
    {
        switch ($code) {
            case UPLOAD_ERR_OK:
                $message = 'There is no error, the file uploaded with success.';
                break;
            case UPLOAD_ERR_INI_SIZE:
                $message = 'The uploaded file exceeds the '.UPLOAD_MAX_SIZE.' directive in php.ini';
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = 'The uploaded file was only partially uploaded';
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = 'No file was uploaded';
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = 'Missing a temporary folder';
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = 'Failed to write file to disk';
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = 'File upload stopped by extension';
                break;
            default:
                $message = 'Unknown upload error';
                break;
        }

        return $message;
    }
}
