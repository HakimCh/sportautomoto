<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Customer;
use App\Entity\CustomerProfessional;
use App\Traits\RepositoryTrait;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CustomerRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    /**
     * @var string
     */
    protected $alias = 'c';
    /**
     * @var string
     */
    protected $entityClass = Customer::class;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, $this->entityClass);
    }

    public function happyBirthDayToday()
    {
        return $this->createQueryBuilder('c')
            ->andWhere("DATE_FORMAT(c.birthAt, '%d%m') = DATE_FORMAT(:curDate, '%d%m')")
            ->setParameter('curDate', new DateTime())
            ->getQuery()->getResult();
    }

    public function findAll()
    {
        return $this->createQueryBuilder($this->alias);
    }

    public function findByCriteria($criteria = [])
    {
        return $this->createCustomerQueryBuilder($criteria);
    }

    public function getQueryBuilderByCriteria($criteria = []): QueryBuilder
    {
        return $this->createCustomerQueryBuilder($criteria);
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     *
     * @return Customer|null
     */
    public function findOneById(int $id)
    {
        return $this->createCustomerQueryBuilder(['id' => $id])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function countGroupedByStatus()
    {
        return $this->createQueryBuilder($this->alias)
            ->select("{$this->alias}.status, COUNT({$this->alias}.id) as nbr")
            ->groupBy("{$this->alias}.status")
            ->getQuery()
            ->getArrayResult();
    }

    public function random(int $maxResults)
    {
        return $this->createCustomerQueryBuilder()
            ->andWhere('c NOT INSTANCE OF :entity')
            ->setParameter('entity', $this->getEntityManager()->getClassMetadata(CustomerProfessional::class))
            ->orderBy('RAND()')
            ->setMaxResults($maxResults)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->setResultCacheLifetime(600)
            ->getResult();
    }

    public function isExist(string $email, ?int $id = null): int
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->select('COUNT(c.id) as nbr')
            ->andWhere('c.email = :email')
            ->setParameter(':email', $email);

        if ($id) {
            $queryBuilder
                ->andWhere('c.id != :id')
                ->setParameter(':id', $id);
        }

        try {
            return $queryBuilder->getQuery()->getSingleScalarResult() > 0;
        } catch (NonUniqueResultException $e) {
            return true;
        }
    }

    protected function createCustomerQueryBuilder(array $criteria = []): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder($this->alias)
            ->leftJoin("{$this->alias}.categories", 'm')->addSelect('m')
            ->andWhere("{$this->alias}.status = :status")->setParameter('status', $criteria['status'] ?? Customer::STATUS_CONFIRMED);

        if (\array_key_exists('category', $criteria)) {
            $queryBuilder->andWhere('m.slug = :categorySlug')->setParameter('categorySlug', $criteria['category']);
        }
        if (\array_key_exists('terms', $criteria)) {
            $queryBuilder = $this->buildRelatedQuery($queryBuilder, $criteria['terms'], "{$this->alias}.title");
        }
        if (\array_key_exists('id', $criteria)) {
            $queryBuilder->andWhere("{$this->alias}.id = :id")->setParameter('id', $criteria['id']);
        }

        return $queryBuilder;
    }
}
