<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PostClassified;

/**
 * @method PostClassified|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostClassified|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostClassified[]    findAll()
 * @method PostClassified[]    getRssFeed()
 * @method PostClassified[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostClassifiedRepository extends PostRepository
{
    protected $alias = 'c';
    protected $entityClass = PostClassified::class;
}
