<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CustomerProfessional;

/**
 * @method CustomerProfessional|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerProfessional|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerProfessional[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerProfessionalRepository extends CustomerRepository
{
    /**
     * @var string
     */
    protected $alias = 'cp';
    /**
     * @var string
     */
    protected $entityClass = CustomerProfessional::class;
}
