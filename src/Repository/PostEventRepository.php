<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PostEvent;
use DateTime;
use Doctrine\ORM\QueryBuilder;
use Exception;

/**
 * @method PostEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostEvent[]    findAll()
 * @method PostEvent[]    getRssFeed()
 * @method PostEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostEventRepository extends PostRepository
{
    protected $alias = 'e';
    protected $entityClass = PostEvent::class;

    public function findByCriteria(array $criteria = []): QueryBuilder
    {
        return $this->createPostQueryBuilder($criteria)
                ->orWhere('e.startAt > :currentDate')
                ->orWhere('e.endAt > :currentDate')
                ->setParameter('currentDate', new DateTime())
                ->orderBy('e.startAt', 'ASC');
    }

    public function nextEvent(): ?PostEvent
    {
        try {
            return $this->createQueryBuilder('e')
                ->orWhere('e.startAt > :currentDate')
                ->orWhere('e.endAt > :currentDate')
                ->setParameter('currentDate', new DateTime())
                ->orderBy('e.startAt', 'ASC')
                ->getQuery()
                ->getSingleResult();
        } catch (Exception $e) {
            return null;
        }
    }
}
