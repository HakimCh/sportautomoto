<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PostDeal;
use DateTime;
use Doctrine\ORM\QueryBuilder;

/**
 * @method PostDeal|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostDeal|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostDeal[]    getRssFeed()
 * @method PostDeal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostDealRepository extends PostRepository
{
    protected $alias = 'd';
    protected $entityClass = PostDeal::class;

    public function findByCriteria(array $criteria = []): QueryBuilder
    {
        $queryBuilder = $this->createPostQueryBuilder($criteria)
            ->leftJoin('d.city', 'c')->addSelect('c')
            ->andWhere('d.endAt >= :currentDate')->setParameter('currentDate', new DateTime())
            ->orWhere('d.endAt IS NULL')
            ->andWhere('d.category is not null');

        if (\array_key_exists('city', $criteria)) {
            $queryBuilder->andWhere('c.slug = :city')->setParameter('city', $criteria['city']);
        }

        return $queryBuilder->orderBy('d.id', 'DESC');
    }
}
