<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CustomerSubscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CustomerSubscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerSubscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerSubscription[]    findAll()
 * @method CustomerSubscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerSubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CustomerSubscription::class);
    }

    public function getQueryBuilder()
    {
        return $this->createQueryBuilder('cs')
            ->leftJoin('cs.customer', 'c')->addSelect('c')
            ->orderBy('cs.createdAt', 'DESC');
    }
}
