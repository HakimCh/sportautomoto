<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PostDealOrders;
use App\Traits\RepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PostDealOrders|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostDealOrders|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostDealOrders[]    findAll()
 * @method PostDealOrders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostDealOrdersRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PostDealOrders::class);
    }

    public function searchByCriteria(array $criteria = []): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('o')
            ->leftJoin('o.deal', 'd')->addSelect('d');

        if (\array_key_exists('dealId', $criteria)) {
            $queryBuilder->andWhere('d.id = :dealId')->setParameter('dealId', $criteria['dealId']);
        }
        if (\array_key_exists('status', $criteria)) {
            $queryBuilder->andWhere('o.status = :status')->setParameter('status', $criteria['status']);
        }
        if (\array_key_exists('terms', $criteria)) {
            $queryBuilder = $this->buildRelatedQuery($queryBuilder, str_replace(' ', '-', $criteria['terms']), 'o.title');
        }

        return $queryBuilder->orderBy('o.id', 'DESC');
    }
}
