<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CustomerStory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CustomerStory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerStory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerStory[]    findAll()
 * @method CustomerStory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerStoriesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CustomerStory::class);
    }

    public function findByCustomerId($id)
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.category', 'm')->addSelect('m')
            ->andWhere('s.customer = :customerId')
            ->setParameter('customerId', $id)
            ->getQuery()
            ->getResult();
    }

    public function unify($newMeta, $oldMetas)
    {
        $this->createQueryBuilder('m')
            ->update('CustomerStoryhp', 'cm')
            ->set('cm.category', $newMeta)
            ->andWhere('cm.category in (:metas)')
            ->setParameter('metas', $oldMetas)
            ->getQuery()
            ->execute();
    }
}
