<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Category;
use App\Entity\CustomerDriver;
use App\Entity\CustomerOfficial;
use App\Entity\CustomerProfessional;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function listWithCount($type)
    {
        return $this->createQueryBuilder('m')
            ->select('m.id', 'm.title', 'm.slug')
            ->leftJoin('m.classifieds', 'c')->addSelect('count(c.id) as classifiedCount')
            ->leftJoin('m.articles', 'a')->addSelect('count(a.id) as articleCount')
            ->leftJoin('m.deals', 'd')->addSelect('count(d.id) as dealCount')
            ->leftJoin('m.events', 'e')->addSelect('count(e.id) as eventCount')
            ->andWhere('m.type = :type')
            ->andWhere('m.slug <> :slug')
            ->setParameter('type', $type)
            ->setParameter('slug', 'autre')
            ->groupBy('m.slug')
            ->getQuery()
            ->getResult();
    }

    public function findByPhrase($phrase, $type = null)
    {
        $queryBuilder = $this->createQueryBuilder('m')
                ->select('m.id', 'm.title', 'm.slug')
                ->andWhere('m.title like :title')
                ->setParameter('title', '%'.$phrase.'%')
                ->andWhere('m.other = :other')
                ->setParameter('other', 1)
                ->andWhere('m.parent > :parent')
                ->setParameter('parent', 0);

        if (null !== $type) {
            $queryBuilder
                ->andWhere('m.type = :type')
                ->setParameter('type', $type);
        }

        return $queryBuilder
                ->getQuery()
                ->getResult();
    }

    public function findCustomersByCategory($category, $type)
    {
        $meta = $this->findOneBy(['slug' => $category, 'type' => $type]);

        if ($customerMetas = $meta->getCustomers()) {
            $data = [];
            foreach ($customerMetas as $customerMeta) {
                $data[] = $customerMeta->getCustomer();
            }

            return $data;
        }

        return null;
    }

    public function findChildsByType(string $type, $isQb = false)
    {
        $queryBuilder = $this->createQueryBuilder('m')
            ->andWhere('m.type = :type')->setParameter('type', $type)
            ->andWhere('m.parent is not null')
            ->orderBy('m.title', 'ASC');

        if ($isQb) {
            return $queryBuilder;
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function findMetasByType(string $type, $isQb = false)
    {
        $queryBuilder = $this->createQueryBuilder('m')
            ->andWhere('m.type = :type')->setParameter('type', $type)
            ->andWhere('m.special = 0')
            ->orderBy('m.title', 'ASC');

        if ($isQb) {
            return $queryBuilder;
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function getParentByType(string $type, $isQb = false)
    {
        $queryBuilder = $this->createQueryBuilder('m')
            ->andWhere('m.type = :type')->setParameter('type', $type)
            ->andWhere('m.parent is null')
            ->orderBy('m.title', 'ASC');

        if ($isQb) {
            return $queryBuilder;
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function clean($ids)
    {
        $this->createQueryBuilder('m')
            ->delete('App:Category', 'm')
            ->andWhere('m.id in (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->execute();
    }

    public function findDuplicatedCategories()
    {
        return $this->createQueryBuilder('c')
            ->select('CONCAT_GROUP(c.id)')
            ->andWhere('c.type IN (:types)')
            ->setParameter('types', [CustomerProfessional::TYPE_LABEL, CustomerOfficial::TYPE_LABEL, CustomerDriver::TYPE_LABEL])
            ->groupBy('c.title', 'c.type')
            ->having('COUNT(c.title) > 1')
            ->getQuery()
            ->getResult();
    }

    public function findByCustomerId($id)
    {
        return $this->createQueryBuilder('cm')
            ->leftJoin('cm.category', 'm')->addSelect('m')
            ->andWhere('cm.customer = :customerId')
            ->setParameter('customerId', $id)
            ->getQuery()
            ->getResult();
    }

    public function findByIdList(array $idList)
    {
        return $this->createQueryBuilder('cm')
            ->andWhere('cm.id IN (:ids)')
            ->setParameter('ids', $idList)
            ->getQuery()
            ->getResult();
    }

    public function unify($newMeta, $oldMetas)
    {
        $this->createQueryBuilder('m')
            ->update('App:CustomerMetas', 'cm')
            ->set('cm.category', $newMeta)
            ->andWhere('cm.category in (:metas)')
            ->setParameter('metas', $oldMetas)
            ->getQuery()
            ->execute();
    }

    public function getQueryMetaByType($type)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.type = :type')
            ->setParameter('type', $type)
            ->orderBy('m.title', 'ASC');
    }

    public function getMetaParents(string $type)
    {
        $parents = $metas = [];
        $metas = $this->findBy(['type' => $type], ['title' => 'ASC']);
        foreach ($metas as $meta) {
            if (null === $meta->getParent()) {
                $parents[$meta->getTitle()] = $meta->getId();
            } else {
                $metas[$meta->getId()] = $meta->getId();
            }
        }

        return [
            'parents' => $parents,
            'metas' => $metas,
        ];
    }
}
