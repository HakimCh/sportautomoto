<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CustomerOfficial;

/**
 * @method CustomerOfficial|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerOfficial|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerOfficial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerOfficialRepository extends CustomerRepository
{
    /**
     * @var string
     */
    protected $alias = 'co';
    /**
     * @var string
     */
    protected $entityClass = CustomerOfficial::class;
}
