<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Post;
use App\Traits\RepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PostRepository extends ServiceEntityRepository implements PostRepositoryInterface
{
    use RepositoryTrait;

    /**
     * @var string
     */
    protected $alias;
    /**
     * @var string
     */
    protected $entityClass;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, $this->entityClass);
    }

    public function getRssFeed()
    {
        return $this->createPostQueryBuilder()
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     *
     * @return Post
     */
    public function findOneById(int $id): Post
    {
        return $this->createPostQueryBuilder(['id' => $id])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByCriteria(array $criteria = []): QueryBuilder
    {
        return $this->createPostQueryBuilder($criteria);
    }

    /**
     * @param string      $fieldName
     * @param string|null $order
     * @param int         $maxResults
     *
     * @return Post[]
     */
    public function findTopItems(string $fieldName = 'id', ?string $order = null, int $maxResults = 5): array
    {
        return $this->createPostQueryBuilder()
            ->orderBy(sprintf('%s.%s', $this->alias, $fieldName), $order ?? 'DESC')
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $phrase
     *
     * @return Post[]
     */
    public function findByPhrase($phrase): array
    {
        return $this->createQueryBuilder($this->alias)
            ->select("{$this->alias}.id as value", "{$this->alias}.title as label")
            ->andWhere("{$this->alias}.title like :title")
            ->setParameter('title', '%'.$phrase.'%')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param string $slug
     * @param int    $id
     * @param int    $maxResults
     *
     * @return Post[]
     */
    public function findRelatedItems(string $slug, int $id, int $maxResults = 3): array
    {
        return $this->createPostQueryBuilder(['related' => [$id, $slug]])
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getResult();
    }

    public function getCountByStatus()
    {
        return $this->createQueryBuilder($this->alias)
            ->select("COUNT({$this->alias}.id) as nbr, {$this->alias}.status")
            ->groupBy("{$this->alias}.status")
            ->getQuery()
            ->getArrayResult();
    }

    protected function createPostQueryBuilder(array $criteria = []): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder($this->alias)
            ->leftJoin("{$this->alias}.user", 'u')->addSelect('u')
            ->leftJoin("{$this->alias}.category", 'm')->addSelect('m')
            ->andWhere("{$this->alias}.status = :status")->setParameter('status', $criteria['status'] ?? Post::STATUS_PUBLISH)
            ->andWhere("{$this->alias}.category is not null");

        if (\array_key_exists('id', $criteria)) {
            $queryBuilder->andWhere("{$this->alias}.id = :id")->setParameter('id', $criteria['id']);
        }
        if (\array_key_exists('category', $criteria)) {
            $queryBuilder->andWhere('m.slug = :categorySlug')->setParameter('categorySlug', $criteria['category']);
        }
        if (\array_key_exists('userId', $criteria)) {
            $queryBuilder->andWhere('u.id = :userId')->setParameter('userId', $criteria['userId']);
        }
        if (\array_key_exists('related', $criteria)) {
            [$id, $slug] = $criteria['related'];
            $queryBuilder = $this->buildRelatedQuery($queryBuilder, $slug, "{$this->alias}.title")
                ->andWhere("{$this->alias}.id <> :id")->setParameter('id', $id);
        }

        return $queryBuilder;
    }
}
