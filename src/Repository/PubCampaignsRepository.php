<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PubCampaigns;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PubCampaigns|null find($id, $lockMode = null, $lockVersion = null)
 * @method PubCampaigns|null findOneBy(array $criteria, array $orderBy = null)
 * @method PubCampaigns[]    findAll()
 * @method PubCampaigns[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PubCampaignsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PubCampaigns::class);
    }

    public function findByCode($code)
    {
        $queryBuilder = $this->createQueryBuilder('pc')
            ->leftJoin('pc.pub', 'p')->addSelect('p')
            ->leftJoin('p.type', 't')->addSelect('t');

        if (\is_array($code)) {
            $queryBuilder->andWhere('p.code IN (:code)');
        } else {
            $queryBuilder->andWhere('p.code = :code');
        }

        return $queryBuilder->setParameter('code', $code)
            ->andWhere('pc.status = :status')->setParameter('status', 'cours')
            ->getQuery()
            ->getResult();
    }

    public function toggleStatus($pubId, $status)
    {
        $this->createQueryBuilder('pc')
            ->update(PubCampaigns::class, 'pc')
            ->set('pc.status', '?1')
            ->where('pc.id = ?2')
            ->setParameter(1, $status)
            ->setParameter(2, $pubId)
            ->getQuery()
            ->execute();
    }
}
