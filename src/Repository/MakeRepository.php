<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Make;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Make|null find($id, $lockMode = null, $lockVersion = null)
 * @method Make|null findOneBy(array $criteria, array $orderBy = null)
 * @method Make[]    findAll()
 * @method Make[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MakeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Make::class);
    }

    /**
     * @param string $phrase
     *
     * @return Make[]
     */
    public function findByPhrase(string $phrase)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.title like :title')
            ->andWhere('b.parent is not null')
            ->setParameter('title', '%'.$phrase.'%')
            ->getQuery()
            ->getResult();
    }

    public function getQueryBuilderByType($criteria = [])
    {
        $queryBuilder = $this->createQueryBuilder('b');
        if (\array_key_exists('type', $criteria)) {
            $queryBuilder
                ->andWhere('b.type = :type')
                ->setParameter(':type', $criteria['type']);
        }
        if (\array_key_exists('parent', $criteria)) {
            $queryBuilder
                ->andWhere('b.parent = :parent')
                ->setParameter(':parent', $criteria['parent']);
        }

        return $queryBuilder->orderBy('b.title', 'ASC');
    }
}
