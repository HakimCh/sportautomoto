<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CustomerDriver;

/**
 * @method CustomerDriver|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerDriver|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerDriver[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerDriverRepository extends CustomerRepository
{
    /**
     * @var string
     */
    protected $alias = 'cd';
    /**
     * @var string
     */
    protected $entityClass = CustomerDriver::class;
}
