<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PubTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PubTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method PubTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method PubTypes[]    findAll()
 * @method PubTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PubTypesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PubTypes::class);
    }

//    /**
//     * @return PubTypes[] Returns an array of PubTypes objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PubTypes
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
