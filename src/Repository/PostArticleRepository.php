<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PostArticle;

/**
 * @method PostArticle|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostArticle|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostArticle[]    findAll()
 * @method PostArticle[]    getRssFeed()
 * @method PostArticle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostArticleRepository extends PostRepository
{
    protected $alias = 'a';
    protected $entityClass = PostArticle::class;
}
