<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Loads the user for the given username.
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @throws NonUniqueResultException
     *
     * @return User|null
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getQueryBuilder()
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.id', 'DESC');
    }

    /**
     * @param string $email
     * @param int    $id
     *
     * @return mixed
     */
    public function isExist(string $email, ?int $id = null)
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->select('COUNT(u.id) as nbr')
            ->andWhere('u.email = :email')
            ->setParameter(':email', $email);

        if ($id) {
            $queryBuilder
                ->andWhere('u.id != :id')
                ->setParameter(':id', $id);
        }

        try {
            return $queryBuilder->getQuery()->getSingleScalarResult() > 0;
        } catch (NonUniqueResultException $e) {
            return true;
        }
    }
}
