<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Post;
use Doctrine\ORM\QueryBuilder;

interface PostRepositoryInterface
{
    /**
     * @param string $slug
     * @param int    $exclude
     * @param int    $maxResults
     *
     * @return Post[]
     */
    public function findRelatedItems(string $slug, int $exclude, int $maxResults = 3): array;

    /**
     * @param string $field
     *
     * @return Post[]
     */
    public function findTopItems(string $field): array;

    /**
     * @param array $criteria
     *
     * @return QueryBuilder
     */
    public function findByCriteria(array $criteria = []): QueryBuilder;

    /**
     * @param int $id
     *
     * @return Post
     */
    public function findOneById(int $id): Post;
}
