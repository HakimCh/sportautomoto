<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Customer;
use App\Entity\CustomerStory;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;
use Exception;

class CustomerStoryFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            SubCategoryFixtures::class,
            CustomerDriverFixture::class,
            CustomerOfficialFixture::class,
            CustomerProfessionalFixture::class,
        ];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_client_history')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new CustomerStory();
        $entity->setId($item['id']);
        $entity->setTitle($item['titre']);
        $entity->setYear($item['annee']);
        $entity->setVictories($item['wins']);
        $entity->setRaking($item['place']);
        $entity->setVehicle($item['machine']);

        try {
            /** @var Category $category */
            $category = $this->getCategoryReference($item['id_meta']);
            $entity->setCategory($category);
        } catch (Exception $e) {
            return null;
        }

        /** @var Customer $client */
        $client = $this->getCustomerReference($item['id_client']);
        $entity->setCustomer($client);

        return $entity;
    }
}
