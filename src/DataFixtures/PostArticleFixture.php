<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\PostArticle;
use Exception;

class PostArticleFixture extends AbstractPostFixture
{
    protected $criteria = ['article'];

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new PostArticle();
        $this->addBaseFields($entity, $item);
        $entity->setTitle($item['post_titre']);
        $entity->setContent($item['post_content']);
        $entity->setExcerpt($item['post_extrait']);
        $entity->setIsSticky($item['stiky']);
        $entity->setStatus(strtoupper($item['post_status']));
        $this->addPictures(['post_photo'], $item, $entity);
        $entity->setCount($item['post_count']);
        $entity->setIsOpenToDiscussion('open' === $item['comment_status']);

        return $entity;
    }
}
