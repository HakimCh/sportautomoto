<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Make;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;

class ModelFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [MakeFixtures::class];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_marques WHERE parent IS NOT NULL AND parent > 0')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new Make();
        $entity->setId($item['id']);
        $entity->setTitle($item['titre']);
        $entity->setVertical($item['type']);

        /** @var Make $make */
        $make = $this->getMakeReference($item['parent']);
        $entity->setParent($make);

        $this->addReference(sprintf('make-%d', $item['id']), $entity);

        return $entity;
    }
}
