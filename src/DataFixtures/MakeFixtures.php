<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Make;
use Doctrine\DBAL\DBALException;

class MakeFixtures extends AbstractFixture
{
    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_marques WHERE parent IS NULL OR parent = 0')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new Make();
        $entity->setId($item['id']);
        $entity->setTitle($item['titre']);
        $entity->setVertical($item['type']);

        $this->addReference(sprintf('make-%d', $item['id']), $entity);

        return $entity;
    }
}
