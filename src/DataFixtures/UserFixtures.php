<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;
use Exception;

class UserFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [CategoryFixtures::class, SubCategoryFixtures::class];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query(
                'SELECT u.id, u.id_role, ui.civilite, ui.prenom, ui.nom, u.email, u.password, 
                u.activated, ui.bio, ui.tel, ui.bday,u.connected_at, u.created_at, u.updated_at 
                FROM sa_users u 
                LEFT JOIN sa_users_infos ui ON ui.id_user = u.id'
            )
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new User();
        $entity->setId($item['id']);
        $entity->setFirstName($item['nom']);
        $entity->setLastName($item['prenom']);
        $entity->setEmail($item['email']);
        $entity->setPassword($item['password']);
        $entity->setCivility($item['civilite']);
        $entity->setHeadline($item['bio']);
        $entity->setIsActive($item['activated']);
        $entity->setBirthAt($this->getDateTime($item['bday']));
        $entity->setCreatedAt($this->getDateTime($item['created_at']));
        $entity->setUpdatedAt($this->getDateTime($item['updated_at']));

        $this->addRoles($entity, $item['id_role']);
        $this->addCategories($entity, $item['id']);

        $this->addReference(sprintf('user-%d', $item['id']), $entity);

        return $entity;
    }

    /**
     * @param User $entity
     * @param int  $userId
     *
     * @throws DBALException
     */
    private function addCategories(User $entity, int $userId): void
    {
        $statement = $this->legacy->prepare('SELECT id_meta FROM sa_post_perms WHERE id_user = ?');
        $statement->execute([$userId]);

        foreach ($statement->fetchAll() as $userCategory) {
            /** @var Category $category */
            $category = $this->getCategoryReference($userCategory['id_meta']);
            $entity->addCategory($category);
        }
    }

    /**
     * @param User $entity
     * @param int  $roleId
     */
    private function addRoles(User $entity, int $roleId): void
    {
        $roles = ['ROLE_USER'];
        switch ($roleId) {
            case 1:
                $roles[] = 'ROLE_SUPER_ADMIN';
                break;
            case 2:
                $roles[] = 'ROLE_ADMIN';
                break;
            case 3:
                $roles[] = 'ROLE_WRITER';
        }

        $entity->setRoles($roles);
    }
}
