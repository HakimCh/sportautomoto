<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\CustomerSubscription;
use App\Entity\Plan;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;
use Exception;

class CustomerSubscriptionsFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * @var bool
     */
    protected $autoincrement = true;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            SubCategoryFixtures::class,
            CustomerDriverFixture::class,
            CustomerOfficialFixture::class,
            CustomerProfessionalFixture::class,
        ];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query(
                'SELECT s.id_client, s.id_offer, b.montant, b.code, b.status, b.payed_at, b.created_at
                FROM sa_billing AS b
                LEFT JOIN sa_subscriptions AS s ON s.id_client = b.id_client'
            )
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new CustomerSubscription();
        $entity->setCode($item['code']);
        $entity->setStatus(strtoupper($item['status']));
        $entity->setAmount($item['montant']);
        $entity->setPaidAt($this->getDateTime($item['payed_at'] ?? $item['created_at']));
        $entity->setCreatedAt($this->getDateTime($item['created_at']));

        try {
            /** @var Customer $customer */
            $customer = $this->getCustomerReference($item['id_client']);
            $entity->setCustomer($customer);
        } catch (Exception $e) {
            return null;
        }

        /** @var Plan $plan */
        $plan = $this->getPlanReference($item['id_offer']);
        $entity->setPlan($plan);

        return $entity;
    }
}
