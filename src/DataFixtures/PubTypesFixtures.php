<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\PubTypes;
use Doctrine\DBAL\DBALException;

class PubTypesFixtures extends AbstractFixture
{
    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_pub_types')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new PubTypes();
        $entity->setId($item['id']);
        $entity->setTitle($item['title']);
        $entity->setSlug($item['slug']);
        $entity->setContent($item['content']);
        $entity->setWidth($item['width']);
        $entity->setHeight($item['height']);

        $this->addReference(sprintf('pubType-%d', $item['id']), $entity);

        return $entity;
    }
}
