<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Post;
use App\Entity\PostDeal;
use Doctrine\DBAL\DBALException;
use Exception;

class PostDealFixture extends AbstractPostFixture
{
    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return mixed[]
     */
    public function getItems(array $criteria = []): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_deal')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new PostDeal();
        $entity->setTitle($item['title']);
        $entity->setContent($item['content']);
        $entity->setPrice($item['price']);
        $entity->setDiscount($item['remise']);
        $entity->setRequirement($item['min'], $item['max']);
        $entity->setAddress($item['adresse']);
        $entity->setStartAt($this->getDateTime($item['start_at']));
        $entity->setEndAt($this->getDateTime($item['end_at']));

        $this->addBaseFields($entity, $item);
        $this->addStatus($entity, $item['status'], $item['archived']);
        $this->addPictures(['photo1', 'photo2', 'photo3', 'photo4'], $item, $entity);

        /** @var City $city */
        $city = $this->getCityReference($item['id_ville']);
        $entity->setCity($city);

        return $entity;
    }

    /**
     * @param PostDeal $entity
     * @param string   $status
     * @param bool     $archived
     */
    private function addStatus(PostDeal $entity, string $status, bool $archived): void
    {
        $status = 'confirmed' === $status ? Post::STATUS_PUBLISH : Post::STATUS_DRAFT;
        if ($archived) {
            $status = Post::STATUS_ARCHIVE;
        }

        $entity->setStatus($status);
    }
}
