<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\CustomerDriver;
use App\Entity\CustomerOfficial;
use App\Entity\CustomerProfessional;
use App\Entity\PostArticle;
use App\Entity\PostClassified;
use App\Entity\PostDeal;
use App\Entity\PostEvent;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;

class SubCategoryFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [CategoryFixtures::class];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT id, titre, type, parent FROM sa_metas WHERE parent IS NOT NULL and parent > 0')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new Category();
        $entity->setId($item['id']);
        $entity->setTitle($item['titre']);
        $entity->setType($this->getTypeLabel($item['type']));

        /** @var Category $category */
        $category = $this->getReference(sprintf(
            'category-%d',
            $item['parent']
        ));
        $entity->setParent($category);

        $this->addReference(sprintf('category-%d', $item['id']), $entity);

        return $entity;
    }

    /**
     * @param string $key
     *
     * @return string
     */
    private function getTypeLabel(string $key): string
    {
        $labels = [
            'pilote' => CustomerDriver::TYPE_LABEL,
            'officiel' => CustomerOfficial::TYPE_LABEL,
            'professionel' => CustomerProfessional::TYPE_LABEL,
            'annonce' => PostClassified::TYPE_LABEL,
            'post' => PostArticle::TYPE_LABEL,
            'event' => PostEvent::TYPE_LABEL,
            'deal' => PostDeal::TYPE_LABEL,
        ];

        if (\array_key_exists($key, $labels)) {
            return $labels[$key];
        }

        return 'general';
    }
}
