<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\CustomerProfessional;
use Doctrine\DBAL\DBALException;

class CustomerProfessionalFixture extends AbstractCustomerFixture
{
    /**
     * @var array
     */
    protected $criteria = ['professionel'];

    /**
     * @param array $item
     *
     * @throws DBALException
     *
     * @return mixed
     */
    public function getEntity(array $item)
    {
        $entity = new CustomerProfessional();
        $this->addBaseFields($entity, $item);
        $entity->setFirstName($item['titre']);
        $entity->setAddress($item['adresse']);
        $this->addCoords($entity, $item['lat'], $item['lng']);

        /** @var City|null $city */
        $city = $item['id_ville'] > 0 ? $this->getCityReference($item['id_ville']) : null;
        $entity->setCity($city);

        return $entity;
    }

    /**
     * @param CustomerProfessional $entity
     * @param float|null           $latitude
     * @param float|null           $longitude
     */
    private function addCoords(CustomerProfessional $entity, $latitude, $longitude)
    {
        if (!$latitude || !$longitude) {
            return;
        }

        $entity->setCoords(sprintf('%s,%s', $latitude, $longitude));
    }
}
