<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\City;
use Doctrine\DBAL\DBALException;

class CityFixtures extends AbstractFixture
{
    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT id, titre FROM sa_villes')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new City();
        $entity->setId($item['id']);
        $entity->setTitle($item['titre']);

        $this->addReference(sprintf('city-%d', $item['id']), $entity);

        return $entity;
    }
}
