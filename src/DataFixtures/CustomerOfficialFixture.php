<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CustomerOfficial;
use Doctrine\DBAL\DBALException;

class CustomerOfficialFixture extends AbstractCustomerFixture
{
    /**
     * @var array
     */
    protected $criteria = ['officiel'];

    /**
     * @param array $item
     *
     * @throws DBALException
     *
     * @return mixed
     */
    public function getEntity(array $item)
    {
        $entity = new CustomerOfficial();
        $this->addBaseFields($entity, $item);
        $entity->setFirstName($item['prenom']);
        $entity->setLastName($item['nom']);
        $entity->setClub($item['club']);
        $entity->setJob($item['job']);

        return $entity;
    }
}
