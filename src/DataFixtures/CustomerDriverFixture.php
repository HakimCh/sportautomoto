<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CustomerDriver;
use Doctrine\DBAL\DBALException;

class CustomerDriverFixture extends AbstractCustomerFixture
{
    /**
     * @var array
     */
    protected $criteria = ['pilote'];

    /**
     * @param array $item
     *
     * @throws DBALException
     *
     * @return mixed
     */
    public function getEntity(array $item)
    {
        $entity = new CustomerDriver();
        $this->addBaseFields($entity, $item);
        $entity->setFirstName($item['prenom']);
        $entity->setLastName($item['nom']);
        $entity->setVehicle($item['machine']);
        $entity->setClub($item['club']);
        $entity->setJob($item['job']);

        return $entity;
    }
}
