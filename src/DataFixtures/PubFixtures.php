<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Pub;
use App\Entity\PubTypes;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;

class PubFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [PubTypesFixtures::class];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT id, title, code, page, content, id_type FROM sa_pub')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new Pub();
        $entity->setId($item['id']);
        $entity->setTitle($item['title']);
        $entity->setContent($item['content']);
        $entity->setCode($item['code']);
        $entity->setPage($item['page']);

        /** @var PubTypes $pubType */
        $pubType = $this->getPubTypeReference($item['id_type']);
        $entity->setType($pubType);

        $this->addReference(sprintf('pub-%d', $item['id']), $entity);

        return $entity;
    }
}
