<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Customer;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;
use Exception;

abstract class AbstractCustomerFixture extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [CategoryFixtures::class, SubCategoryFixtures::class, UserFixtures::class];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array|mixed[]
     */
    public function getItems(array $criteria = []): array
    {
        $statement = $this->legacy
            ->prepare(
                'SELECT c.id,ci.nom,c.titre,ci.prenom,c.content,ci.email,ci.portable,ci.fixe,ci.club,ci.machine,
                ci.facebook,c.status,ci.bday,c.created_at,c.updated_at,ci.siteweb,ci.job,ci.adresse,ci.lat,ci.lng,c.type, ci.id_ville
                FROM sa_client c
                left join sa_client_infos ci on ci.id_client = c.id
                WHERE c.type = ?'
            );
        $statement->execute($criteria);

        return $statement->fetchAll();
    }

    /**
     * @param Customer $entity
     * @param array    $item
     *
     * @throws DBALException
     * @throws Exception
     */
    protected function addBaseFields(Customer $entity, array $item): void
    {
        $entity->setId($item['id']);
        $entity->setContent($item['content']);
        $entity->setStatus(strtoupper($item['status']));
        $entity->setEmail($item['email']);
        $entity->setWebsite($item['siteweb']);
        $entity->setFacebook($item['facebook']);
        $entity->setBirthAt($this->getDateTime($item['bday']));
        $entity->setCreatedAt($this->getDateTime($item['created_at']));

        $this->addPictures($entity);
        $this->addCategories($entity);
        $this->addPhones([$item['portable'], $item['fixe']]);

        $this->addReference(sprintf('customer-%d', $item['id']), $entity);
    }

    /**
     * @param Customer $entity
     *
     * @throws DBALException
     */
    private function addPictures(Customer $entity): void
    {
        $statement = $this->legacy->prepare('SELECT type, file FROM sa_client_photos WHERE id_client = ?');
        $statement->execute([$entity->getId()]);
        $pictures = $statement->fetchAll();

        foreach ($pictures as $picture) {
            $key = 0 === $picture['type'] ? 'profile' : 'picture'.$picture['type'];
            $entity->addPicture($key, $picture['file']);
        }
    }

    /**
     * @param Customer $entity
     *
     * @throws DBALException
     */
    private function addCategories(Customer $entity): void
    {
        $statement = $this->legacy->prepare('SELECT id_meta FROM sa_client_metas WHERE id_client = ?');
        $statement->execute([$entity->getId()]);
        $customerCategories = $statement->fetchAll();

        foreach ($customerCategories as $customerCategory) {
            /** @var Category $category */
            $category = $this->getCategoryReference($customerCategory['id_meta']);
            $entity->addCategory($category);
        }
    }

    /**
     * @param array $phones
     *
     * @return array
     */
    private function addPhones(array $phones): array
    {
        return array_filter($phones, function ($phone) {
            return $phone && $phone > 0;
        });
    }
}
