<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\CustomerDriver;
use App\Entity\CustomerOfficial;
use App\Entity\CustomerProfessional;
use App\Entity\PostArticle;
use App\Entity\PostClassified;
use App\Entity\PostDeal;
use App\Entity\PostEvent;
use Doctrine\DBAL\DBALException;

class CategoryFixtures extends AbstractFixture
{
    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    public function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT id, titre, type FROM sa_metas WHERE parent IS NULL OR parent = 0')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    public function getEntity(array $item)
    {
        $entity = new Category();
        $entity->setId($item['id']);
        $entity->setTitle($item['titre']);
        $entity->setType($this->getTypeLabel($item['type']));

        $this->addReference(sprintf('category-%d', $item['id']), $entity);

        return $entity;
    }

    /**
     * @param string $key
     *
     * @return string
     */
    private function getTypeLabel(string $key): string
    {
        $labels = [
            'pilote' => CustomerDriver::TYPE_LABEL,
            'officiel' => CustomerOfficial::TYPE_LABEL,
            'professionel' => CustomerProfessional::TYPE_LABEL,
            'annonce' => PostClassified::TYPE_LABEL,
            'post' => PostArticle::TYPE_LABEL,
            'event' => PostEvent::TYPE_LABEL,
            'deal' => PostDeal::TYPE_LABEL,
        ];

        if (\array_key_exists($key, $labels)) {
            return $labels[$key];
        }

        return 'general';
    }
}
