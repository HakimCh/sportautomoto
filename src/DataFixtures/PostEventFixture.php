<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\PostEvent;
use Exception;

class PostEventFixture extends AbstractPostFixture
{
    protected $criteria = ['event'];

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new PostEvent();
        $entity->setTitle($item['post_titre']);
        $entity->setContent($item['post_content']);
        $entity->setEndAt($this->getDateTime($item['end_at']));
        $entity->setStartAt($this->getDateTime($item['start_at']));
        $entity->setStatus(strtoupper($item['post_status']));
        $entity->setCount($item['post_count']);
        $this->addBaseFields($entity, $item);
        $this->addPictures(['post_photo'], $item, $entity);
        $entity->setIsOpenToDiscussion('open' === $item['comment_status']);

        return $entity;
    }
}
