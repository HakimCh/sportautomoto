<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\PostDealOrders;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;
use Exception;

class PostDealOrdersFixture extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [UserFixtures::class, PostDealFixture::class];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_deal_orders')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new PostDealOrders();
        $entity->setId($item['id']);
        $entity->setCode($item['code']);
        $entity->setStatus(strtoupper($item['status']));
        $entity->setCreatedAt($this->getDateTime($item['created_at']));

        try {
            /** @var User $deal */
            $deal = $this->getDealReference($item['id_deal']);
            $entity->setDeal($deal);
        } catch (Exception $e) {
            return null;
        }

        /** @var User $user */
        $user = $this->getUserReference($item['id_user']);
        $entity->setUser($user);

        return $entity;
    }
}
