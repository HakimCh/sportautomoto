<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;
use Exception;

abstract class AbstractPostFixture extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * @var bool
     */
    protected $autoincrement = true;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [CategoryFixtures::class, SubCategoryFixtures::class, UserFixtures::class];
    }

    /**
     * @param Post  $entity
     * @param array $item
     *
     * @throws Exception
     */
    public function addBaseFields(Post $entity, array $item): void
    {
        $entity->setCreatedAt($this->getDateTime($item['created_at']));

        /** @var Category $category */
        $category = $this->getCategoryReference($item['id_meta'] > 0 ? $item['id_meta'] : 1);
        $entity->setCategory($category);

        /** @var User $user */
        $user = $this->getUserReference($item['id_user']);
        $entity->setUser($user);
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    public function getItems(array $criteria): array
    {
        $statement = $this->legacy->prepare('SELECT * FROM sa_post WHERE post_type = ?');
        $statement->execute($criteria);

        return $statement->fetchAll();
    }

    protected function addPictures(array $keys, array $item, Post $entity): void
    {
        if (empty($keys)) {
            return;
        }
        foreach ($keys as $key) {
            if (!\array_key_exists($key, $item) || empty($item[$key]) || !$item[$key]) {
                continue;
            }
            $entity->addPicture(
                'picture'.substr($key, -1, 1),
                $item[$key]
            );
        }
    }
}
