<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CustomerDriver;
use App\Entity\CustomerOfficial;
use App\Entity\CustomerProfessional;
use App\Entity\Plan;
use Doctrine\DBAL\DBALException;
use Exception;

class PlanFixtures extends AbstractFixture
{
    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_offers')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new Plan();
        $entity->setId($item['id']);
        $entity->setTitle($item['titre']);
        $entity->setContent($item['description']);
        $entity->setPrice($item['price']);
        $entity->setType($this->getTypeByLabel($item['type']));
        $entity->setCreatedAt($this->getDateTime($item['created_at']));

        $this->addReference(sprintf('plan-%d', $item['id']), $entity);

        return $entity;
    }

    /**
     * @param string $label
     *
     * @return string
     */
    private function getTypeByLabel(string $label): string
    {
        switch ($label) {
            case 'pilote':
                return CustomerDriver::TYPE_LABEL;
                break;
            case 'officiel':
                return CustomerOfficial::TYPE_LABEL;
                break;
            default:
                return CustomerProfessional::TYPE_LABEL;
        }
    }
}
