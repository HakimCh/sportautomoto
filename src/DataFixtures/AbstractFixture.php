<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\City;
use App\Entity\Customer;
use App\Entity\Make;
use App\Entity\Plan;
use App\Entity\PostDeal;
use App\Entity\Pub;
use App\Entity\PubTypes;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\Mapping\ClassMetadata;
use Exception;

/**
 * @method User     getUserReference(int $id)
 * @method Category getCategoryReference(int $id)
 * @method City     getCityReference(int $id)
 * @method PostDeal getDealReference(int $id)
 * @method PubTypes getPubTypeReference(int $id)
 * @method Pub      getPubReference(int $id)
 * @method Make     getMakeReference(int $id)
 * @method Customer getCustomerReference(int $id)
 * @method Plan     getPlanReference(int $id)
 */
abstract class AbstractFixture extends Fixture
{
    /**
     * @var int
     */
    protected $batchSize = 100;
    /**
     * @var bool
     */
    protected $autoincrement = false;
    /**
     * @var array
     */
    protected $criteria = [];
    /**
     * @var Connection
     */
    protected $legacy;

    public function __construct(Connection $legacy)
    {
        $this->legacy = $legacy;
    }

    /**
     * @param string $name
     * @param array  $arguments
     *
     * @throws Exception
     *
     * @return object
     */
    public function __call($name, $arguments)
    {
        if (!preg_match('/^get([a-zA-Z]+)Reference$/', $name, $match)) {
            throw new Exception('Fatal Error: %s() not found');
        }
        if (empty($arguments)) {
            throw new Exception(sprintf('Fatal Error: %s() excepts an integer parameter, 0 parameter given', $name));
        }
        if (!is_numeric($arguments[0])) {
            throw new Exception(sprintf(
                'Fatal Error: %s() excepts parameter 1 to be an integer, %s given',
                $name,
                \gettype($arguments[0])
            ));
        }
        if (!is_numeric($arguments[0]) || $arguments[0] <= 0) {
            throw new Exception(sprintf(
                'Fatal Error: %s() excepts an integer parameter greater than 0, %s given',
                $name,
                $arguments[0]
            ));
        }

        return $this->getReference(sprintf('%s-%d', lcfirst($match[1]), $arguments[0]));
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->persist($manager);
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    abstract protected function getItems(array $criteria): array;

    /**
     * @param array $item
     *
     * @return mixed
     */
    abstract protected function getEntity(array $item);

    /**
     * @param ObjectManager $manager
     *
     * @throws DBALException
     */
    protected function persist(ObjectManager $manager): void
    {
        $items = $this->getItems($this->criteria);
        if (!is_iterable($items) || empty($items)) {
            return;
        }
        foreach ($items as $key => $item) {
            $entity = \call_user_func([$this, 'getEntity'], $item);
            if (!$entity) {
                continue;
            }
            if (!$this->autoincrement) {
                $metadata = $manager->getClassMetadata(\get_class($entity));
                $metadata->setIdGenerator(new AssignedGenerator());
                $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
            }
            $manager->persist($entity);
            if (0 === ($key % $this->batchSize)) {
                $manager->flush();
                $manager->clear(); // Detaches all objects from Doctrine!
            }
        }
        $manager->flush(); // Persist objects that did not make up an entire batch
        $manager->clear();
    }

    /**
     * @param string|null $stringDate
     *
     * @throws Exception
     *
     * @return DateTime|null
     */
    protected function getDateTime(?string $stringDate)
    {
        if ('0000-00-00 00:00:00' === $stringDate) {
            return new DateTime();
        }
        if (!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])(.*)$/', $stringDate)) {
            return null;
        }
        try {
            return new DateTime($stringDate);
        } catch (Exception $e) {
            return null;
        }
    }
}
