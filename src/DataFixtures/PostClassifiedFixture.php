<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Make;
use App\Entity\Post;
use App\Entity\PostClassified;
use Doctrine\DBAL\DBALException;
use Exception;

class PostClassifiedFixture extends AbstractPostFixture
{
    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return mixed[]
     */
    public function getItems(array $criteria = []): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_annonce')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new PostClassified();
        $this->addBaseFields($entity, $item);
        $entity->setTitle($item['titre']);
        $entity->setContent($item['content']);
        $entity->setPrice($item['price']);
        $entity->setMileage($item['driven']);
        $entity->setFirstCirculationYear($item['started']);
        $entity->setCount($item['count']);

        $this->addPictures(['photo1', 'photo2', 'photo3', 'photo4'], $item, $entity);
        $this->addStatus($entity, $item['status']);
        $this->addEnergy($entity, $item['carburant']);

        if ($item['id_model'] > 0) {
            /** @var Make $model */
            $model = $this->getMakeReference($item['id_model']);
            $entity->setModel($model);
        }

        /** @var City $city */
        $city = $this->getCityReference($item['id_ville']);
        $entity->setCity($city);

        return $entity;
    }

    /**
     * @param PostClassified $entity
     * @param string         $energy
     */
    private function addEnergy(PostClassified $entity, string $energy): void
    {
        switch ($energy) {
            case 'Essence':
                $energy = 'GASOLINE';
                break;
            default:
                $energy = 'DIESEL';
        }

        $entity->setEnergy($energy);
    }

    /**
     * @param PostClassified $entity
     * @param string         $status
     */
    private function addStatus(PostClassified $entity, string $status): void
    {
        switch ($status) {
            case 'confirmed':
                $status = Post::STATUS_PUBLISH;
                break;
            default:
                $status = Post::STATUS_DRAFT;
        }

        $entity->setStatus($status);
    }
}
