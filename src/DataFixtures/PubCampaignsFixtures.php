<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Pub;
use App\Entity\PubCampaigns;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\DBAL\DBALException;

class PubCampaignsFixtures extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [PubTypesFixtures::class, PubFixtures::class];
    }

    /**
     * @param array $criteria
     *
     * @throws DBALException
     *
     * @return array
     */
    protected function getItems(array $criteria): array
    {
        return $this->legacy
            ->query('SELECT * FROM sa_pub_campaigns')
            ->fetchAll();
    }

    /**
     * @param array $item
     *
     * @return mixed
     */
    protected function getEntity(array $item)
    {
        $entity = new PubCampaigns();
        $entity->setId($item['id']);
        $entity->setTitle($item['title']);
        $entity->setLink($item['link']);
        $entity->setType($item['type']);
        $entity->setUid($item['id_type']);

        $entity->addPicture('picture1', $item['image']);
        $this->addStatus($entity, $item['image']);

        /** @var Pub $pub */
        $pub = $item['id_pub'] > 0 ? $this->getPubReference($item['id_pub']) : null;
        $entity->setPub($pub);

        /** @var User $user */
        $user = $this->getUserReference($item['id_user']);
        $entity->setUser($user);

        return $entity;
    }

    /**
     * @param PubCampaigns $entity
     * @param string       $status
     */
    private function addStatus(PubCampaigns $entity, string $status): void
    {
        switch ($status) {
            case 'pause':
                $status = PubCampaigns::STATUS_PENDING;
                break;
            default:
                $status = PubCampaigns::STATUS_ONGOING;
        }

        $entity->setStatus($status);
    }
}
