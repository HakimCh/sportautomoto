<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\EntityInterface;
use App\Entity\User;
use App\Exceptions\UploaderExtensionException;
use App\Form\DeleteType;
use App\Traits\PicturesTrait;
use Closure;
use Doctrine\ORM\EntityManagerInterface;
use HakimCh\Helpers\Http\Uploader;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Templating\EngineInterface;

class Crud
{
    use PicturesTrait;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var string
     */
    protected $classLabel;
    /**
     * @var ParameterBag
     */
    protected $parameters;
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    /**
     * @var Uploader
     */
    private $uploader;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EngineInterface
     */
    private $engine;
    /**
     * @var FormInterface
     */
    private $form;
    /**
     * @var EntityInterface
     */
    private $entity;
    /**
     * @var Closure
     */
    private $preFlush;
    /**
     * @var Closure
     */
    private $postFlush;
    /**
     * @var Closure
     */
    private $preSubmit;
    /**
     * @var User
     */
    private $user;
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(EngineInterface $engine, SessionInterface $session, Uploader $uploader, EntityManagerInterface $em, FormFactoryInterface $formFactory)
    {
        $defaultParameters = [];
        if (isset($this->classLabel)) {
            $defaultParameters = [
                'label' => $this->classLabel,
            ];
        }
        $this->parameters = new ParameterBag($defaultParameters);
        $this->formFactory = $formFactory;
        $this->uploader = $uploader;
        $this->session = $session;
        $this->engine = $engine;
        $this->em = $em;
    }

    public function setEntity($instance, ?string $label = null): self
    {
        $this->entity = $instance;
        if ($label) {
            $this->classLabel = $label;
        }

        return $this;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function createForm(string $formType, array $options = []): self
    {
        $this->form = $this->formFactory->create($formType, $this->entity, $options);

        return $this;
    }

    public function onPreFlush(Closure $preFlush): self
    {
        $this->preFlush = $preFlush;

        return $this;
    }

    public function onPostFlush(Closure $postFlush): self
    {
        $this->postFlush = $postFlush;

        return $this;
    }

    public function onPreSubmit(Closure $preSubmit): self
    {
        $this->preSubmit = $preSubmit;

        return $this;
    }

    /**
     * @param Request $request
     *
     * @throws UploaderExtensionException
     *
     * @return Crud|RedirectResponse
     */
    public function persist(Request $request)
    {
        $this->form->handleRequest($request);
        if ($this->form->isSubmitted() && $this->form->isValid()) {
            if ($this->preFlush) {
                \call_user_func($this->preFlush, $this->entity, $this->form);
            }
            if (method_exists($this->entity, 'setPictures')) {
                $this->entity->setPictures($this->uploadAndGetPictures());
            }
            if (method_exists($this->entity, 'setUser') && $this->user instanceof User) {
                $this->entity->setUser($this->user);
            }
            $this->em->persist($this->entity);
            $this->em->flush();
            $this->session->remove('pictures');
            $this->session->getFlashBag()->add('success', "L'element a été ajouté avec succès");
            if ($this->postFlush) {
                \call_user_func($this->postFlush, $this->entity, $this->form);
            }
        } elseif ($this->preSubmit) {
            \call_user_func($this->preSubmit, $this->entity, $this->form);
        }

        $this->parameters->set('form', $this->form->createView());
        if (method_exists($this->entity, 'getPictures')) {
            $this->parameters->set('pictures', $this->getPictures($this->session->get('pictures', [])));
        }

        return $this;
    }

    /**
     * @param Request $request
     *
     * @throws UploaderExtensionException
     *
     * @return Crud|RedirectResponse
     */
    public function update(Request $request)
    {
        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            if ($this->preFlush) {
                \call_user_func($this->preFlush, $this->entity, $this->form);
            }
            if (method_exists($this->entity, 'addPictures')) {
                $this->entity->addPictures($this->uploadAndGetPictures());
            }
            $this->em->persist($this->entity);
            $this->em->flush();
            $this->session->remove('pictures');
            $this->session->getFlashBag()->add('success', "L'element a été mis à jour avec succès");
            if ($this->postFlush) {
                \call_user_func($this->postFlush, $this->entity, $this->form);
            }
        } elseif ($this->preSubmit) {
            \call_user_func($this->preSubmit, $this->entity, $this->form);
        }

        $this->parameters->set('form', $this->form->createView());
        if (method_exists($this->entity, 'getPictures')) {
            $this->parameters->set('pictures', $this->getPicturesWithFullPath(
                [$this->classLabel, $this->entity->getPictures()],
                $this->session->get('pictures', [])
            ));
        }

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function delete(Request $request): Response
    {
        $form = $this->formFactory->create(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->preFlush) {
                \call_user_func($this->preFlush, $this->entity, $form);
            }
            $this->em->remove($this->entity);
            $this->em->flush();
            $this->session->getFlashBag()->add('danger', "L'element a été supprimer avec succès");
            if ($this->postFlush) {
                return \call_user_func($this->postFlush, $this->entity, $form);
            }
        } elseif ($this->preSubmit) {
            \call_user_func($this->preSubmit, $this->entity, $form);
        }

        $this->parameters->replace([
            'form' => $form->createView(),
            'element' => sprintf('%s : %s', $this->classLabel, $this->entity->getTitle()),
        ]);

        return $this->render('admin/single/remove.html.twig');
    }

    /**
     * @param string $view
     * @param array  $parameters
     *
     * @return Response
     */
    public function render(string $view, array $parameters = []): Response
    {
        $content = $this->engine->render($view, array_replace(
            $this->parameters->all(),
            $parameters
        ));

        return new Response($content);
    }

    /**
     * @throws UploaderExtensionException
     *
     * @return array
     */
    protected function uploadAndGetPictures()
    {
        $pictures = $this->uploader
            ->setTmpDir('tmp', $this->classLabel)
            ->setFiles($this->session->get('pictures', []))
            ->upload();

        return $this->getFilesBasename($pictures);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function uploadAndGetImageBasename(string $name)
    {
        $file = $this->uploader->generateFile($this->session->get("pictures/{$name}"), 'tmp');
        if (!$file) {
            return '';
        }
        $picture = $this->uploader->move($file, $this->classLabel);

        return basename($picture);
    }
}
