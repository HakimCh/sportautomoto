<?php

declare(strict_types=1);

namespace App\Services\Mailer;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Twig\Error\Error;

class AbstractEmailType
{
    /**
     * @var string
     */
    protected $subject;
    /**
     * @var string
     */
    protected $templatePath;
    /**
     * @var array
     */
    protected $parameters;
    /**
     * @var TwigEngine
     */
    private $template;

    public function __construct(TwigEngine $template, ?array $parameters = [])
    {
        $this->parameters = $parameters;
        $this->template = $template;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string|null
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param array|null $parameters
     *
     * @throws MailerTemplateNotFoundException
     * @throws Error
     *
     * @return string
     */
    public function render(?array $parameters = []): string
    {
        if ($parameters) {
            $this->parameters = $parameters;
        }

        if (null === $this->templatePath) {
            throw new MailerTemplateNotFoundException();
        }

        return $this->template->render(
            sprintf('emails/%s.html.twig', $this->templatePath),
            $this->parameters
        );
    }
}
