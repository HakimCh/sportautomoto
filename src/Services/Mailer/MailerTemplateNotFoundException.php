<?php

declare(strict_types=1);

namespace App\Services\Mailer;

use Exception;

class MailerTemplateNotFoundException extends Exception
{
}
