<?php

declare(strict_types=1);

namespace App\Services\Mailer;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Swift_Mailer;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Twig\Error\Error;

class Mailer
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var Template
     */
    private $template;
    /**
     * @var array|string
     */
    private $sendFrom;
    /**
     * @var array|string
     */
    private $sendTo;
    /**
     * @var string
     */
    private $subject;

    public function __construct(Swift_Mailer $mailer, TwigEngine $template, array $sendFrom)
    {
        $this->mailer = $mailer;
        $this->template = $template;
        $this->sendFrom = $this->sendTo = $sendFrom;
    }

    /**
     * @param $sendTo
     *
     * @return Mailer
     */
    public function setSendTo($sendTo): self
    {
        $this->sendTo = $sendTo;

        return $this;
    }

    /**
     * @param $subject
     *
     * @return Mailer
     */
    public function setSubject($subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param $sendForm
     *
     * @return Mailer
     */
    public function setSendFrom($sendForm): self
    {
        $this->sendFrom = $sendForm;

        return $this;
    }

    /**
     * @param string $emailType
     * @param array  $parameters
     *
     * @throws MailerTemplateNotFoundException
     * @throws Error
     *
     * @return array
     */
    public function send(string $emailType, array $parameters = []): array
    {
        // @var AbstractEmailType $email
        $email = new $emailType($this->template);
        $subject = $email->getSubject() ?: $this->subject;
        $parameters['subject'] = $subject;
        $parameters['sendToName'] = key($this->sendTo);
        $parameters['sendTo'] = current($this->sendTo);

        $message = new \Swift_Message($subject);

        $message
            ->setTo($this->sendTo)
            ->setFrom($this->sendFrom)
            ->setBody($email->render($parameters), 'text/html');

        $failedRecipients = [];

        $this->mailer->send($message, $failedRecipients);

        return $failedRecipients;
    }
}
