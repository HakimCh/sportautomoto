<?php

declare(strict_types=1);

namespace App\Controller\Api;

use HakimCh\Helpers\ArrayBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/story/{customerType}", name="api_story_")
 */
class StoriesController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"GET"}, name="read")
     *
     * @param string  $id
     * @param string  $customerType
     * @param Session $session
     *
     * @return JsonResponse
     */
    public function read(string $id, string $customerType, Session $session): Response
    {
        $arrayBag = new ArrayBag($session->all());
        $data = $arrayBag->getByPath(sprintf('customer.%s.stories.%s', $customerType, $id));

        if ($data) {
            return $this->json(['status' => 'Donnée lu avec succès', 'data' => $data]);
        }

        return $this->json(['status' => 'Donnée non disponible', 'data' => 0]);
    }

    /**
     * @Route("/save", methods={"POST"}, name="save")
     *
     * @param string  $customerType
     * @param Request $request
     * @param Session $session
     *
     * @return JsonResponse
     */
    public function save(string $customerType, Request $request, Session $session): Response
    {
        $status = 'Mis à jour avec succès';
        $data = $session->get('customer');
        $experience = $request->request->all();

        if (!isset($data[$customerType]['stories'])) {
            $data[$customerType]['stories'] = [];
        }

        if (!\array_key_exists('id', $experience)) {
            $status = 'Ajouté avec succès';
            $experience['id'] = sha1(json_encode($experience));
            if (\array_key_exists($experience['id'], $data[$customerType]['stories'])) {
                return $this->json([
                    'status' => 'L\'entrée existe déja dans la liste',
                    'data' => 0,
                ]);
            }
        }

        $data[$customerType]['stories'][$experience['id']] = $experience;

        $session->set('customer', $data);

        return $this->json([
            'status' => $status,
            'data' => $data[$customerType]['stories'],
        ]);
    }

    /**
     * @Route("/{id}", methods={"DELETE"}, name="delete")
     *
     * @param mixed   $id
     * @param string  $customerType
     * @param Session $session
     *
     * @return Response
     */
    public function delete($id, string $customerType, Session $session): Response
    {
        $data = $session->get('customer');

        if (isset($data[$customerType]['stories'][$id])) {
            unset($data[$customerType]['stories'][$id]);
            $session->set('customer', $data);

            return $this->json(['status' => 'Supprimé avec succès', 'data' => $data[$customerType]['stories']]);
        }

        return $this->json(['status' => 'Donnée non disponible', 'data' => 0]);
    }
}
