<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Repository\PostClassifiedRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/classified", name="api_classified_")
 */
class ClassifiedController
{
    /**
     * @Route("/autocomplete/{text}", name="autocomplete")
     *
     * @param string                   $text
     * @param PostClassifiedRepository $repository
     *
     * @return JsonResponse
     */
    public function autocomplete(string $text, PostClassifiedRepository $repository): JsonResponse
    {
        $metas = $repository->findByPhrase($text);

        return new JsonResponse($metas);
    }
}
