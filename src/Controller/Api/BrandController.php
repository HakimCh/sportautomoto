<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Make;
use App\Repository\MakeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/brand", name="api_brand_")
 */
class BrandController extends AbstractController
{
    /**
     * @Route("/autocomplete/{text}", name="autocomplete")
     *
     * @param $text
     * @param MakeRepository $repository
     *
     * @return Response
     */
    public function autocomplete($text, MakeRepository $repository): Response
    {
        /** @var Make[] $models */
        $models = $repository->findByPhrase($text);
        $options = [];
        foreach ($models as $model) {
            $options[] = [
                'value' => $model->getId(),
                'label' => sprintf(
                    '%s / %s',
                    $model->getParent()->getTitle(),
                    $model->getTitle()
                ),
            ];
        }

        return $this->json($options);
    }
}
