<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Repository\PostEventRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/api/event", name="api_event_")
 */
class EventController
{
    /**
     * @Route("/", name="all")
     *
     * @param PostEventRepository $repository
     * @param RouterInterface     $router
     *
     * @return JsonResponse
     */
    public function all(PostEventRepository $repository, RouterInterface $router): JsonResponse
    {
        $items = [];
        $events = $repository->getRssFeed();

        foreach ($events as $event) {
            $startAt = $event->getStartAt();
            $endAt = $event->getEndAt();
            $items[] = [
                'id' => $event->getId(),
                'name' => $event->getTitle(),
                'startdate' => $startAt->format('Y-m-d'),
                'enddate' => $endAt->format('Y-m-d'),
                'starttime' => $startAt->format('H:i'),
                'endtime' => $endAt->format('H:i'),
                'color' => $this->getColor(),
                'url' => $router->generate('events_detail', [
                    'category' => $event->getCategory()->getSlug(),
                    'slug' => $event->getSlug(),
                    'id' => $event->getId(),
                ]),
            ];
        }

        return new JsonResponse(['monthly' => $items]);
    }

    private function getColor()
    {
        $colors = [
            '#F1654C', '#3E4651', '#2C82C9', '#8A2D3C', '#43353D', '#1D2247', '#D98B3A', '#42729B', '#45362E', '#63393E', '#3C3741',
            '#282830', '#953163', '#00587A', '#444B54', '#5C9F97', '#9E5428', '#1F9EA3', '#34495E', '#462446', '#A42A15', '#203040', '#3F3F3F', '#3F303F', '#E74C3C',
            '#5E412F', '#16A086', '#16A086', '#0F6177', '#025159', '#64C4AF', '#453E4A', '#3DA4AB', '#4387B5', '#BD3C4E', '#E74C3C', '#2C3E50', '#E74C3C', '#11132F',
        ];

        return $colors[rand(0, 38)];
    }
}
