<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/category", name="api_category_")
 */
class CategoryController
{
    /**
     * @Route("/{type}", defaults={"type" = "null"}, name="all")
     *
     * @param string             $type
     * @param CategoryRepository $repository
     * @param Request            $request
     *
     * @return JsonResponse
     */
    public function all(string $type, CategoryRepository $repository, Request $request): JsonResponse
    {
        $phrase = $request->request->get('phrase');
        $data = $repository->findByPhrase($phrase, $type);

        return new JsonResponse($data);
    }
}
