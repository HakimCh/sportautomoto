<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Form\ClassifiedSearchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxFormController extends AbstractController
{
    /**
     * @Route("/form/classified-search", name="classified_search_form")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function classifiedSearchForm(Request $request): Response
    {
        $form = $this->createForm(ClassifiedSearchType::class);
        $form->handleRequest($request);

        return $this->render('classified/form.twig', [
            'form' => $form->createView(),
        ]);
    }
}
