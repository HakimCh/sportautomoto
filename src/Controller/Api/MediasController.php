<?php

declare(strict_types=1);

namespace App\Controller\Api;

use Exception;
use HakimCh\Helpers\Http\Uploader;
use HakimCh\UploaderBundle\Services\ImageUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/medias", name="api_medias_")
 */
class MediasController
{
    public const SESSION_NAMESPACE = 'pictures';

    /**
     * @Route("/{key}", methods={"GET","OPTIONS"}, name="read")
     *
     * @param string           $key
     * @param SessionInterface $session
     *
     * @return Response
     */
    public function read(string $key, SessionInterface $session): Response
    {
        $key = $this->getKey($key);
        if ($photo = $session->get($key)) {
            return new JsonResponse([
                'status' => 'Image found',
                'data' => sprintf('/images/tmp/%s', $photo),
            ]);
        }

        return new JsonResponse(null, 404);
    }

    /**
     * @Route("/{path}/{key}", methods={"POST","OPTIONS"}, name="store")
     *
     * @param string           $path
     * @param string           $key
     * @param Request          $request
     * @param SessionInterface $session
     * @param Uploader         $uploader
     *
     * @throws Exception
     *
     * @return Response
     */
    public function store(string $path, string $key, Request $request, SessionInterface $session, Uploader $uploader): Response
    {
        /** @var UploadedFile $file */
        if ($file = $request->files->get($key)) {
            $fileUrl = $uploader->move($file, $path);
            $key = $this->getKey($key);
            $session->set($key, basename($fileUrl));

            return new JsonResponse([
                'status' => 'File successfully uploaded',
                'data' => $fileUrl,
            ]);
        }

        return new JsonResponse(null, 404);
    }

    /**
     * @Route("/{key}", methods={"DELETE","OPTIONS"}, name="delete")
     *
     * @param int|string       $key
     * @param SessionInterface $session
     *
     * @return Response
     */
    public function remove(string $key, SessionInterface $session): Response
    {
        $key = $this->getKey($key);
        if ($file = $session->get($key)) {
            $status = 204;
            $session->remove($key);
            if (file_exists($pathname = sprintf('%s/images/tmp/%s', ROOT_PATH, $file))) {
                unlink($pathname);
            }
        }

        return new JsonResponse(null, $status ?? 404);
    }

    /**
     * @Route("/", methods={"DELETE","OPTIONS"}, name="clear")
     *
     * @param SessionInterface $session
     *
     * @return Response
     */
    public function clear(SessionInterface $session): Response
    {
        $session->remove(self::SESSION_NAMESPACE);

        return new JsonResponse(null, 204);
    }

    /**
     * @Route("/crop", methods={"POST","OPTIONS"}, name="crop")
     *
     * @param Request          $request
     * @param SessionInterface $session
     * @param ImageUploader    $uploader
     *
     * @throws Exception
     *
     * @return Response
     */
    public function crop(Request $request, SessionInterface $session, ImageUploader $uploader): Response
    {
        $filename = $uploader
            ->open($request->get('pathname'))
            ->crop(
                $request->get('x'),
                $request->get('y'),
                $request->get('width'),
                $request->get('height')
            )
            ->upload('tmp');
        $session->set('tmp/crop', $filename);

        return new JsonResponse(null, 204);
    }

    /**
     * @Route("/rotate", name="rotate")
     *
     * @return Response
     * @TODO Add rotate method to the ImageUploader class
     */
    public function rotate(): Response
    {
        return new JsonResponse(null, 204);
    }

    /**
     * @param string $key
     *
     * @return string
     */
    private function getKey(string $key): string
    {
        return sprintf('%s/%s', self::SESSION_NAMESPACE, $key);
    }
}
