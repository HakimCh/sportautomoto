<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Make;
use App\Repository\MakeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/make", name="api_make_")
 */
class MakeController
{
    /**
     * @Route("/{type}", name="options")
     *
     * @param string         $type
     * @param MakeRepository $repository
     *
     * @return JsonResponse
     */
    public function options(string $type, MakeRepository $repository): JsonResponse
    {
        $makes = $repository->findBy(['type' => $type, 'parent' => null]);

        return new JsonResponse($this->getChoices($makes));
    }

    /**
     * @Route("/{type}/{make}", name="options_by_make")
     *
     * @param string         $type
     * @param int            $make
     * @param MakeRepository $repository
     *
     * @return JsonResponse
     */
    public function optionsByMake(string $type, $make, MakeRepository $repository): JsonResponse
    {
        $makes = $repository->findBy(['parent' => $make, 'type' => $type]);

        return new JsonResponse($this->getChoices($makes));
    }

    /**
     * @param Make[] $makes
     * @param bool   $flip
     *
     * @return array
     */
    private function getChoices($makes, bool $flip = false): array
    {
        $choices = [];
        foreach ($makes as $make) {
            if ($flip) {
                $choices[$make->getTitle()] = $make->getId();
            } else {
                $choices[$make->getId()] = $make->getTitle();
            }
        }

        return $choices;
    }
}
