<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\CustomerDriver;
use App\Entity\EntityInterface;
use App\Entity\PostEvent;
use App\Exceptions\UploaderExtensionException;
use App\Form\EventType;
use App\Form\SearchType;
use App\Repository\PostEventRepository;
use App\Services\Crud;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/event", name="backend_event_")
 */
class PostEventController extends AbstractController
{
    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show")
     *
     * @param PostEventRepository $repository
     * @param Paginate            $paginate
     * @param Request             $request
     *
     * @return Response
     */
    public function show(PostEventRepository $repository, Paginate $paginate, Request $request): Response
    {
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        $criteria = $request->query->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $criteria += $form->getData();
        }

        $query = $repository->findByCriteria($criteria);

        return $this->render('admin/event/show.html.twig', [
            'form' => $form->createView(),
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
            'statutes' => $repository->getCountByStatus(),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud)
    {
        $crud
            ->setEntity(new PostEvent(), PostEvent::TYPE_LABEL)
            ->createForm(EventType::class)
            ->onPostFlush(function (EntityInterface $post) {
                $this->redirectToRoute(
                    'backend_event_update',
                    ['id' => $post->getId()]
                );
            })
            ->persist($request);

        return $crud->render('admin/event/save.html.twig');
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param PostEvent $event
     * @param Request   $request
     * @param Crud      $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(PostEvent $event, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($event, PostEvent::TYPE_LABEL)
            ->createForm(EventType::class)
            ->onPostFlush(function (EntityInterface $event) {
                $this->redirectToRoute(
                    'backend_event_update',
                    ['id' => $event->getId()]
                );
            })
            ->update($request);

        return $crud->render('admin/event/save.html.twig');
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param PostEvent $event
     * @param Request   $request
     * @param Crud      $crud
     *
     * @return Response
     */
    public function remove(PostEvent $event, Request $request, Crud $crud): Response
    {
        return $crud
            ->setEntity($event, PostEvent::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('backend_event_show');
            })
            ->onPreFlush(function (CustomerDriver $customer) {
                $customer->getCategories()->clear();
            })
            ->delete($request);
    }
}
