<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Form\DeleteType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/category", name="backend_categories_")
 */
class CategoriesController extends AbstractController
{
    /**
     * @Route("/{type}/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show")
     *
     * @param string                 $type
     * @param Request                $request
     * @param EntityManagerInterface $em
     * @param CategoryRepository     $repository
     * @param Paginate               $paginate
     *
     * @return Response
     */
    public function show(string $type, Request $request, EntityManagerInterface $em, CategoryRepository $repository, Paginate $paginate): Response
    {
        $meta = new Category();
        $form = $this->createForm(CategoryType::class, $meta, ['type' => $type]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($meta);
            $em->flush();
            $this->addFlash('success', sprintf('Catégorie %s a été ajouter dans la section Deal', $meta->getTitle()));

            return $this->redirectToRoute('backend_categories_show', compact('type'));
        }
        $query = $repository->getQueryMetaByType($type);

        return $this->render('admin/categories/show.html.twig', [
            'form' => $form->createView(),
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
            'type' => $type,
        ]);
    }

    /**
     * @Route("/{type}/{action}/{id}", defaults={"action" = "update"}, name="update")
     *
     * @param string                 $type
     * @param Category               $meta
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function update(string $type, Category $meta, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(CategoryType::class, $meta, ['type' => $type]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($meta);
            $em->flush();
            $this->addFlash('success', sprintf('Catégorie %s a été modifier avec succès', $meta->getTitle()));

            return $this->redirectToRoute('backend_categories_show', compact('type'));
        }

        return $this->render('admin/categories/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{type}/remove/{id}", requirements={"action" = "remove"}, name="remove")
     *
     * @param string                 $type
     * @param Category               $meta
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function remove(string $type, Category $meta, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(DeleteType::class, $meta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('danger', sprintf('Catégorie %s a été supprimer avec succès', $meta->getTitle()));
            $em->remove($meta);
            $em->flush();

            return $this->redirectToRoute('backend_categories_show', compact('type'));
        }

        return $this->render('admin/single/remove.html.twig', [
            'form' => $form->createView(),
            'element' => 'La catégorie: '.$meta->getTitle(),
        ]);
    }
}
