<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\CustomerProfessional;
use App\Entity\EntityInterface;
use App\Exceptions\UploaderExtensionException;
use App\Form\CustomerProfessionalType;
use App\Repository\CustomerProfessionalRepository;
use App\Services\Crud;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/professional", name="backend_professional_")
 */
class CustomerProfessionalController extends AbstractController
{
    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show"))
     *
     * @param CustomerProfessionalRepository $repository
     * @param Paginate                       $paginate
     *
     * @return Response
     */
    public function show(CustomerProfessionalRepository $repository, Paginate $paginate): Response
    {
        $query = $repository->findAll();

        return $this->render('admin/customers/professional/show.html.twig', [
            'customerType' => CustomerProfessional::TYPE_LABEL,
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
        ]);
    }

    /**
     * @Route("/subscription/{id}", name="subscription"))
     *
     * @param CustomerProfessional $customer
     *
     * @return Response
     */
    public function subscription(CustomerProfessional $customer): Response
    {
        return $this->render('admin/customers/professional/subscription.html.twig', [
            'subscriptions' => $customer->getSubscriptions(),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity(new CustomerProfessional(), CustomerProfessional::TYPE_LABEL)
            ->createForm(CustomerProfessionalType::class)
            ->onPostFlush(function (EntityInterface $customer) {
                $this->redirectToRoute(
                    'backend_professional_update',
                    ['customerId' => $customer->getId()]
                );
            })
            ->persist($request);

        return $crud->render('admin/customers/professional/save.html.twig', [
            'customerType' => CustomerProfessional::TYPE_LABEL,
        ]);
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param CustomerProfessional $customer
     * @param Request              $request
     * @param Crud                 $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(CustomerProfessional $customer, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($customer, CustomerProfessional::TYPE_LABEL)
            ->createForm(CustomerProfessionalType::class)
            ->onPostFlush(function (EntityInterface $customer) {
                $this->redirectToRoute(
                    'backend_professional_update',
                    ['customerId' => $customer->getId()]
                );
            })
            ->update($request);

        return $crud->render('admin/customers/professional/save.html.twig', [
            'customerType' => CustomerProfessional::TYPE_LABEL,
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param CustomerProfessional $customer
     * @param Request              $request
     * @param Crud                 $crud
     *
     * @return Response
     */
    public function remove(CustomerProfessional $customer, Request $request, Crud $crud): Response
    {
        return $crud
            ->setEntity($customer, CustomerProfessional::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('backend_driver_show');
            })
            ->onPreFlush(function (CustomerProfessional $customer) {
                $customer->getCategories()->clear();
            })
            ->delete($request);
    }
}
