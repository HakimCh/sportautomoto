<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\CustomerStory;
use App\Exceptions\UploaderExtensionException;
use App\Form\CustomerStoryType;
use App\Repository\CustomerRepository;
use App\Repository\CustomerStoriesRepository;
use App\Services\Crud;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/{type}/{customerId}/story", name="backend_customer_story_", requirements={"type" = "official|driver"})
 */
class CustomerStoriesController extends AbstractController
{
    /**
     * @Route("/add", name="add")
     *
     * @param int                       $customerId
     * @param string                    $type
     * @param Request                   $request
     * @param CustomerStoriesRepository $repository
     * @param CustomerRepository        $customerRepository
     * @param Crud                      $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(int $customerId, string $type, Request $request, CustomerStoriesRepository $repository, CustomerRepository $customerRepository, Crud $crud): Response
    {
        $crud
            ->setEntity(new CustomerStory())
            ->createForm(CustomerStoryType::class, compact('type'))
            ->onPreFlush(function (CustomerStory $story) use ($customerId, $customerRepository) {
                $story->setCustomer(
                    $customerRepository->find($customerId)
                );
            })
            ->onPostFlush(function () use ($customerId, $type) {
                return $this->redirectToRoute('backend_customer_story_add', compact('customerId', 'type'));
            })
            ->persist($request);

        return $crud->render(sprintf('admin/customers/%s/stories.html.twig', $type), [
            'type' => $type,
            'customerId' => $customerId,
            'items' => $repository->findByCustomerId($customerId),
        ]);
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param int                       $customerId
     * @param string                    $type
     * @param CustomerStory             $story
     * @param Request                   $request
     * @param CustomerStoriesRepository $repository
     * @param Crud                      $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(int $customerId, string $type, CustomerStory $story, Request $request, CustomerStoriesRepository $repository, Crud $crud): Response
    {
        $crud
            ->setEntity($story)
            ->createForm(CustomerStoryType::class, compact('type'))
            ->onPostFlush(function () use ($customerId, $type) {
                return $this->redirectToRoute('backend_customer_story_add', compact('customerId', 'type'));
            })
            ->update($request);

        return $crud->render(sprintf('admin/customers/%s/stories.html.twig', $type), [
            'type' => $type,
            'customerId' => $customerId,
            'items' => $repository->findByCustomerId($customerId),
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param int           $customerId
     * @param string        $type
     * @param CustomerStory $story
     * @param Request       $request
     * @param Crud          $crud
     *
     * @return Response
     */
    public function remove(int $customerId, string $type, CustomerStory $story, Request $request, Crud $crud): Response
    {
        return $crud
            ->setEntity($story)
            ->onPostFlush(function () use ($customerId, $type) {
                return $this->redirectToRoute('backend_customer_story_add', compact('customerId', 'type'));
            })
            ->delete($request);
    }
}
