<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\CustomerOfficial;
use App\Entity\EntityInterface;
use App\Exceptions\UploaderExtensionException;
use App\Form\CustomerOfficialType;
use App\Repository\CustomerOfficialRepository;
use App\Services\Crud;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/official", name="backend_official_")
 */
class CustomerOfficialController extends AbstractController
{
    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show"))
     *
     * @param CustomerOfficialRepository $repository
     * @param Paginate                   $paginate
     *
     * @return Response
     */
    public function show(CustomerOfficialRepository $repository, Paginate $paginate): Response
    {
        $query = $repository->findAll();

        return $this->render('admin/customers/official/show.html.twig', [
            'customerType' => CustomerOfficial::TYPE_LABEL,
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
        ]);
    }

    /**
     * @Route("/subscription/{id}", name="subscription"))
     *
     * @param CustomerOfficial $customer
     *
     * @return Response
     */
    public function subscription(CustomerOfficial $customer): Response
    {
        return $this->render('admin/customers/official/subscription.html.twig', [
            'subscriptions' => $customer->getSubscriptions(),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity(new CustomerOfficial(), CustomerOfficial::TYPE_LABEL)
            ->createForm(CustomerOfficialType::class)
            ->onPostFlush(function (EntityInterface $customer) {
                $this->redirectToRoute(
                    'backend_official_update',
                    ['id' => $customer->getId()]
                );
            })
            ->persist($request);

        return $crud->render('admin/customers/official/save.html.twig', [
            'customerType' => CustomerOfficial::TYPE_LABEL,
        ]);
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param CustomerOfficial $customer
     * @param Request          $request
     * @param Crud             $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(CustomerOfficial $customer, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($customer, CustomerOfficial::TYPE_LABEL)
            ->createForm(CustomerOfficialType::class)
            ->onPostFlush(function (EntityInterface $customer) {
                return $this->redirectToRoute(
                    'backend_official_update',
                    ['id' => $customer->getId()]
                );
            })
            ->update($request);

        return $crud->render('admin/customers/official/save.html.twig', [
            'customerType' => CustomerOfficial::TYPE_LABEL,
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param CustomerOfficial $customer
     * @param Request          $request
     * @param Crud             $crud
     *
     * @return Response
     */
    public function remove(CustomerOfficial $customer, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($customer, CustomerOfficial::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('backend_official_show');
            })
            ->onPreFlush(function (CustomerOfficial $customer) {
                $customer->getCategories()->clear();
            })
            ->delete($request);
    }
}
