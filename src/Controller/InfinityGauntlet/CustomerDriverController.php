<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\CustomerDriver;
use App\Entity\EntityInterface;
use App\Exceptions\UploaderExtensionException;
use App\Form\CustomerDriverType;
use App\Repository\CustomerDriverRepository;
use App\Services\Crud;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/driver", name="backend_driver_")
 */
class CustomerDriverController extends AbstractController
{
    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show"))
     *
     * @param CustomerDriverRepository $repository
     * @param Paginate                 $paginate
     *
     * @return Response
     */
    public function show(CustomerDriverRepository $repository, Paginate $paginate): Response
    {
        $query = $repository->findAll();

        return $this->render('admin/customers/driver/show.html.twig', [
            'customerType' => CustomerDriver::TYPE_LABEL,
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
        ]);
    }

    /**
     * @Route("/subscription/{id}", name="subscription"))
     *
     * @param CustomerDriver $customer
     *
     * @return Response
     */
    public function subscription(CustomerDriver $customer): Response
    {
        return $this->render('admin/customers/driver/subscription.html.twig', [
            'subscriptions' => $customer->getSubscriptions(),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity(new CustomerDriver(), CustomerDriver::TYPE_LABEL)
            ->createForm(CustomerDriverType::class)
            ->onPostFlush(function (EntityInterface $customer) {
                $this->redirectToRoute(
                    'backend_driver_update',
                    ['id' => $customer->getId()]
                );
            })
            ->persist($request);

        return $crud->render('admin/customers/driver/save.html.twig', [
            'customerType' => CustomerDriver::TYPE_LABEL,
        ]);
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param CustomerDriver $customer
     * @param Request        $request
     * @param Crud           $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(CustomerDriver $customer, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($customer, CustomerDriver::TYPE_LABEL)
            ->createForm(CustomerDriverType::class)
            ->onPostFlush(function (EntityInterface $customer) {
                $this->redirectToRoute(
                    'backend_driver_update',
                    ['id' => $customer->getId()]
                );
            })
            ->update($request);

        return $crud->render('admin/customers/driver/save.html.twig', [
            'customerType' => CustomerDriver::TYPE_LABEL,
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param CustomerDriver $customer
     * @param Request        $request
     * @param Crud           $crud
     *
     * @return Response
     */
    public function remove(CustomerDriver $customer, Request $request, Crud $crud): Response
    {
        return $crud
            ->setEntity($customer, CustomerDriver::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('backend_driver_show');
            })
            ->onPreFlush(function (CustomerDriver $customer) {
                $customer->getCategories()->clear();
            })
            ->delete($request);
    }
}
