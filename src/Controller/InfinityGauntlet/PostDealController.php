<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\EntityInterface;
use App\Entity\PostDeal;
use App\Exceptions\UploaderExtensionException;
use App\Form\DealType;
use App\Form\SearchType;
use App\Repository\PostDealOrdersRepository;
use App\Repository\PostDealRepository;
use App\Services\Crud;
use Doctrine\ORM\EntityManagerInterface;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/deal", name="backend_deal_")
 */
class PostDealController extends AbstractController
{
    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show"))
     *
     * @param Request            $request
     * @param PostDealRepository $repository
     * @param Paginate           $paginate
     *
     * @return Response
     */
    public function list(Request $request, PostDealRepository $repository, Paginate $paginate)
    {
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        $criteria = $request->query->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $criteria += $form->getData();
        }

        $query = $repository->findByCriteria($criteria);

        return $this->render('admin/deals/show.html.twig', [
            'form' => $form->createView(),
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
            'statutes' => $repository->getCountByStatus(),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud)
    {
        $crud
            ->setEntity(new PostDeal(), PostDeal::TYPE_LABEL)
            ->createForm(DealType::class)
            ->onPostFlush(function (EntityInterface $post) {
                $this->redirectToRoute(
                    'backend_deal_update',
                    ['id' => $post->getId()]
                );
            })
            ->persist($request);

        return $crud->render('admin/deals/save.html.twig');
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param PostDeal $deal
     * @param Request  $request
     * @param Crud     $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(PostDeal $deal, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($deal, PostDeal::TYPE_LABEL)
            ->createForm(DealType::class)
            ->onPostFlush(function (EntityInterface $deal) {
                $this->redirectToRoute(
                    'backend_deal_update',
                    ['id' => $deal->getId()]
                );
            })
            ->update($request);

        return $crud->render('admin/deals/save.html.twig');
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param PostDeal $deal
     * @param Request  $request
     * @param Crud     $crud
     *
     * @return Response
     */
    public function remove(PostDeal $deal, Request $request, Crud $crud): Response
    {
        return $crud
            ->setEntity($deal, PostDeal::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('backend_deal_show');
            })
            ->delete($request);
    }

    /**
     * @Route("/status/{action}/{id}", name="change_status")
     *
     * @param string                 $action
     * @param PostDeal               $deal
     * @param EntityManagerInterface $em
     * @param TranslatorInterface    $translator
     *
     * @return RedirectResponse
     */
    public function status(string $action, PostDeal $deal, EntityManagerInterface $em, TranslatorInterface $translator): RedirectResponse
    {
        $deal->setStatus(strtoupper($action));
        $em->persist($deal);
        $em->flush();
        $this->addFlash('success', sprintf(
            'Le deal a été %s avec succès',
            $translator->trans($action)
        ));

        return $this->redirectToRoute('backend_deal_show');
    }

    /**
     * @Route("/orders/{dealId}", name="orders")
     *
     * @param int                      $dealId
     * @param PostDealOrdersRepository $repository
     * @param Paginate                 $paginate
     *
     * @return Response
     */
    public function orders(int $dealId, PostDealOrdersRepository $repository, Paginate $paginate)
    {
        $query = $repository->searchByCriteria(['dealId' => $dealId]);

        return $this->render('admin/deals/orders.html.twig', [
            'dealId' => $dealId,
            'paginate' => $paginate->setItemsPerPage(100)->make($query),
        ]);
    }
}
