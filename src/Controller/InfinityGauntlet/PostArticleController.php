<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\EntityInterface;
use App\Entity\PostArticle;
use App\Exceptions\UploaderExtensionException;
use App\Form\ArticleType;
use App\Form\SearchType;
use App\Repository\PostArticleRepository;
use App\Services\Crud;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/article", name="backend_article_")
 */
class PostArticleController extends AbstractController
{
    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show")
     *
     * @param PostArticleRepository $repository
     * @param Paginate              $paginate
     * @param Request               $request
     *
     * @return Response
     */
    public function show(PostArticleRepository $repository, Paginate $paginate, Request $request): Response
    {
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        $criteria = $request->query->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $criteria += $form->getData();
        }

        $query = $repository->findByCriteria($criteria);

        return $this->render('admin/article/show.html.twig', [
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
            'statutes' => $repository->getCountByStatus(),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud)
    {
        $crud
            ->setEntity(new PostArticle(), PostArticle::TYPE_LABEL)
            ->createForm(ArticleType::class)
            ->setUser($this->getUser())
            ->onPostFlush(function (EntityInterface $post) {
                $this->redirectToRoute(
                    'backend_article_update',
                    ['id' => $post->getId()]
                );
            })
            ->persist($request);

        return $crud->render('admin/article/add.html.twig');
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param PostArticle $article
     * @param Request     $request
     * @param Crud        $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(PostArticle $article, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($article, PostArticle::TYPE_LABEL)
            ->createForm(ArticleType::class)
            ->onPostFlush(function (EntityInterface $article) {
                $this->redirectToRoute(
                    'backend_article_update',
                    ['id' => $article->getId()]
                );
            })
            ->update($request);

        return $crud->render('admin/article/update.html.twig');
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param PostArticle $article
     * @param Request     $request
     * @param Crud        $crud
     *
     * @return Response
     */
    public function remove(PostArticle $article, Request $request, Crud $crud): Response
    {
        return $crud
            ->setEntity($article, PostArticle::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('backend_article_show');
            })
            ->delete($request);
    }
}
