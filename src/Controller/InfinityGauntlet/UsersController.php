<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\User;
use App\Form\DeleteType;
use App\Form\UserProfileType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Security\Encoder\LegacyEncoder;
use Doctrine\ORM\EntityManagerInterface;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user", name="backend_users_")
 */
class UsersController extends AbstractController
{
    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show")
     *
     * @param UserRepository $repository
     * @param Paginate       $paginate
     *
     * @return Response
     */
    public function show(UserRepository $repository, Paginate $paginate): Response
    {
        $query = $repository->getQueryBuilder();

        return $this->render('admin/users/show.html.twig', [
            'paginate' => $paginate->setItemsPerPage(16)->make($query),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $em): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', "L'element a été ajouté avec succès");

            return $this->redirectToRoute('backend_users_update', ['id' => $user->getId()]);
        }

        return $this->render('admin/users/save.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param User                   $user
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function update(User $user, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', "L'element a été mis à jour avec succès");

            return $this->redirectToRoute('backend_users_update', ['id' => $user->getId()]);
        }

        return $this->render('admin/users/save.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profile", name="profile")
     *
     * @param Request                $request
     * @param EntityManagerInterface $em
     * @param LegacyEncoder          $encoder
     *
     * @return Response
     */
    public function profile(Request $request, EntityManagerInterface $em, LegacyEncoder $encoder)
    {
        /** @var User $user */
        $user = $this->getUser();
        $currentPassword = $user->getPassword();
        $form = $this->createForm(UserProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (null === ($newPassword = $user->getPassword())) {
                $user->setPassword($currentPassword);
            } else {
                $user->setEncodedPassword($encoder);
            }
            $em->persist($user);
            $em->flush();
        }

        return $this->render('admin/single/profile.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param User                   $user
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function remove(User $user, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($user);
            $em->flush();
            $this->addFlash('danger', "L'element a été supprimer avec succès");

            return $this->redirectToRoute('backend_users_show');
        }

        return $this->render('admin/single/remove.html.twig', [
            'form' => $form->createView(),
            'element' => 'Utilisateur: '.$user->getTitle(),
        ]);
    }
}
