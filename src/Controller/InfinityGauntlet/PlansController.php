<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\Plan;
use App\Exceptions\UploaderExtensionException;
use App\Form\PlanType;
use App\Repository\PlanRepository;
use App\Services\Crud;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/plan/{type}", name="backend_plans_")
 */
class PlansController extends AbstractController
{
    /**
     * @Route("/{action}", defaults={"action" = "update"}, name="update")
     *
     * @param string         $type
     * @param PlanRepository $repository
     * @param Request        $request
     * @param Crud           $crud
     *
     * @throws UploaderExtensionException
     * @throws NoResultException
     * @throws NonUniqueResultException
     *
     * @return Response
     */
    public function update(string $type, PlanRepository $repository, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($repository->findByType($type))
            ->createForm(PlanType::class)
            ->onPostFlush(function (Plan $plan) {
                return $this->redirectToRoute('backend_plans_update', ['type' => $plan->getType()]);
            })
            ->update($request);

        return $crud->render('admin/plans/save.html.twig');
    }
}
