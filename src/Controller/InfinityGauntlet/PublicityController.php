<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Repository\PubRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/publicity", name="backend_publicity_")
 */
class PublicityController extends AbstractController
{
    /**
     * @Route("/", name="show")
     *
     * @param PubRepository $pubRepository
     *
     * @return Response
     */
    public function show(PubRepository $pubRepository)
    {
        $items = $pubRepository->findPubs();

        return $this->render('admin/publicity/show.html.twig', [
            'items' => $items,
        ]);
    }
}
