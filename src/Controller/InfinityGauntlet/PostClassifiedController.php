<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\EntityInterface;
use App\Entity\Make;
use App\Entity\PostClassified;
use App\Exceptions\UploaderExtensionException;
use App\Form\ClassifiedType;
use App\Form\SearchType;
use App\Repository\PostClassifiedRepository;
use App\Services\Crud;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/classified", name="backend_classified_")
 */
class PostClassifiedController extends AbstractController
{
    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="show")
     *
     * @param PostClassifiedRepository $repository
     * @param Paginate                 $paginate
     * @param Request                  $request
     *
     * @return Response
     */
    public function show(PostClassifiedRepository $repository, Paginate $paginate, Request $request): Response
    {
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        $criteria = $request->query->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $criteria += $form->getData();
        }

        $query = $repository->findByCriteria($criteria);

        return $this->render('admin/classifieds/show.html.twig', [
            'form' => $form->createView(),
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
            'statutes' => $repository->getCountByStatus(),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud)
    {
        $crud
            ->setEntity(new PostClassified(), PostClassified::TYPE_LABEL)
            ->createForm(ClassifiedType::class)
            ->onPostFlush(function (EntityInterface $classified) {
                $this->redirectToRoute(
                    'backend_classified_update',
                    ['id' => $classified->getId()]
                );
            })
            ->persist($request);

        return $crud->render('admin/posts/save.html.twig');
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param PostClassified $classified
     * @param Request        $request
     * @param Crud           $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(PostClassified $classified, Request $request, Crud $crud): Response
    {
        $crud
            ->setEntity($classified, PostClassified::TYPE_LABEL)
            ->createForm(ClassifiedType::class)
            ->onPreSubmit(function (PostClassified $classified, FormInterface $form) {
                if (!$classified->getModel() instanceof Make) {
                    return null;
                }
                $form->get('classifiedModel')->setData($classified->getModel()->getTitle());
                $form->get('model')->setData($classified->getModel()->getId());
            })
            ->onPostFlush(function (EntityInterface $classified) {
                $this->redirectToRoute(
                    'backend_classified_update',
                    ['id' => $classified->getId()]
                );
            })
            ->update($request);

        return $crud->render('admin/classifieds/save.html.twig');
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param PostClassified $classified
     * @param Request        $request
     * @param Crud           $crud
     *
     * @return Response
     */
    public function remove(PostClassified $classified, Request $request, Crud $crud): Response
    {
        return $crud
            ->setEntity($classified, PostClassified::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('backend_classified_show');
            })
            ->delete($request);
    }
}
