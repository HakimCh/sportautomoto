<?php

declare(strict_types=1);

namespace App\Controller\InfinityGauntlet;

use App\Entity\CustomerDriver;
use App\Entity\PubCampaigns;
use App\Exceptions\UploaderExtensionException;
use App\Form\CampaignType;
use App\Repository\PubCampaignsRepository;
use App\Repository\PubRepository;
use App\Services\Crud;
use App\Traits\PicturesTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/campaigns/{pubId}", name="backend_campaigns_")
 */
class CampaignsController extends AbstractController
{
    use PicturesTrait;

    /**
     * @Route("/", name="show")
     *
     * @param int                    $pubId
     * @param PubCampaignsRepository $repository
     *
     * @return Response
     */
    public function show(int $pubId, PubCampaignsRepository $repository): Response
    {
        $items = $repository->findBy(['pub' => $pubId]);

        return $this->render('admin/publicity/campaigns/show.html.twig', [
            'items' => $items,
        ]);
    }

    /**
     * @Route("/{action]", defaults={"action" = "add"}, name="add")
     *
     * @param int           $pubId
     * @param Crud          $crud
     * @param Request       $request
     * @param PubRepository $pubRepository
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(int $pubId, Crud $crud, Request $request, PubRepository $pubRepository): Response
    {
        $pub = $pubRepository->find($pubId);

        $crud
            ->setEntity(new PubCampaigns())
            ->createForm(CampaignType::class)
            ->onPreFlush(function (PubCampaigns $campaign) use ($pub) {
                $campaign->setPub($pub);
            })
            ->onPostFlush(function (PubCampaigns $campaign) use ($pubId) {
                $this->redirectToRoute(
                    'backend_campaigns_show',
                    ['pubId' => $pubId]
                );
            })
            ->persist($request);

        return $crud->render('admin/publicity/campaigns/save.html.twig', [
            'photoSizes' => $pub->getType(),
        ]);
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param int          $pubId
     * @param PubCampaigns $campaign
     * @param Crud         $crud
     * @param Request      $request
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function update(int $pubId, PubCampaigns $campaign, Crud $crud, Request $request): Response
    {
        $crud
            ->setEntity($campaign)
            ->createForm(CampaignType::class)
            ->onPostFlush(function (PubCampaigns $campaign) use ($pubId) {
                $this->redirectToRoute(
                    'backend_campaigns_show',
                    ['pubId' => $pubId]
                );
            })
            ->update($request);

        return $crud->render('admin/publicity/campaigns/save.html.twig', [
            'photoSizes' => $campaign->getType(),
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param int          $pubId
     * @param PubCampaigns $campaign
     * @param Request      $request
     * @param Crud         $crud
     *
     * @return Response
     */
    public function remove(int $pubId, PubCampaigns $campaign, Request $request, Crud $crud): Response
    {
        return $crud
            ->setEntity($campaign)
            ->onPostFlush(function () use ($pubId) {
                $this->redirectToRoute(
                    'backend_campaigns_show',
                    ['pubId' => $pubId]
                );
            })
            ->onPreFlush(function (CustomerDriver $customer) {
                $customer->getCategories()->clear();
            })
            ->delete($request);
    }

    /**
     * @Route("/archive/{status}/{id}", name="toggle")
     *
     * @param int                    $pubId
     * @param string                 $status
     * @param PubCampaigns           $campaign
     * @param EntityManagerInterface $em
     * @param TranslatorInterface    $translator
     *
     * @return Response
     */
    public function toggle(int $pubId, string $status, PubCampaigns $campaign, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $campaign->setStatus(strtoupper($status));
        $em->persist($campaign);
        $em->flush();
        $this->addFlash('success', sprintf("La status de l'element a été modifié en %s", $translator->trans($status)));

        return $this->redirectToRoute('backend_campaigns_show', compact('pubId'));
    }
}
