<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\EntityInterface;
use App\Entity\Make;
use App\Entity\Post;
use App\Entity\PostClassified;
use App\Exceptions\UploaderExtensionException;
use App\Form\ClassifiedType;
use App\Repository\PostClassifiedRepository;
use App\Services\Crud;
use Exception;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur/classified", name="user_classified_")
 */
class ClassifiedsController extends AbstractController
{
    /**
     * @var PostClassifiedRepository
     */
    private $repository;

    public function __construct(PostClassifiedRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route(
     *     "/{pageName}/{page}",
     *     defaults={"pageName" = "page", "page" = 1},
     *     requirements={"pageName" = "page", "page" = "[\d]+"},
     *     name="list"
     * )
     *
     * @param Paginate $paginate
     *
     * @return Response
     */
    public function list(Paginate $paginate): Response
    {
        $query = $this->repository->findByCriteria([
            'userId' => $this->getUser()->getId(),
        ]);

        return $this->render('user/classified/index.html.twig', [
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud)
    {
        $crud
            ->setEntity(new PostClassified(), PostClassified::TYPE_LABEL)
            ->createForm(ClassifiedType::class)
            ->onPostFlush(function (EntityInterface $classified) {
                $this->redirectToRoute(
                    'backend_classified_update',
                    ['id' => $classified->getId()]
                );
            })
            ->persist($request);

        return $crud->render('user/classified/save.html.twig');
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param int     $id
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     * @throws Exception
     *
     * @return Response
     */
    public function update(int $id, Request $request, Crud $crud): Response
    {
        $classified = $this->getUserClassifiedBy($id);
        $crud
            ->setEntity($classified, PostClassified::TYPE_LABEL)
            ->createForm(ClassifiedType::class)
            ->onPreSubmit(function (PostClassified $classified, FormInterface $form) {
                if (!$classified->getModel() instanceof Make) {
                    return null;
                }
                $form->get('classifiedModel')->setData($classified->getModel()->getTitle());
                $form->get('model')->setData($classified->getModel()->getId());
            })
            ->onPostFlush(function (EntityInterface $classified) {
                $this->redirectToRoute(
                    'user_classified_update',
                    ['id' => $classified->getId()]
                );
            })
            ->update($request);

        return $crud->render('user/classified/save.html.twig');
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param int     $id
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws Exception
     *
     * @return Response
     */
    public function remove(int $id, Request $request, Crud $crud): Response
    {
        $classified = $this->getUserClassifiedBy($id);

        return $crud
            ->setEntity($classified, PostClassified::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('user_classified_list');
            })
            ->delete($request);
    }

    /**
     * @Route("/archive/{id}", name="archive", requirements={"action" = "archive|publish"})
     *
     * @param int $id
     *
     * @throws Exception
     *
     * @return Response
     */
    public function archive(int $id): Response
    {
        $this->getUserClassifiedBy($id)->setStatus(Post::STATUS_ARCHIVE);
        $this->addFlash('success', 'Votre annonce a été archivé');

        return $this->redirectToRoute('user_classified_list');
    }

    /**
     * @Route("/publish/{id}", name="publish", requirements={"action" = "archive|publish"})
     *
     * @param int $id
     *
     * @throws Exception
     *
     * @return Response
     */
    public function publish(int $id): Response
    {
        $this->getUserClassifiedBy($id)->setStatus(Post::STATUS_PENDING);
        $this->addFlash('success', "Votre annonce a été mise en attente pour la validation d'un modérateur");

        return $this->redirectToRoute('user_classified_list');
    }

    /**
     * @param int $id
     *
     * @throws Exception
     *
     * @return PostClassified
     */
    private function getUserClassifiedBy(int $id): PostClassified
    {
        $classified = $this->repository->findOneBy([
            'user' => $this->getUser(),
            'id' => $id,
        ]);
        if (!$classified) {
            throw new Exception(sprintf(
                'Fatal Error: User (id: %d) not allowed to update this classified (id: %d)',
                $this->getUser()->getId(),
                $id
            ));
        }

        return $classified;
    }
}
