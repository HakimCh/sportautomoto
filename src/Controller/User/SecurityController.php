<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Form\UserLoginType;
use App\Form\UserSecurityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/utilisateur/securite-connexion", name="user_security")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function profile(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserSecurityType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // process data
        }

        return $this->render('user/security.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/connexion", name="login_page")
     *
     * @param AuthenticationUtils           $utils
     * @param AuthorizationCheckerInterface $checker
     *
     * @return Response
     */
    public function login(AuthenticationUtils $utils, AuthorizationCheckerInterface $checker): Response
    {
        if ($checker->isGranted('IS_AUTHENTICATED_FULLY')) {
            $route = $checker->isGranted('ROLE_USER') ? 'user_profile' : 'backend_home';

            return $this->redirectToRoute($route);
        }

        $error = $utils->getLastAuthenticationError();
        $lastUsername = $utils->getLastUsername();

        $form = $this->createForm(UserLoginType::class, [
            '_username' => $lastUsername,
        ]);

        return $this->render(
            'user/login.html.twig',
            [
                'form' => $form->createView(),
                'error' => $error,
                'page' => [
                    'title' => 'Se connecter',
                ],
            ]
        );
    }

    /**
     * @Route("/reset-password", name="reset_password")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function resetPassword(Request $request): Response
    {
        return new Response();
    }
}
