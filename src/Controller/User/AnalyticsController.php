<?php

declare(strict_types=1);

namespace App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur", name="user_")
 */
class AnalyticsController extends AbstractController
{
    /**
     * @Route("/", name="dashboard")
     *
     * @return Response
     */
    public function index()
    {
        return $this->render('user/index.html.twig');
    }
}
