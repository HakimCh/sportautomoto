<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\PostDeal;
use App\Services\Crud;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur/order", name="user_order_")
 */
class OrdersController extends AbstractController
{
    /**
     * @Route(
     *     "/{pageName}/{page}",
     *     defaults={"pageName" = "page", "page" = 1},
     *     requirements={"pageName" = "page", "page" = "[\d]+"},
     *     name="list"
     * )
     *
     * @param Paginate $paginate
     *
     * @return Response
     */
    public function list(Paginate $paginate): Response
    {
        return new Response();
    }

    /**
     * @Route("/add/{id}", name="add")
     *
     * @param PostDeal $deal
     * @param Crud $crud
     *
     * @return Response
     */
    public function add(PostDeal $deal, Crud $crud)
    {
        return new Response();
    }
}
