<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\EntityInterface;
use App\Entity\Post;
use App\Entity\PostDeal;
use App\Exceptions\UploaderExtensionException;
use App\Form\DealType;
use App\Repository\PostDealRepository;
use App\Services\Crud;
use Exception;
use HakimCh\PaginateBundle\Services\Paginate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur/deal", name="user_deal_")
 */
class DealsController extends AbstractController
{
    /**
     * @var PostDealRepository
     */
    private $repository;

    public function __construct(PostDealRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route(
     *     "/{pageName}/{page}",
     *     defaults={"pageName" = "page", "page" = 1},
     *     requirements={"pageName" = "page", "page" = "[\d]+"},
     *     name="list"
     * )
     *
     * @param Paginate $paginate
     *
     * @return Response
     */
    public function list(Paginate $paginate): Response
    {
        $query = $this->repository->findByCriteria([
            'userId' => $this->getUser()->getId(),
        ]);

        return $this->render('user/deal/index.html.twig', [
            'paginate' => $paginate->setItemsPerPage(50)->make($query),
        ]);
    }

    /**
     * @Route("/add", name="add")
     *
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     *
     * @return Response
     */
    public function add(Request $request, Crud $crud)
    {
        $crud
            ->setEntity(new PostDeal(), PostDeal::TYPE_LABEL)
            ->createForm(DealType::class)
            ->onPostFlush(function (EntityInterface $deal) {
                $this->redirectToRoute(
                    'user_deal_update',
                    ['id' => $deal->getId()]
                );
            })
            ->persist($request);

        return $crud->render('user/deal/save.html.twig');
    }

    /**
     * @Route("/update/{id}", name="update")
     *
     * @param int     $id
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws UploaderExtensionException
     * @throws Exception
     *
     * @return Response
     */
    public function update(int $id, Request $request, Crud $crud): Response
    {
        $deal = $this->getUserDealById($id);
        $crud
            ->setEntity($deal, PostDeal::TYPE_LABEL)
            ->createForm(DealType::class)
            ->onPostFlush(function (EntityInterface $deal) {
                $this->redirectToRoute(
                    'user_deal_update',
                    ['id' => $deal->getId()]
                );
            })
            ->update($request);

        return $crud->render('user/deal/save.html.twig');
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param int     $id
     * @param Request $request
     * @param Crud    $crud
     *
     * @throws Exception
     *
     * @return Response
     */
    public function remove(int $id, Request $request, Crud $crud): Response
    {
        $deal = $this->getUserDealById($id);

        return $crud
            ->setEntity($deal, PostDeal::TYPE_LABEL)
            ->onPostFlush(function () {
                return $this->redirectToRoute('user_deal_list');
            })
            ->delete($request);
    }

    /**
     * @Route("/archive/{id}", name="archive", requirements={"action" = "archive|publish"})
     *
     * @param int $id
     *
     * @throws Exception
     *
     * @return Response
     */
    public function archive(int $id): Response
    {
        $this->getUserDealById($id)->setStatus(Post::STATUS_ARCHIVE);
        $this->addFlash('success', 'Votre deal a été archivé');

        return $this->redirectToRoute('user_deal_list');
    }

    /**
     * @Route("/publish/{id}", name="publish", requirements={"action" = "archive|publish"})
     *
     * @param int $id
     *
     * @throws Exception
     *
     * @return Response
     */
    public function publish(int $id): Response
    {
        $this->getUserDealById($id)->setStatus(Post::STATUS_PENDING);
        $this->addFlash('success', "Votre deal a été mise en attente pour la validation d'un modérateur");

        return $this->redirectToRoute('user_deal_list');
    }

    /**
     * @param int $id
     *
     * @throws Exception
     *
     * @return PostDeal
     */
    private function getUserDealById(int $id): PostDeal
    {
        $deal = $this->repository->findOneBy([
            'user' => $this->getUser(),
            'id' => $id,
        ]);
        if (!$deal) {
            throw new Exception(sprintf(
                'Fatal Error: User (id: %d) not allowed to update this deal (id: %d)',
                $this->getUser()->getId(),
                $id
            ));
        }

        return $deal;
    }
}
