<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Form\UserProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur", name="user_")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/profil", name="profile")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function profile(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // process data
        }

        return $this->render('user/profil.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
