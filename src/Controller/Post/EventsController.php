<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Entity\PostEvent;
use App\Normalizers\EventNormalizer;
use App\Repository\CategoryRepository;
use HakimCh\SeoBundle\Exceptions\ParameterNormalizerNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/evenements", name="events_")
 */
class EventsController extends AbstractPostController
{
    /**
     * @var string
     */
    protected $entityClassName = PostEvent::class;
    /**
     * @var string
     */
    protected $normalizer = EventNormalizer::class;
    /**
     * @var int
     */
    protected $itemsPerPage = PostEvent::MAX_POST_PER_PAGE;

    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="home")
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function list(): Response
    {
        return $this->render(
            'events/index.html.twig',
            $this->generateListParameters()->toArray()
        );
    }

    /**
     * @Route("/categorie/{category}/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, name="category", requirements={"category" = "[\w-]+", "pageName" = "page", "page" = "[\d]+"})
     *
     * @param string $category
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function listByCategory(string $category): Response
    {
        return $this->render(
            'events/index.html.twig',
            $this->generateListParameters(['category' => $category])->toArray()
        );
    }

    /**
     * @Route("/{category}/{slug}/{id}", name="detail", requirements={"id" = "[\d]+"})
     *
     * @param int                $id
     * @param CategoryRepository $categoryRepository
     *
     * @return Response
     */
    public function show(int $id, CategoryRepository $categoryRepository): Response
    {
        $response = $this->render(
            'events/show.html.twig',
            $this->generateShowParameters($id, $categoryRepository)->toArray()
        );
        $response->setSharedMaxAge(3600);
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }
}
