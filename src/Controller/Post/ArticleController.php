<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Entity\PostArticle;
use App\Entity\User;
use App\Normalizers\ArticleNormalizer;
use App\Repository\CategoryRepository;
use HakimCh\SeoBundle\Exceptions\ParameterNormalizerNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/blog", name="blog_")
 */
class ArticleController extends AbstractPostController
{
    /**
     * @var string
     */
    protected $entityClassName = PostArticle::class;
    /**
     * @var string
     */
    protected $normalizer = ArticleNormalizer::class;
    /**
     * @var int
     */
    protected $itemsPerPage = PostArticle::MAX_POST_PER_PAGE;

    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="home")
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function list(): Response
    {
        return $this->render(
            'posts/index.html.twig',
            $this->generateListParameters()->toArray()
        );
    }

    /**
     * @Route("/categorie/{category}/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, name="category", requirements={"category" = "[\w-]+", "pageName" = "page", "page" = "[\d]+"})
     *
     * @param string $category
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function listByCategory(string $category): Response
    {
        return $this->render(
            'posts/index.html.twig',
            $this->generateListParameters(['category' => $category])->toArray()
        );
    }

    /**
     * @Route("/auteur/{id}/{slug}/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"id" = "[\d]+", "pageName" = "page", "page" = "[\d]+"}, name="author")
     *
     * @param User $user
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function listByUser(User $user): Response
    {
        return $this->render(
            'posts/index.html.twig',
            $this->generateListParameters([
                'user' => $user->getTitle(),
                'userId' => $user->getId(),
            ])->toArray()
        );
    }

    /**
     * @Route("/{category}/{slug}/{id}", name="detail", requirements={"id" = "[\d]+"})
     *
     * @param int                $id
     * @param CategoryRepository $categoryRepository
     *
     * @return Response
     */
    public function show(int $id, CategoryRepository $categoryRepository): Response
    {
        $response = $this->render(
            'posts/show.html.twig',
            $this->generateShowParameters($id, $categoryRepository)->toArray()
        );
        $response->setSharedMaxAge(3600);
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }
}
