<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Repository\CategoryRepository;
use App\Repository\PostRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use HakimCh\PaginateBundle\Services\Paginate;
use HakimCh\SeoBundle\Exceptions\ParameterNormalizerNotFoundException;
use HakimCh\SeoBundle\Services\ParametersBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AbstractPostController extends AbstractController
{
    /**
     * @var string
     */
    protected $postType = 'article';
    /**
     * @var string
     */
    protected $normalizer;
    /**
     * @var int
     */
    protected $itemsPerPage;
    /**
     * @var Paginate
     */
    protected $paginate;
    /**
     * @var PostRepositoryInterface
     */
    protected $repository;
    /**
     * @var ParametersBuilder
     */
    protected $parametersBuilder;
    /**
     * @var string
     */
    protected $entityClassName;

    /**
     * AbstractPostController constructor.
     *
     * @param Paginate               $paginate
     * @param EntityManagerInterface $em
     */
    public function __construct(Paginate $paginate, EntityManagerInterface $em)
    {
        $this->paginate = $paginate;
        $this->parametersBuilder = new ParametersBuilder($this->normalizer);
        $this->repository = $em->getRepository($this->entityClassName);
    }

    /**
     * @param int                $id
     * @param CategoryRepository $categoryRepository
     *
     * @return ParametersBuilder
     */
    protected function generateShowParameters(int $id, CategoryRepository $categoryRepository): ParametersBuilder
    {
        $post = $this->repository->findOneById($id);

        return $this->parametersBuilder
            ->buildFromEntity($post)
            ->addData('relatedPosts', $this->repository->findRelatedItems($post->getSlug(), $id))
            ->addData('topPosts', $this->repository->findTopItems('count'))
            ->addData('recentPosts', $this->repository->findTopItems('createdAt'))
            ->addData('categories', $categoryRepository->listWithCount($this->postType));
    }

    /**
     * @param array $criteria
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return ParametersBuilder
     */
    protected function generateListParameters($criteria = []): ParametersBuilder
    {
        $paginate = $this->paginate
            ->setItemsPerPage($this->itemsPerPage)
            ->make($this->repository->findByCriteria($criteria));

        return $this->parametersBuilder
            ->buildFromGlobal($criteria)
            ->addData('paginate', $paginate);
    }
}
