<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Entity\PostDeal;
use App\Entity\User;
use App\Normalizers\DealNormalizer;
use App\Repository\CategoryRepository;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/deals", name="deals_")
 */
class DealsController extends AbstractPostController
{
    /**
     * @var string
     */
    protected $entityClassName = PostDeal::class;
    /**
     * @var string
     */
    protected $normalizer = DealNormalizer::class;
    /**
     * @var string
     */
    protected $postType = 'deal';
    /**
     * @var int
     */
    protected $itemsPerPage = PostDeal::MAX_POST_PER_PAGE;

    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page", "page" = "[\d]+"}, name="home")
     *
     * @throws Exception
     *
     * @return Response
     */
    public function list(): Response
    {
        return $this->render(
            'deals/index.html.twig',
            $this->generateListParameters()->toArray()
        );
    }

    /**
     * @Route("/categorie/{category}/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, name="category", requirements={"category" = "[\w-]+", "pageName" = "page", "page" = "[\d]+"})
     *
     * @param string $category
     *
     * @throws Exception
     *
     * @return Response
     */
    public function listByCategory(string $category): Response
    {
        return $this->render(
            'deals/index.html.twig',
            $this->generateListParameters(['category' => $category])->toArray()
        );
    }

    /**
     * @Route("/ville/{city}/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, name="city", requirements={"category" = "[\w-]+", "pageName" = "page", "page" = "[\d]+"})
     *
     * @param string $city
     *
     * @throws Exception
     *
     * @return Response
     */
    public function listByCity(string $city): Response
    {
        return $this->render(
            'deals/index.html.twig',
            $this->generateListParameters(['city' => $city])->toArray()
        );
    }

    /**
     * @Route("/auteur/{id}/{slug}/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, name="author", requirements={"id" = "[\d]+", "pageName" = "page", "page" = "[\d]+"})
     *
     * @param User $user
     *
     * @throws Exception
     *
     * @return Response
     */
    public function listByUser(User $user): Response
    {
        return $this->render(
            'deals/index.html.twig',
            $this->generateListParameters([
                'user' => $user->getTitle(),
                'userId' => $user->getId(),
            ])->toArray()
        );
    }

    /**
     * @Route("/{city}/{category}/{slug}/{id}", name="detail", requirements={"id" = "[\d]+"})
     *
     * @param int                $id
     * @param CategoryRepository $categoryRepository
     *
     * @return Response
     */
    public function show(int $id, CategoryRepository $categoryRepository): Response
    {
        $parameters = $this->generateShowParameters($id, $categoryRepository)->toArray();

        $response = $this->render('deals/show.html.twig', $parameters);
        $response->setSharedMaxAge(3600);
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }
}
