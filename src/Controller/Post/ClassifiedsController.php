<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Entity\PostClassified;
use App\Form\ClassifiedSearchType;
use App\Normalizers\ClassifiedNormalizer;
use App\Repository\CategoryRepository;
use Exception;
use HakimCh\SeoBundle\Exceptions\ParameterNormalizerNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/annonces", name="classified_")
 */
class ClassifiedsController extends AbstractPostController
{
    /**
     * @var string
     */
    protected $entityClassName = PostClassified::class;
    /**
     * @var string
     */
    protected $normalizer = ClassifiedNormalizer::class;
    /**
     * @var string
     */
    protected $postType = 'classified';
    /**
     * @var int
     */
    protected $itemsPerPage = PostClassified::MAX_POST_PER_PAGE;

    /**
     * @Route("/", name="home")
     *
     * @param Request $request
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function list(Request $request): Response
    {
        $form = $this->createForm(ClassifiedSearchType::class);
        $form->handleRequest($request);
        $criteria = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $criteria = $form->getData();
        }
        $parametersBuilder = $this->generateListParameters($criteria)
            ->addData('form', $form->createView());

        return $this->render('classified/index.html.twig', $parametersBuilder->toArray());
    }

    /**
     * @Route("/categorie/{category}", name="by_category", requirements={"category" = "[\w-]+"})
     *
     * @param string  $category
     * @param Request $request
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function listByCategory(string $category, Request $request): Response
    {
        $form = $this->createForm(ClassifiedSearchType::class);
        $form->handleRequest($request);
        $criteria = ['category' => $category];
        if ($form->isSubmitted() && $form->isValid()) {
            $criteria += $form->getData();
        }
        $parametersBuilder = $this->generateListParameters($criteria)
            ->addData('form', $form->createView());

        return $this->render('classified/index.html.twig', $parametersBuilder->toArray());
    }

    /**
     * @Route("/ville/{city}", name="by_city")
     *
     * @param string  $city
     * @param Request $request
     *
     * @throws Exception
     *
     * @return Response
     */
    public function listByCity(string $city, Request $request): Response
    {
        $form = $this->createForm(ClassifiedSearchType::class);
        $form->handleRequest($request);
        $criteria = ['city_slug' => $city];
        if ($form->isSubmitted() && $form->isValid()) {
            $criteria += $form->getData();
        }
        $parametersBuilder = $this->generateListParameters($criteria)
            ->addData('form', $form->createView());

        return $this->render('classified/index.html.twig', $parametersBuilder->toArray());
    }

    /**
     * @Route("/{city}/{category}/{slug}/{id}", name="detail")
     *
     * @param int                $id
     * @param CategoryRepository $categoryRepository
     *
     * @return Response
     */
    public function show(int $id, CategoryRepository $categoryRepository): Response
    {
        $parameters = $this->generateShowParameters($id, $categoryRepository)
            ->toArray();
        $response = $this->render('classified/show.html.twig', $parameters);
        $response->setSharedMaxAge(3600);
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }
}
