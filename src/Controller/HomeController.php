<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\CustomerRepository;
use App\Repository\PostArticleRepository;
use App\Repository\PostEventRepository;
use App\Repository\PubCampaignsRepository;
use HakimCh\SeoBundle\Services\ParametersBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var bool
     */
    private $debug = true;

    /**
     * @Route("/", name="home")
     *
     * @param PostArticleRepository  $articleRepository
     * @param PostEventRepository    $eventRepository
     * @param PubCampaignsRepository $pubCampaignsRepository
     * @param CustomerRepository     $customerRepository
     *
     * @return Response
     */
    public function home(
        PostArticleRepository $articleRepository,
        PostEventRepository $eventRepository,
        PubCampaignsRepository $pubCampaignsRepository,
        CustomerRepository $customerRepository
    ) {
        $parameters = (new ParametersBuilder())
            ->addTitle('Actu Automobile, essais, nouveautés, occasion')
            ->addDescription(sprintf(
                '%s, %s',
                "Retrouvez toute l'actualité auto et moto sur note site à travers des nouveautés",
                "des essais, de l'occasion, du pratique et de l'insolite."
            ))
            ->toArray();
        $parameters['posts'] = $this->getFromCache('home.posts', [$articleRepository, 'findTopItems', []]);
        $parameters['nextEvent'] = $this->getFromCache('home.nextEvent', [$eventRepository, 'nextEvent', []]);
        $parameters['birthday'] = $this->getFromCache('home.birthday', [$customerRepository, 'happyBirthDayToday', []]);
        $parameters['customers'] = $this->getFromCache('home.customers', [$customerRepository, 'random', [5]], 5);
        $this->getPublicity($parameters, $pubCampaignsRepository);

        return $this->render('pages/home.html.twig', $parameters);
    }

    /**
     * @param array                  $parameters
     * @param PubCampaignsRepository $repository
     */
    private function getPublicity(array &$parameters, PubCampaignsRepository $repository): void
    {
        $pubs = $this->getFromCache(
            'home.publicity',
            [
                $repository,
                'findByCode',
                [['HOMEPAGE', 'HOME1POST', 'HOME2POST', 'HOME3POST', 'HOME4POST']],
            ]
        );
        if (!empty($pubs)) {
            foreach ($pubs as $pub) {
                $parameters[$pub->getPub()->getCode()] = $pub;
            }
        }
    }

    /**
     * @param string $id
     * @param array  $callback
     * @param int    $ttl
     *
     * @return mixed|null
     */
    private function getFromCache(string $id, array $callback, int $ttl = 0)
    {
        $cache = new FilesystemAdapter();
        $item = $cache->getItem($id);
        if (!$item->isHit() || $this->debug) {
            [$object, $method, $arguments] = $callback;
            $item->set(\call_user_func_array([$object, $method], $arguments));
            if ($ttl > 0) {
                $item->expiresAfter($ttl * 60);
            }
            $cache->save($item);

            return $item->get();
        }

        return $cache->getItem($id)->get();
    }
}
