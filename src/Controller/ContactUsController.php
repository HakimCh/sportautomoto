<?php

declare(strict_types=1);

namespace App\Controller;

use App\Emails\ContactType;
use App\Entity\Email;
use App\Form\ContactUsType;
use App\Services\Mailer\Mailer;
use App\Services\Mailer\MailerTemplateNotFoundException;
use HakimCh\SeoBundle\Services\ParametersBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\Error;

class ContactUsController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     *
     * @param Request $request
     * @param Mailer  $mailer
     *
     * @throws MailerTemplateNotFoundException
     * @throws Error
     *
     * @return Response
     */
    public function contact(Request $request, Mailer $mailer): Response
    {
        $form = $this->createForm(ContactUsType::class, new Email());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Email $email */
            $email = $form->getData();
            $mailResponse = $mailer
                ->setSubject($email->getSubject())
                ->setSendFrom([$email->getEmail() => $email->getName()])
                ->send(ContactType::class, ['email' => $email]);
            if ($mailResponse) {
                $this->addFlash('danger', "Message n'a pas pu être envoyé, veuillez verifier les champs obligatoires");
            } else {
                $this->addFlash('success', 'Message envoyé avec succès');
            }
        }
        $parameters = (new ParametersBuilder())
            ->addTitle('Contactez-nous')
            ->addData('form', $form->createView())
            ->toArray();

        return $this->render('pages/contact.html.twig', $parameters);
    }
}
