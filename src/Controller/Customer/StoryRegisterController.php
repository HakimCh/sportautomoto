<?php

declare(strict_types=1);

namespace App\Controller\Customer;

use App\Entity\CustomerStory;
use App\Form\CustomerStoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/inscription/{label}", name="register_story_", requirements={"label" = "pilote|officiel"})
 */
class StoryRegisterController extends AbstractCustomerRegisterController
{
    /**
     * @Route("/palmares", name="home")
     *
     * @param string  $label
     * @param Request $request
     *
     * @return Response
     */
    public function list(string $label, Request $request): Response
    {
        $story = new CustomerStory();
        $stories = $this->getStoriesFromSession();
        $customerType = $this->getCustomerTypeByLabel($label);
        $form = $this->createForm(CustomerStoryType::class, $story, ['type' => $customerType]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $story->setId(0);
            $stories[] = $story;
            $this->setStoriesToSession($stories);

            return $this->redirectToRoute('register_story_home', ['label' => $label]);
        }

        return $this->render('customer/register/stories.html.twig', [
            'items' => $stories,
            'form' => $form->createView(),
            'customerType' => $customerType,
            'label' => $label,
        ]);
    }

    /**
     * @Route("/palmares/update/{id}", name="update")
     *
     * @param string  $label
     * @param int     $id
     * @param Request $request
     *
     * @return Response
     */
    public function update(string $label, int $id, Request $request): Response
    {
        $stories = $this->getStoriesFromSession();
        if (!\array_key_exists($id, $stories)) {
            return $this->redirectToRoute('register_story_home', ['label' => $label]);
        }
        $story = $stories[$id];
        $customerType = $this->getCustomerTypeByLabel($label);
        $form = $this->createForm(CustomerStoryType::class, $story, ['type' => $customerType]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $stories[$id] = $story;
            $this->setStoriesToSession($stories);

            return $this->redirectToRoute('register_story_home', ['label' => $label]);
        }

        return $this->render('customer/register/stories.html.twig', [
            'items' => $stories,
            'form' => $form->createView(),
            'customerType' => $customerType,
            'label' => $label,
        ]);
    }

    /**
     * @Route("/palmares/delete/{id}", name="delete")
     *
     * @param string $label
     * @param int    $id
     *
     * @return Response
     */
    public function delete(string $label, int $id): Response
    {
        $stories = $this->getStoriesFromSession();
        if (\array_key_exists($id, $stories)) {
            unset($stories[$id]);
            $this->setStoriesToSession($stories);
        }

        return $this->redirectToRoute('register_story_home', ['label' => $label]);
    }
}
