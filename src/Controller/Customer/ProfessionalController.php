<?php

declare(strict_types=1);

namespace App\Controller\Customer;

use App\Entity\CustomerProfessional;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/annuaire/professionels", name="directory_professional_")
 */
class ProfessionalController extends AbstractCustomerController
{
    /**
     * @var string
     */
    protected $entityClassName = CustomerProfessional::class;
    /**
     * @var string
     */
    protected $customerType = CustomerProfessional::TYPE_LABEL;
}
