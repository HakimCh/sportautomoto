<?php

declare(strict_types=1);

namespace App\Controller\Customer;

use App\Entity\Customer;
use App\Entity\CustomerStory;
use App\Traits\CustomerTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractCustomerRegisterController extends AbstractController
{
    use CustomerTrait;

    /**
     * @var SerializerInterface
     */
    protected $serializer;
    /**
     * @var SessionInterface
     */
    protected $session;

    public function __construct(SessionInterface $session, SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $this->session = $session;
    }

    /**
     * @param Customer $customer
     */
    protected function setCustomerToSession(Customer $customer): void
    {
        $customer->setId(0);
        $serializedCustomer = $this->get('serializer')->serialize($customer, 'json', [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
        ]);
        $this->session->set('customer', $serializedCustomer);
    }

    /**
     * @param string|null $label
     *
     * @return Customer|object
     */
    protected function getCustomerFromSession(?string $label = null)
    {
        if (!$this->session->has('customer')) {
            return $this->getEntityByLabel($label);
        }
        $serializedCustomer = $this->session->get('customer');

        return $this->serializer
            ->deserialize($serializedCustomer, $this->getEntityClassNameByLabel($label), 'json', [
                ObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true,
                ObjectNormalizer::SKIP_NULL_VALUES => true,
            ]);
    }

    /**
     * @param CustomerStory[] $stories
     */
    protected function setStoriesToSession(array $stories): void
    {
        $serializedStories = $this->get('serializer')->serialize($stories, 'json', [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
        ]);

        $this->session->set('stories', $serializedStories);
    }

    /**
     * @return CustomerStory[]|object
     */
    protected function getStoriesFromSession(): array
    {
        if (!$this->session->has('stories')) {
            return [];
        }
        $serializedStories = $this->session->get('stories');

        return $this->serializer->deserialize($serializedStories, 'App\\Entity\\CustomerStory[]', 'json');
    }
}
