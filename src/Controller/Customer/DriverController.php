<?php

declare(strict_types=1);

namespace App\Controller\Customer;

use App\Entity\CustomerDriver;
use App\Entity\CustomerStory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/annuaire/pilotes", name="directory_driver_")
 */
class DriverController extends AbstractCustomerController
{
    /**
     * @var string
     */
    protected $entityClassName = CustomerDriver::class;
    /**
     * @var string
     */
    protected $customerType = CustomerDriver::TYPE_LABEL;

    /**
     * @Route("/{username}/{id}", name="show", requirements={"id" = "\d+"})
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id): Response
    {
        if (!$customer = $this->repository->findOneById($id)) {
            throw $this->createNotFoundException(sprintf('%s with id: %d not found', ucfirst(CustomerDriver::TYPE_LABEL), $id));
        }

        $parameters = $this->parametersBuilder
            ->addData('raking', $this->getRaking($customer->getStories()))
            ->buildFromEntity($customer)
            ->toArray();

        return $this->render('customer/directory/driver/show.html.twig', $parameters);
    }

    /**
     * @param CustomerStory[] $stories
     *
     * @return array
     */
    private function getRaking($stories): array
    {
        $raking = ['silver' => 0, 'gold' => 0, 'bronze' => 0];
        foreach ($stories as $story) {
            if ($story->getRaking() < 1 && $story->getRaking() > 3) {
                continue;
            }
            switch ($story->getRaking()) {
                case 1:
                    $raking['gold']++;
                    break;
                case 2:
                    $raking['silver']++;
                    break;
                case 3:
                    $raking['bronze']++;
                    break;
                default:
                    continue;
            }
        }

        return $raking;
    }
}
