<?php

declare(strict_types=1);

namespace App\Controller\Customer;

use App\Entity\CustomerOfficial;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/annuaire/officiels", name="directory_official_")
 */
class OfficialController extends AbstractCustomerController
{
    /**
     * @var string
     */
    protected $entityClassName = CustomerOfficial::class;
    /**
     * @var string
     */
    protected $customerType = CustomerOfficial::TYPE_LABEL;
}
