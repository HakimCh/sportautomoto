<?php

declare(strict_types=1);

namespace App\Controller\Customer;

use App\Normalizers\CustomerNormalizer;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use HakimCh\PaginateBundle\Services\Paginate;
use HakimCh\SeoBundle\Exceptions\ParameterNormalizerNotFoundException;
use HakimCh\SeoBundle\Services\ParametersBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

abstract class AbstractCustomerController extends AbstractController
{
    /**
     * @var string
     */
    protected $customerType;
    /**
     * @var Paginate
     */
    protected $paginate;
    /**
     * @var ObjectRepository
     */
    protected $repository;
    /**
     * @var ParametersBuilder
     */
    protected $parametersBuilder;
    /**
     * @var string
     */
    protected $entityClassName;

    /**
     * AbstractPostController constructor.
     *
     * @param Paginate               $paginate
     * @param EntityManagerInterface $em
     */
    public function __construct(Paginate $paginate, EntityManagerInterface $em)
    {
        $this->paginate = $paginate;
        $this->parametersBuilder = new ParametersBuilder(CustomerNormalizer::class);
        $this->parametersBuilder->addData('customerType', $this->customerType);
        $this->repository = $em->getRepository($this->entityClassName);
    }

    /**
     * @Route("/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, defaults={"pageName" = "page", "page" = 1}, requirements={"pageName" = "page"}, name="list")
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function list(): Response
    {
        return $this->render(
            sprintf('customer/directory/%s/list.html.twig', $this->customerType),
            $this->generateListParameters()->toArray()
        );
    }

    /**
     * @Route("/categorie/{category}/{pageName}/{page}", defaults={"pageName" = "page", "page" = 1}, requirements={"category" = "[\w-]+"}, name="list_by_category")
     *
     * @param string $category
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return Response
     */
    public function listByCategory(string $category): Response
    {
        return $this->render(
            sprintf('customer/directory/%s/list.html.twig', $this->customerType),
            $this->generateListParameters(['category' => $category])
                ->toArray()
        );
    }

    /**
     * @Route("/{username}/{id}", name="show", requirements={"id" = "\d+"})
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id): Response
    {
        if (!$customer = $this->repository->findOneById($id)) {
            throw $this->createNotFoundException(sprintf('%s with id: %d not found', ucfirst($this->customerType), $id));
        }

        $parameters = $this->parametersBuilder
            ->buildFromEntity($customer)
            ->toArray();

        return $this->render(sprintf('customer/directory/%s/show.html.twig', $this->customerType), $parameters);
    }

    /**
     * @param array $criteria
     *
     * @throws ParameterNormalizerNotFoundException
     *
     * @return ParametersBuilder
     */
    protected function generateListParameters($criteria = []): ParametersBuilder
    {
        $paginate = $this->paginate
            ->setItemsPerPage(40)
            ->make($this->repository->findByCriteria($criteria));

        return $this->parametersBuilder
            ->buildFromGlobal($criteria)
            ->addData('paginate', $paginate);
    }
}
