<?php

declare(strict_types=1);

namespace App\Controller\Customer;

use App\Emails\CustomerType;
use App\Entity\Customer;
use App\Entity\CustomerDriver;
use App\Entity\CustomerProfessional;
use App\Entity\CustomerSubscription;
use App\Entity\Plan;
use App\Services\Mailer\Mailer;
use App\Services\Mailer\MailerTemplateNotFoundException;
use App\Traits\PicturesTrait;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use HakimCh\UploaderBundle\Services\ImageUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Error\Error;

/**
 * @Route("/inscription/{label}", name="register_customer_", requirements={"label" = "pilote|officiel|professionel"})
 */
class CustomerRegisterController extends AbstractCustomerRegisterController
{
    use PicturesTrait;

    /**
     * @var ImageUploader
     */
    protected $uploader;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Mailer
     */
    private $mailer;

    public function __construct(ImageUploader $uploader, Mailer $mailer, EntityManagerInterface $em, SessionInterface $session, SerializerInterface $serializer)
    {
        parent::__construct($session, $serializer);
        $this->uploader = $uploader;
        $this->mailer = $mailer;
        $this->em = $em;
    }

    /**
     * @Route("/", name="page")
     *
     * @param string  $label
     * @param Request $request
     *
     * @return Response
     */
    public function profile(string $label, Request $request): Response
    {
        $customer = $this->getCustomerFromSession($label);
        $form = $this->createForm($this->getCustomerFormTypeByLabel($label), $customer);
        $form->handleRequest($request);
        $pictures = $this->session->get('pictures', []);
        if ($form->isSubmitted() && $form->isValid()) {
            $customer->setPictures($pictures);
            $this->setCustomerToSession($customer);
            if (CustomerProfessional::TYPE_LABEL === $customer->getType()) {
                return $this->redirectToRoute('register_customer_confirmation', ['label' => $label]);
            }

            return $this->redirectToRoute('register_story_home', ['label' => $label]);
        }

        return $this->render(sprintf(
            'customer/register/%s.html.twig',
            $customer->getType()
        ), [
            'form' => $form->createView(),
            'pictures' => $this->getPictures($pictures),
            'customerType' => $customer->getType(),
        ]);
    }

    /**
     * @Route("/confirmation", name="confirmation")
     *
     * @param string $label
     *
     * @throws Error
     * @throws MailerTemplateNotFoundException
     * @throws NoResultException
     * @throws NonUniqueResultException
     *
     * @return Response
     */
    public function confirmation(string $label): Response
    {
        if (!$this->session->has('customer')) {
            return $this->redirectToRoute('register_customer_page', ['label' => $label]);
        }
        $transactionCode = strtoupper(uniqid());
        /** @var CustomerDriver $customer */
        $customer = $this->getCustomerFromSession($label);
        $this->addStories($customer);
        $this->uploadPictures($customer);
        $this->addSubscription($customer, $transactionCode);
        $this->sendNotificationByMail($customer);
        $this->em->persist($customer);
        $this->em->flush();
        $this->session->clear();

        return $this->render('customer/register/confirmation.html.twig', [
            'transactionCode' => $transactionCode,
        ]);
    }

    /**
     * @param Customer $customer
     * @param string   $code
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    private function addSubscription(Customer $customer, string $code): void
    {
        $plan = $this->em
            ->getRepository(Plan::class)
            ->findByType($customer->getType());
        $subscription = new CustomerSubscription();
        $subscription->setPlan($plan);
        $subscription->setAmount($plan->getPrice());
        $subscription->setCode($code);

        $customer->addSubscription($subscription);
    }

    /**
     * @param Customer $customer
     */
    private function addStories(Customer $customer): void
    {
        $stories = $this->getStoriesFromSession();
        if (!is_iterable($stories)) {
            return;
        }
        foreach ($stories as $story) {
            $customer->addStory($story);
        }
    }

    /**
     * @param Customer $customer
     */
    private function uploadPictures(Customer $customer): void
    {
        foreach ($customer->getPictures() as $key => $picture) {
            try {
                $this->uploader
                    ->open($picture)
                    ->upload($customer->getType());
            } catch (Exception $e) {
                continue;
            }
        }
    }

    /**
     * @param Customer $customer
     *
     * @throws Error
     * @throws MailerTemplateNotFoundException
     */
    private function sendNotificationByMail(Customer $customer): void
    {
        if (!$customer->getEmail()) {
            return;
        }
        /** @var CustomerSubscription $subscription */
        $subscription = $customer->getSubscriptions()->get(0);

        $this->mailer
            ->setSendTo($customer->getRecipient())
            ->send(CustomerType::class, [
                'transactionCode' => $subscription->getCode(),
                'transactionAmount' => $subscription->getAmount(),
            ]);
    }
}
