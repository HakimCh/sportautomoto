<?php

declare(strict_types=1);

namespace App\Command;

use App\Repository\CategoryRepository;
use App\Repository\CustomerStoriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MergeDuplicatedCategoriesCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var CustomerStoriesRepository
     */
    private $storiesRepository;

    public function __construct(
        EntityManagerInterface $manager,
        CategoryRepository $categoryRepository,
        CustomerStoriesRepository $storiesRepository,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->manager = $manager;
        $this->categoryRepository = $categoryRepository;
        $this->storiesRepository = $storiesRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        parent::configure();

        $this
            ->setName('app:meta:duplicate')
            ->setDescription('Remove duplicated metas.')
            ->setHelp('This command allows you to remove duplicated metas...');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $categories = $this->categoryRepository->findDuplicatedCategories();
        $stories = $this->storiesRepository->findAll();

        $progressBar = new ProgressBar($output, \count($categories));

        foreach ($categories as $identifiers) {
            $identifiers = explode(',', current($identifiers));
            $categoryId = current($identifiers);
            array_shift($identifiers);
            $category = $this->categoryRepository->find($categoryId);
            foreach ($stories as $story) {
                if (!\in_array($category->getId(), $identifiers)) {
                    continue;
                }
                $story->setCategory($category);
                $this->manager->persist($story);
            }
            foreach ($identifiers as $identifier) {
                $category = $this->categoryRepository->find($identifier);
                $this->manager->remove($category);
            }
            $this->manager->flush();

            //$this->categoryRepository->unify($categoryId, $identifiers);
            //$this->storiesRepository->unify($categoryId, $identifiers);
            //$this->categoryRepository->clean($identifiers);

            $progressBar->advance();
        }

        $progressBar->finish();
    }
}
