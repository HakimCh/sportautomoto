<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Category;
use App\Entity\CustomerDriver;
use App\Entity\CustomerStory;
use App\Form\DataTransformer\CategoryTransformer;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerStoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $type = $options['type'];

        $builder
            ->add('title', TextType::class, ['label' => 'title'])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => 'meta',
                'choice_label' => 'title',
                'query_builder' => function (CategoryRepository $repo) use ($type) {
                    return $repo->getQueryMetaByType($type);
                },
            ])
            ->add('year', TextType::class, ['label' => 'year']);

        /*
        $builder
            ->get('category')
            ->addModelTransformer(new CategoryTransformer());
        */

        if (CustomerDriver::TYPE_LABEL === $type) {
            $builder
                ->add('vehicle', TextType::class, ['label' => 'vehicle'])
                ->add('victories', TextType::class, ['label' => 'wins'])
                ->add('raking', TextType::class, ['label' => 'raking']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired('type')
            ->setAllowedTypes('type', 'string')
            ->setDefaults([
                'data_class' => CustomerStory::class,
                'attr' => ['novalidate' => 'novalidate'],
                'translation_domain' => 'form',
            ]);
    }
}
