<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Category;
use App\Entity\CustomerDriver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomerDriverType extends AbstractCustomerType
{
    protected $dataClass = CustomerDriver::class;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $field = $builder->get('categories');

        $builder
            ->add('birthAt', BirthdayType::class, ['widget' => 'single_text', 'label' => 'birthday'])
            ->add('firstName', TextType::class, ['label' => 'firstName'])
            ->add('lastName', TextType::class, ['label' => 'lastName'])
            ->add('job', TextType::class, ['label' => 'job'])
            ->add('club', TextType::class, ['label' => 'club'])
            ->add('vehicle', TextType::class, ['label' => 'vehicle'])
            ->add('categories', EntityType::class, array_replace(
                $field->getOptions(),
                [
                    'compound' => true,
                    'multiple' => true,
                    'expanded' => true,
                    'attr' => ['id' => 'categories'],
                    'group_by' => function (Category $category) {
                        $entity = $category->getParent() ?? $category;

                        return $entity->getTitle();
                    },
                ]
            ));
    }
}
