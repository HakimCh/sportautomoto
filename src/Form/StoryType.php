<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, ['label' => 'title', 'mapped' => false])
            ->add('preciser', TextType::class, ['label' => 'category', 'mapped' => false])
            ->add('meta', HiddenType::class, ['mapped' => false])
            ->add('vehicle', TextType::class, ['label' => 'vehicle', 'mapped' => false])
            ->add('year', TextType::class, ['label' => 'year', 'mapped' => false])
            ->add('victory', TextType::class, ['label' => 'wins', 'mapped' => false])
            ->add('result', TextType::class, ['label' => 'raking', 'mapped' => false]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'translation_domain' => 'form',
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }
}
