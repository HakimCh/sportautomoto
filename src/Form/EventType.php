<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\PostEvent;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;

class EventType extends PostType
{
    protected $dataClass = PostEvent::class;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $now = (new DateTime())->format('Y-m-dTH:i');

        $builder
            ->add('startAt', DateTimeType::class, [
                'label' => 'start start',
                'widget' => 'single_text',
                'attr' => ['value' => $now],
            ])
            ->add('endAt', DateTimeType::class, [
                'label' => 'end date',
                'widget' => 'single_text',
                'attr' => ['value' => $now],
            ])
            ->remove('category');
    }
}
