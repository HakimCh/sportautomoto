<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\City;
use App\Entity\PostDeal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class DealType extends PostType
{
    /**
     * @var string
     */
    protected $dataClass = PostDeal::class;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('price', TextType::class, ['label' => 'price'])
            ->add('discount', TextType::class, ['label' => 'discount'])
            ->add('address', TextType::class, ['label' => 'address'])
            ->add('minimumRequirement', TextType::class, [
                'label' => 'min participants',
            ])
            ->add('maximumRequirement', TextType::class, [
                'label' => 'max participants',
            ])
            ->add('startAt', DateType::class, [
                'label' => 'start date',
                'widget' => 'single_text',
            ])
            ->add('endAt', DateType::class, [
                'label' => 'end date',
                'widget' => 'single_text',
            ])
            ->add('city', EntityType::class, [
                'label' => 'city',
                'class' => City::class,
                'choice_label' => 'title',
            ]);
    }
}
