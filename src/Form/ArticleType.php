<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\PostArticle;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;

class ArticleType extends PostType
{
    protected $dataClass = PostArticle::class;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('createdAt', DateTimeType::class, [
                'mapped' => false,
                'label' => 'publish date',
                'widget' => 'single_text',
                'attr' => ['value' => (new DateTime())->format('Y-m-dTH:i')],
            ])
            ->add('isSticky', ChoiceType::class, [
                'label' => 'sticky',
                'choices' => ['no' => 0, 'yes' => 1],
            ]);
    }
}
