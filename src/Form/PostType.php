<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Category;
use App\Entity\Post;
use App\Form\Type\TemporaryFileType;
use App\Repository\CategoryRepository;
use ReflectionClass;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class PostType extends AbstractType
{
    protected $dataClass;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, ['label' => 'title'])
            ->add('content', TextareaType::class, [
                'label' => 'content',
                'attr' => ['class' => 'wysiwyg'],
            ])
            ->add('photo1', TemporaryFileType::class)
            ->add('photo2', FileType::class, ['mapped' => false])
            ->add('photo3', FileType::class, ['mapped' => false])
            ->add('photo4', FileType::class, ['mapped' => false])
            ->add('status', ChoiceType::class, [
                'label' => 'status',
                'choices' => [
                  Post::STATUS_PUBLISH => Post::STATUS_PUBLISH,
                  Post::STATUS_PENDING => Post::STATUS_PENDING,
                  Post::STATUS_ARCHIVE => Post::STATUS_ARCHIVE,
                  Post::STATUS_DRAFT => Post::STATUS_DRAFT,
                ],
            ])
            ->add('isOpenToDiscussion', ChoiceType::class, [
                'label' => 'is open for discussion',
                'choices' => ['no' => 0, 'yes' => 1],
            ])
            ->add('category', EntityType::class, [
                'label' => 'meta',
                'class' => Category::class,
                'query_builder' => function (CategoryRepository $repo) {
                    $reflexion = new ReflectionClass($this->dataClass);

                    return $repo->getQueryMetaByType($reflexion->getConstant('TYPE_LABEL'));
                },
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => $this->dataClass,
                'attr' => ['novalidate' => 'novalidate'],
                'translation_domain' => 'form',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
