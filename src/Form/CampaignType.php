<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\PubCampaigns;
use App\Form\Type\TemporaryFileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampaignType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'title'])
            ->add('link', TextType::class, ['label' => 'link'])
            ->add('image', TemporaryFileType::class)
            ->add('status', ChoiceType::class, [
                'choices' => [
                    PubCampaigns::STATUS_ONGOING => PubCampaigns::STATUS_ONGOING,
                    PubCampaigns::STATUS_PENDING => PubCampaigns::STATUS_PENDING,
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PubCampaigns::class,
            'attr' => ['novalidate' => 'novalidate'],
            'translation_domain' => 'form',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
