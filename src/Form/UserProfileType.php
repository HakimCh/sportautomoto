<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, ['label' => 'firstName'])
            ->add('lastName', TextType::class, ['label' => 'lastName'])
            ->add('birth_at', BirthdayType::class, [
                'label' => 'birthday',
                'widget' => 'single_text',
            ])
            ->add('civility', ChoiceType::class, [
                'label' => 'civility',
                'choices' => ['mr.' => 'M', 'mrs.' => 'F'],
            ])
            ->add('headline', TextareaType::class, [
                'label' => 'content',
                'attr' => ['class' => 'wysiwyg-light'],
            ])
            ->add('newsletter', CheckboxType::class, ['label' => 'receive newsletters']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => ['novalidate' => 'novalidate'],
            'translation_domain' => 'form',
            'data_class' => User::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
