<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\City;
use App\Entity\Make;
use App\Entity\PostClassified;
use App\Repository\CityRepository;
use App\Repository\MakeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassifiedSearchType extends AbstractType
{
    /**
     * @var array
     */
    private $data;

    public function __construct(RequestStack $requestStack)
    {
        $this->data = $requestStack->getCurrentRequest()->request->all();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'auto or moto ?',
                'choices' => ['auto' => 'auto', 'moto' => 'moto'],
                'attr' => ['data-chained-child' => '#brand'],
            ])
            ->add('brand', ChoiceType::class, [
                'label' => 'all makes',
                'attr' => ['data-chained-child' => '#model'],
            ])
            ->add('model', ChoiceType::class, ['label' => 'all models'])
            ->add('engine', ChoiceType::class, [
                'label' => 'fuel type',
                'choices' => array_flip(PostClassified::ENERGY_TYPES),
            ])
            ->add('year', ChoiceType::class, [
                'label' => 'model year',
                'attr' => ['class' => 'sp-grid-view sp-grid-6'],
                'choices' => $this->getDatesChoices(),
            ])
            ->add('year', ChoiceType::class, [
                'label' => 'model year',
                'attr' => ['class' => 'sp-grid-view sp-grid-6'],
                'choices' => $this->getDatesChoices(),
            ])
            ->add('city', EntityType::class, [
                'label' => 'location',
                'class' => City::class,
                'choice_label' => 'title',
                'query_builder' => function (CityRepository $repo) {
                    return $repo->getQueryBuilder();
                },
            ])
            ->add('min_price', TextType::class, ['label' => 'min price'])
            ->add('max_price', TextType::class, ['label' => 'max price']);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            if (isset($data['type'])) {
                $this->addElements('brand', 'all makes', ['type' => $this->data['type']], $event->getForm());
            }
            if (isset($data['brand'])) {
                $this->addElements('model', 'all models', ['parent' => $this->data['brand']], $event->getForm());
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'attr' => ['novalidate' => 'novalidate'],
                'translation_domain' => 'form',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return null;
    }

    /**
     * @param string        $name
     * @param string        $label
     * @param array         $criteria
     * @param FormInterface $form
     */
    protected function addElements(string $name, string $label, array $criteria, FormInterface $form): void
    {
        $form->remove($name);
        $form->add($name, ChoiceType::class, [
            'label' => $label,
            'class' => Make::class,
            'query_builder' => function (MakeRepository $repo) use ($criteria) {
                return $repo->getQueryBuilderByType($criteria);
            },
        ]);
    }

    /**
     * @return array
     */
    private function getDatesChoices(): array
    {
        $years = [];
        for ($year = date('Y'); $year > 1980; --$year) {
            $years[$year] = $year;
        }
        $years['1980 ou plus ancien'] = 1980;

        return $years;
    }
}
