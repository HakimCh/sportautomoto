<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\DataTransformerInterface;

class CategoriesTransformer implements DataTransformerInterface
{
    /**
     * No need to transform.
     *
     * @param ArrayCollection|Category[] $categories
     *
     * @return string
     */
    public function transform($categories)
    {
        return $categories;
    }

    /**
     * Transforms an object (category) to an array.
     *
     * @param array|Category $categories
     *
     * @return array
     */
    public function reverseTransform($categories)
    {
        if (is_iterable($categories)) {
            return $categories;
        }

        return [$categories];
    }
}
