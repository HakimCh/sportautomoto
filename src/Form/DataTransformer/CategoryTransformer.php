<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Category;
use Symfony\Component\Form\DataTransformerInterface;

class CategoryTransformer implements DataTransformerInterface
{
    /**
     * No need to transform.
     *
     * @param Category|null $value
     *
     * @return string
     */
    public function transform($value)
    {
        if (!$value instanceof Category) {
            return $value;
        }

        return $value->getTitle();
    }

    /**
     * Transforms an object (category) to an array.
     *
     * @param Category|null $value
     *
     * @return mixed
     */
    public function reverseTransform($value)
    {
        return $value;
    }
}
