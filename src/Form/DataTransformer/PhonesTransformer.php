<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class PhonesTransformer implements DataTransformerInterface
{
    /**
     * Transforms an array (phones) to a string.
     *
     * @param array $phones
     *
     * @return string
     */
    public function transform($phones): string
    {
        if (null === $phones) {
            return '';
        }

        return implode(', ', $phones);
    }

    /**
     * Transforms a string (phones) to an array.
     *
     * @param string $phones
     *
     * @return array|void
     */
    public function reverseTransform($phones)
    {
        if (!$phones) {
            return;
        }

        return explode(', ', $phones);
    }
}
