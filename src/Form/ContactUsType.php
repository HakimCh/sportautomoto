<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Email;
use HakimCh\ReCaptchaBundle\Form\Type\ReCaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactUsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'alias'])
            ->add('email', TextType::class, ['label' => 'email'])
            ->add('phone', TextType::class, ['label' => 'phone'])
            ->add('subject', ChoiceType::class, [
                'label' => 'subject',
                'choices' => Email::SUBJECTS,
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Votre message',
                'attr' => ['class' => 'wysiwygme'],
            ])
            ->add('captcha', ReCaptchaType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'mapped' => Email::class,
            'translation_domain' => 'form',
        ]);
    }
}
