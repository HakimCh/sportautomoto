<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\CustomerProfessional;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomerProfessionalType extends AbstractCustomerType
{
    protected $dataClass = CustomerProfessional::class;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('firstName', TextType::class, ['label' => 'company name'])
            ->add('address', TextType::class, ['label' => 'address'])
            ->add('birthAt', BirthdayType::class, ['widget' => 'single_text', 'label' => 'createdAt']);
    }
}
