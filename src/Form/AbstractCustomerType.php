<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Category;
use App\Form\DataTransformer\CategoriesTransformer;
use App\Form\DataTransformer\PhonesTransformer;
use App\Form\Type\TemporaryFileType;
use App\Form\Type\UniqueType;
use App\Repository\CategoryRepository;
use ReflectionClass;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbstractCustomerType extends AbstractType
{
    /**
     * @var string
     */
    protected $dataClass;
    /**
     * @var PhonesTransformer
     */
    private $phonesTransformer;
    /**
     * @var CategoriesTransformer
     */
    private $categoriesTransformer;

    public function __construct(PhonesTransformer $phonesTransformer, CategoriesTransformer $categoriesTransformer)
    {
        $this->phonesTransformer = $phonesTransformer;
        $this->categoriesTransformer = $categoriesTransformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profile', TemporaryFileType::class, ['label' => 'profile picture'])
            ->add('photo1', TemporaryFileType::class)
            ->add('photo2', FileType::class, ['mapped' => false])
            ->add('photo3', FileType::class, ['mapped' => false])
            ->add('photo4', FileType::class, ['mapped' => false])
            ->add('phones', TextType::class, ['label' => 'phones'])
            ->add('email', UniqueType::class, ['label' => 'email'])
            ->add('content', TextareaType::class, ['label' => 'content', 'attr' => ['class' => 'wysiwyg']])
            ->add('categories', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'title',
                'attr' => ['id' => 'category'],
                'query_builder' => function (CategoryRepository $repo) {
                    $reflexion = new ReflectionClass($this->dataClass);

                    return $repo->getQueryMetaByType($reflexion->getConstant('TYPE_LABEL'));
                },
            ]);

        $builder
            ->get('phones')
            ->addModelTransformer($this->phonesTransformer);

        $builder
            ->get('categories')
            ->addModelTransformer($this->categoriesTransformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => $this->dataClass,
                'attr' => ['novalidate' => 'novalidate'],
                'translation_domain' => 'form',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
