<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Form\Type\UniqueType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserRegisterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', UniqueType::class, ['label' => 'email'])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'password'],
                'second_options' => ['label' => 'repeat the password'],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 8, 'max' => 100]),
                ],
            ])
            ->add('firstName', TextType::class, [
                'label' => 'firstName',
                'constraints' => [new Length(['min' => 3])],
            ])
            ->add('lastName', TextType::class, [
                'label' => 'lastName',
                'constraints' => [new Length(['min' => 3])],
            ])
            ->add('phone', TextType::class, ['label' => 'phone'])
            ->add('newsletter', CheckboxType::class, ['label' => 'receive newsletters'])
            ->add('termsAccepted', CheckboxType::class, [
                'mapped' => false,
                'attr' => ['disabled' => true, 'checked' => true],
                'label' => 'i have read and accept the terms and conditions',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => User::class,
                'attr' => ['novalidate' => 'novalidate'],
                'translation_domain' => 'form',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
