<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\City;
use App\Entity\PostClassified;
use App\Repository\CityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ClassifiedType extends PostType
{
    protected $dataClass = PostClassified::class;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('firstCirculationYear', TextType::class, ['label' => 'model year'])
            ->add('mileage', TextType::class, ['label' => 'mileage'])
            ->add('classifiedModel', TextType::class, [
                'mapped' => false,
                'label' => 'search by term',
            ])
            ->add('model', HiddenType::class, [
                'attr' => ['class' => 'autocomplete-value'],
            ])
            ->add('energy', ChoiceType::class, [
                'label' => 'fuel type',
                'choices' => array_flip(PostClassified::ENERGY_TYPES),
            ])
            ->add('price', TextType::class, ['label' => 'price'])
            ->add('city', EntityType::class, [
                'label' => 'city',
                'class' => City::class,
                'choice_label' => 'title',
                'query_builder' => function (CityRepository $repo) {
                    return $repo->getQueryBuilder();
                },
            ]);
    }
}
