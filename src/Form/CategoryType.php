<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $type = $options['type'];

        $builder
            ->add('title', TextType::class, ['label' => 'title'])
            ->add('type', HiddenType::class, ['attr' => ['value' => $type]])
            ->add('parent', EntityType::class, [
                'class' => Category::class,
                'label' => 'parent category',
                'placeholder' => 'no parent',
                'query_builder' => function (CategoryRepository $repo) use ($type) {
                    return $repo->getParentByType($type, true);
                },
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('type')
            ->setAllowedTypes('type', 'string')
            ->setDefaults([
                'data_class' => Category::class,
                'attr' => ['novalidate' => 'novalidate'],
                'translation_domain' => 'form',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
