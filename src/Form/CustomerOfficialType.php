<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\CustomerOfficial;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomerOfficialType extends AbstractCustomerType
{
    protected $dataClass = CustomerOfficial::class;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('birthAt', BirthdayType::class, ['widget' => 'single_text', 'label' => 'birthday'])
            ->add('firstName', TextType::class, ['label' => 'firstName'])
            ->add('lastName', TextType::class, ['label' => 'lastName'])
            ->add('job', TextType::class, ['label' => 'job'])
            ->add('club', TextType::class, ['label' => 'club']);
    }
}
