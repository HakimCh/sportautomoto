<?php

declare(strict_types=1);

namespace App\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UniqueType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'constraints' => [
                    new Callback(function ($data, ExecutionContextInterface $context) {
                        if ($this->isInUse($context)) {
                            $context
                                ->buildViolation('The email is used before')
                                ->atPath($context->getObject()->getName())
                                ->addViolation();
                        }
                    }),
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return EmailType::class;
    }

    /**
     * @param ExecutionContextInterface $context
     *
     * @return bool
     */
    protected function isInUse(ExecutionContextInterface $context): bool
    {
        $entity = $context->getRoot()->getData();
        if (!$entity->getEmail()) {
            return false;
        }
        $repository = $this->em->getRepository(\get_class($entity));

        return $repository->isExist(
            $entity->getEmail(),
            $entity->getId()
        );
    }
}
