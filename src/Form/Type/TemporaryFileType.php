<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\EntityInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class TemporaryFileType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'mapped' => false,
            'constraints' => [
                new Callback(function ($data, ExecutionContextInterface $context) {
                    $key = $context->getObject()->getName();
                    $entity = $context->getObject()->getParent()->getData();
                    if (!$this->hasPicture($entity, $key)) {
                        $context
                            ->buildViolation('Veuillez selectioner une image')
                            ->atPath($key)
                            ->addViolation();
                    }
                }),
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return FileType::class;
    }

    /**
     * @param EntityInterface $entity
     * @param string          $key
     *
     * @return bool
     */
    private function hasPicture(EntityInterface $entity, string $key): bool
    {
        $storedPictures = [];
        if (method_exists($entity, 'getPictures')) {
            $storedPictures = $entity->getPictures();
        }
        $pictures = array_replace(
            $storedPictures,
            $this->session->get('pictures', [])
        );

        return \array_key_exists($key, $pictures);
    }
}
