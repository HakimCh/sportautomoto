<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupedMetaType extends AbstractTypeExtension implements FormTypeInterface
{
    /**
     * @var bool
     */
    private static $enable = false;
    /**
     * @var string
     */
    private static $userType;

    /**
     * @param string $userType
     * @param bool   $enable
     *
     * @return string
     */
    public static function configure(string $userType, bool $enable = false): string
    {
        self::$userType = $userType;
        self::$enable = $enable;

        return self::class;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        if (!self::$enable) {
            return;
        }

        $resolver->setDefaults([
            'mapped' => false,
            'multiple' => true,
            'expanded' => true,
            'class' => Category::class,
            'query_builder' => function (CategoryRepository $repo) {
                return $repo->findMetasByType('pilote', true);
            },
            'group_by' => function (Category $meta) {
                if ($meta->getParent()) {
                    return $meta->getParent()->getTitle();
                }

                return $meta->getTitle();
            },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return EntityType::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): array
    {
        return [EntityType::class];
    }
}
