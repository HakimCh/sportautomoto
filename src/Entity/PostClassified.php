<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostClassifiedRepository")
 */
class PostClassified extends Post
{
    public const MAX_POST_PER_PAGE = 24;
    public const TYPE_LABEL = 'classified';
    public const ENERGY_TYPES = [
        'DIESEL' => 'diesel',
        'GASOLINE' => 'gasoline',
        'ELECTRIC' => 'electric',
        'GPL' => 'gpl',
    ];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="classifieds")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="classifieds")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank
     * @Assert\GreaterThan(0)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\NotBlank
     */
    private $energy;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\Length(max="4", maxMessage="The year must be at least {{ limit }} characters long")
     */
    private $firstCirculationYear;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\GreaterThan(0)
     */
    private $mileage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="classifieds")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Make", inversedBy="classifieds")
     * @ORM\JoinColumn(nullable=true)
     */
    private $model;

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getEnergy(): string
    {
        return $this->energy;
    }

    public function setEnergy(string $energy): self
    {
        $this->energy = $energy;

        return $this;
    }

    public function getFirstCirculationYear(): int
    {
        return $this->firstCirculationYear;
    }

    public function setFirstCirculationYear(int $firstCirculationYear): self
    {
        $this->firstCirculationYear = $firstCirculationYear;

        return $this;
    }

    public function getMileage(): int
    {
        return $this->mileage;
    }

    public function setMileage(int $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getModel(): ?Make
    {
        return $this->model;
    }

    public function setModel(Make $model): self
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::TYPE_LABEL;
    }
}
