<?php

declare(strict_types=1);

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Email
{
    public const SUBJECTS = [
        'general' => 'general',
        'suggestion' => 'suggestion',
        'technical help' => 'technical help',
        'report a bug' => 'report a bug',
        'report a scam' => 'report a scam',
        'report a driver sheet' => 'report a driver sheet',
        'report an official sheet' => 'report an official sheet',
        'report a professional sheet' => 'report a professional sheet',
    ];
    /**
     * @var string
     * @Assert\NotBlank
     */
    private $name;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var string
     * @Assert\NotBlank
     */
    private $subject;
    /**
     * @var string
     * @Assert\NotBlank
     */
    private $message;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(?string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }
}
