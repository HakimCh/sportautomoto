<?php

declare(strict_types=1);

namespace App\Entity;

use Cocur\Slugify\Slugify;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use HakimCh\SeoBundle\Contracts\SeoEntityInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"event" = "PostEvent", "article" = "PostArticle", "deal" = "PostDeal", "classified" = "PostClassified"})
 * @ORM\HasLifecycleCallbacks
 */
abstract class Post implements EntityInterface, SeoEntityInterface
{
    public const STATUS_PUBLISH = 'PUBLISH';
    public const STATUS_DRAFT = 'DRAFT';
    public const STATUS_ARCHIVE = 'ARCHIVED';
    public const STATUS_PENDING = 'PENDING';

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var User
     */
    protected $user;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(min="5")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @Assert\Length(min="70")
     */
    private $content;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @ORM\Column(type="json")
     */
    private $pictures = [];

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" = 1})
     */
    private $isOpenToDiscussion;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @return string
     */
    abstract public function getType(): string;

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate(): void
    {
        $this->updatedAt = new DateTime();
        $this->slug = Slugify::create()->slugify($this->title);
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist(): void
    {
        $this->count = $this->count ?? 0;
        $this->createdAt = $this->createdAt ?? new DateTime();
        $this->slug = Slugify::create()->slugify($this->title);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPictures(): array
    {
        return $this->pictures ?? [];
    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    public function getPicture(string $key)
    {
        return $this->pictures[$key] ?? null;
    }

    /**
     * @return string|null
     */
    public function getMainPicture()
    {
        return !empty($this->pictures) ? current($this->pictures) : null;
    }

    public function setPictures(array $pictures): self
    {
        $this->pictures = $pictures;

        return $this;
    }

    public function addPictures(array $pictures): self
    {
        $this->pictures = array_replace(
            $this->pictures,
            $pictures
        );

        return $this;
    }

    public function addPicture(string $key, string $value): self
    {
        if (!empty($value)) {
            $this->pictures[$key] = $value;
        }

        return $this;
    }

    public function getIsOpenToDiscussion(): bool
    {
        return $this->isOpenToDiscussion ?? false;
    }

    public function setIsOpenToDiscussion(?bool $isOpenToDiscussion): self
    {
        $this->isOpenToDiscussion = $isOpenToDiscussion;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
