<?php

declare(strict_types=1);

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MakeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Make
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Make", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Make", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     */
    protected $parent;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Choice({"auto", "moto"})
     */
    private $vertical;

    /**
     * @ORM\OneToMany(targetEntity="PostClassified", mappedBy="model")
     */
    private $classifieds;

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function onFlush()
    {
        $this->slug = Slugify::create()->slugify($this->title);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getVertical(): string
    {
        return $this->vertical;
    }

    public function setVertical(string $vertical): self
    {
        $this->vertical = $vertical;

        return $this;
    }

    /**
     * @return Make[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function getParent(): self
    {
        return $this->parent;
    }

    public function setParent(self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return PostClassified[]
     */
    public function getClassifieds()
    {
        return $this->classifieds;
    }

    public function getContent(): string
    {
        return '';
    }
}
