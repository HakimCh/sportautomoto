<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostDealRepository")
 */
class PostDeal extends Post
{
    public const TYPE_LABEL = 'deal';
    public const MAX_POST_PER_PAGE = 24;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="deals")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="deals")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     * @Assert\NotBlank
     * @Assert\GreaterThan(0)
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\GreaterThan(0)
     */
    private $discount;

    /**
     * @ORM\Column(type="text")
     */
    private $address;

    /**
     * @ORM\Column(type="json")
     */
    private $requirement = ['min' => 0, 'max' => 0];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="deals")
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="PostDealOrders", mappedBy="deal")
     */
    private $dealOrders;

    public function __construct()
    {
        $this->dealOrders = new ArrayCollection();
    }

    public function getDiscountedPrice(): ?float
    {
        if (!$this->price) {
            return null;
        }

        return $this->price * (1 - ($this->getDiscount() / 100));
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setRequirement(int $min, int $max = 0): self
    {
        $this->requirement = compact('min', 'max');

        return $this;
    }

    public function getRequirement(): ?array
    {
        return $this->requirement;
    }

    public function getMinimumRequirement(): int
    {
        return $this->requirement['min'] ?? 0;
    }

    public function getMaximumRequirement(): int
    {
        return $this->requirement['max'] ?? 0;
    }

    public function setMinimumRequirement(int $minimum): self
    {
        $this->requirement['min'] = $minimum;

        return $this;
    }

    public function setMaximumRequirement(int $maximum): self
    {
        $this->requirement['max'] = $maximum;

        return $this;
    }

    public function hasMinimumRequirement(): bool
    {
        return $this->getMinimumRequirement() > 0;
    }

    public function hasMaximumRequirement(): bool
    {
        return $this->getMaximumRequirement() > 0;
    }

    public function getStartAt(): ?DateTime
    {
        return $this->startAt;
    }

    public function setStartAt(?DateTime $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?DateTime
    {
        return $this->endAt;
    }

    public function setEndAt(?DateTime $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function isOpen(): bool
    {
        return null === $this->endAt;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection|PostDealOrders[]
     */
    public function getDealOrders()
    {
        return $this->dealOrders;
    }

    public function addDealOrder(PostDealOrders $order): self
    {
        if (!$this->dealOrders->contains($order)) {
            $this->dealOrders[] = $order;
            $order->setDeal($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::TYPE_LABEL;
    }
}
