<?php

declare(strict_types=1);

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Category
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Category", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id", nullable=true)
     */
    protected $parent;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostClassified", mappedBy="category")
     */
    private $classifieds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostArticle", mappedBy="category")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostEvent", mappedBy="category")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity="PostDeal", mappedBy="category")
     */
    private $deals;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Customer", mappedBy="categories")
     */
    private $customers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="categories")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="CustomerStory", mappedBy="category")
     */
    private $stories;

    public function __toString(): string
    {
        return $this->title;
    }

    /**
     * @ORM\PreFlush
     */
    public function preFlush(): void
    {
        $this->slug = Slugify::create()->slugify($this->title);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getContent(): string
    {
        return '';
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUid(): string
    {
        return sprintf(
            '%s:%s',
            $this->slug,
            $this->type
        );
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Category[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return PostClassified[]
     */
    public function getClassifieds()
    {
        return $this->classifieds;
    }

    /**
     * @return Post[]
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @return Customer[]
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return PostDeal[]
     */
    public function getDeals()
    {
        return $this->deals;
    }

    /**
     * @return CustomerStory[]
     */
    public function getStories()
    {
        return $this->stories;
    }

    /**
     * @return PostEvent[]
     */
    public function getEvents()
    {
        return $this->events;
    }
}
