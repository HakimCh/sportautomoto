<?php

declare(strict_types=1);

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Users.
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="json_array", length=255)
     */
    private $roles = ['ROLE_USER'];

    /**
     * @ORM\Column(type="string", length=5, nullable=true, options={"default" = "M"}))
     */
    private $civility;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = "0"})
     */
    private $newsletter;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     "/(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}/",
     *     message="Le mot de passe n'est pas valide. Il doit avoir au minimum 8 caractères, une lettre et un nombre."
     * )
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true, options={"default" = "0"}))
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true))
     */
    private $headline;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Image
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = true})
     */
    private $confirmation;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" = "1970-01-01"})
     */
    private $birthAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" = "1970-01-01"})
     */
    private $connectedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" = "1970-01-01"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" = "1970-01-01"})
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostDealOrders", mappedBy="user")
     */
    private $dealOrders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PubCampaigns", mappedBy="user")
     */
    private $pubCampaigns;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="users", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="user_categories")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostDeal", mappedBy="user")
     */
    private $deals;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostClassified", mappedBy="user")
     */
    private $classifieds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostArticle", mappedBy="user")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostEvent", mappedBy="user")
     */
    private $events;

    /**
     * @var string
     */
    private $salt;

    public function __construct()
    {
        $this->isActive = true;
        $this->categories = new ArrayCollection();
        $this->classifieds = new ArrayCollection();
        $this->deals = new ArrayCollection();
        $this->dealOrders = new ArrayCollection();
        $this->pubCampaigns = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     *
     * @throws \Exception
     */
    public function onUpdate(): void
    {
        $this->updatedAt = new \DateTime();
        $this->username = Slugify::create()->slugify($this->firstName.' '.$this->lastName);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getConfirmation()
    {
        return $this->confirmation;
    }

    /**
     * @param mixed $confirmation
     */
    public function setConfirmation($confirmation): void
    {
        $this->confirmation = $confirmation;
    }

    /**
     * @return mixed
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @param mixed $newsletter
     */
    public function setNewsletter($newsletter): void
    {
        $this->newsletter = $newsletter;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->firstName.' '.$this->lastName;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username): void
    {
        $this->username = $username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function getSaltedPassword($password)
    {
        return $password.$this->email;
    }

    public function setEncodedPassword(BasePasswordEncoder $encoder)
    {
        $this->password = $encoder->encodePassword(
            $this->getSaltedPassword($this->password),
            $this->getSalt()
        );
    }

    public function getSalt()
    {
        return 'aé&scr65+^p$msqµ£+*';
    }

    /**
     * @return mixed
     */
    public function getCivility()
    {
        return $this->civility;
    }

    /**
     * @param mixed $civility
     */
    public function setCivility($civility): void
    {
        $this->civility = $civility;
    }

    /**
     * @return mixed
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param mixed $headline
     */
    public function setHeadline($headline): void
    {
        $this->headline = $headline;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getBirthAt()
    {
        return $this->birthAt;
    }

    public function setBirthAt($birthAt): void
    {
        $this->birthAt = $birthAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getisActive()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getClassifieds()
    {
        return $this->classifieds;
    }

    /**
     * @return mixed
     */
    public function getDeals()
    {
        return $this->deals;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles($roles): void
    {
        $this->roles = $roles;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->salt,
        ]);
    }

    /**
     * @see \Serializable::unserialize()
     *
     * @param $serialized
     */
    public function unserialize($serialized)
    {
        [
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            $this->salt
            ] = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getConnectedAt()
    {
        return $this->connectedAt;
    }

    /**
     * @param mixed $connectedAt
     */
    public function setConnectedAt($connectedAt): void
    {
        $this->connectedAt = $connectedAt;
    }

    /**
     * @return mixed
     */
    public function getDealOrders()
    {
        return $this->dealOrders;
    }

    /**
     * @param mixed $dealOrders
     */
    public function setDealOrders($dealOrders): void
    {
        $this->dealOrders = $dealOrders;
    }

    /**
     * @return mixed
     */
    public function getPubCampaigns()
    {
        return $this->pubCampaigns;
    }

    /**
     * @param mixed $pubCampaigns
     */
    public function setPubCampaigns($pubCampaigns): void
    {
        $this->pubCampaigns = $pubCampaigns;
    }

    public function getArticles(array $criteriaEqual = [], array $criteriaNot = []): iterable
    {
        if (!empty($criteriaEqual) || !empty($criteriaNot)) {
            $criteria = Criteria::create();
            if (!empty($criteriaEqual)) {
                foreach ($criteriaEqual as $key => $value) {
                    $criteria->where(Criteria::expr()->eq($key, $value));
                }
            }
            if (!empty($criteriaNot)) {
                foreach ($criteriaNot as $key => $value) {
                    $criteria->where(Criteria::expr()->neq($key, $value));
                }
            }

            return $this->articles->matching($criteria);
        }

        return $this->articles;
    }

    public function getEvents(): iterable
    {
        return $this->events;
    }

    public function addClassified(PostClassified $classified): self
    {
        if (!$this->classifieds->contains($classified)) {
            $this->classifieds[] = $classified;
            $classified->setUser($this);
        }

        return $this;
    }

    public function removeClassified(PostClassified $classified): self
    {
        if ($this->classifieds->contains($classified)) {
            $this->classifieds->removeElement($classified);
            // set the owning side to null (unless already changed)
            if ($classified->getUser() === $this) {
                $classified->setUser(null);
            }
        }

        return $this;
    }

    public function addDeal(PostDeal $deal): self
    {
        if (!$this->deals->contains($deal)) {
            $this->deals[] = $deal;
            $deal->setUser($this);
        }

        return $this;
    }

    public function removeDeal(PostDeal $deal): self
    {
        if ($this->deals->contains($deal)) {
            $this->deals->removeElement($deal);
            // set the owning side to null (unless already changed)
            if ($deal->getUser() === $this) {
                $deal->setUser(null);
            }
        }

        return $this;
    }

    public function addDealOrder(PostDealOrders $dealOrder): self
    {
        if (!$this->dealOrders->contains($dealOrder)) {
            $this->dealOrders[] = $dealOrder;
            $dealOrder->setUser($this);
        }

        return $this;
    }

    public function removeDealOrder(PostDealOrders $dealOrder): self
    {
        if ($this->dealOrders->contains($dealOrder)) {
            $this->dealOrders->removeElement($dealOrder);
            // set the owning side to null (unless already changed)
            if ($dealOrder->getUser() === $this) {
                $dealOrder->setUser(null);
            }
        }

        return $this;
    }

    public function addPubCampaign(PubCampaigns $pubCampaign): self
    {
        if (!$this->pubCampaigns->contains($pubCampaign)) {
            $this->pubCampaigns[] = $pubCampaign;
            $pubCampaign->setUser($this);
        }

        return $this;
    }

    public function removePubCampaign(PubCampaigns $pubCampaign): self
    {
        if ($this->pubCampaigns->contains($pubCampaign)) {
            $this->pubCampaigns->removeElement($pubCampaign);
            // set the owning side to null (unless already changed)
            if ($pubCampaign->getUser() === $this) {
                $pubCampaign->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Category[]|Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }
}
