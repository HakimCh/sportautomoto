<?php

declare(strict_types=1);

namespace App\Entity;

use Cocur\Slugify\Slugify;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use HakimCh\SeoBundle\Contracts\SeoEntityInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"general" = "Customer", "driver" = "CustomerDriver", "official" = "CustomerOfficial", "professional" = "CustomerProfessional"})
 * @ORM\Table(
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="customer_unique", columns={"email", "type"})
 *     }
 * )
 * @ORM\HasLifecycleCallbacks
 */
class Customer implements EntityInterface, SeoEntityInterface
{
    public const STATUS_PENDING = 'pending';
    public const STATUS_CONFIRMED = 'confirmed';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @param int|null
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=125, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=125, nullable=true)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @Assert\Length(min="70")
     */
    protected $content;

    /**
     * @ORM\Column(type="json")
     */
    protected $pictures;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Email
     */
    protected $email;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $phones;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $facebook;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    protected $status = self::STATUS_PENDING;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $count = 0;

    /**
     * @var string
     */
    protected $type;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $birthAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="customers")
     * @ORM\JoinTable(name="customer_category")
     *
     * @var Category[]
     */
    protected $categories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CustomerSubscription", mappedBy="customer", cascade={"persist"})
     */
    protected $subscriptions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CustomerStory", mappedBy="customer", cascade={"persist", "remove"})
     */
    protected $stories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->stories = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function onUpdate(): void
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function onInsert(): void
    {
        $this->count = $this->count ?? 0;
        $this->status = $this->status ?? self::STATUS_PENDING;
        $this->createdAt = $this->createdAt ?? new DateTime();
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getTitle(): string
    {
        return trim($this->firstName.' '.$this->lastName);
    }

    public function getUsername(): ?string
    {
        return Slugify::create()->slugify($this->getTitle());
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPictures(): array
    {
        return $this->pictures ?? [];
    }

    public function getProfilePicture(): ?string
    {
        return $this->pictures['profile'] ?? null;
    }

    public function getSlideShowPictures(): array
    {
        $pictures = $this->pictures ?? [];
        if (\array_key_exists('profile', $pictures)) {
            unset($pictures['profile']);
        }

        return $pictures;
    }

    public function setPictures(array $pictures): self
    {
        $this->pictures = $pictures;

        return $this;
    }

    public function addPictures(array $photos): self
    {
        $this->pictures = array_replace($this->pictures, $photos);

        return $this;
    }

    public function addPicture(string $key, string $value): self
    {
        $this->pictures[$key] = $value;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getRecipient(): array
    {
        return [$this->email => $this->getTitle()];
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhones(): ?array
    {
        return $this->phones;
    }

    public function setPhones(?array $phones): self
    {
        $this->phones = $phones;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function addPageView(int $count = 1): self
    {
        $this->count += $count;

        return $this;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function getBirthAt(): ?DateTime
    {
        return $this->birthAt;
    }

    public function setBirthAt(?DateTime $birthAt): self
    {
        $this->birthAt = $birthAt;

        return $this;
    }

    /**
     * @return Category[]|Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category[] $categories
     *
     * @return Customer
     */
    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    public function clearCategories(): self
    {
        $this->categories->clear();

        return $this;
    }

    /**
     * @return Collection|CustomerSubscription[]
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    public function addSubscription(CustomerSubscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions[] = $subscription;
            $subscription->setCustomer($this);
        }

        return $this;
    }

    /**
     * @return Collection|CustomerStory[]
     */
    public function getStories()
    {
        return $this->stories;
    }

    public function addStory(CustomerStory $story): self
    {
        if (!$this->stories->contains($story)) {
            $this->stories[] = $story;
            $story->setCustomer($this);
        }

        return $this;
    }

    public function removeStoryByIndex(int $index): self
    {
        $this->stories->remove($index);

        return $this;
    }
}
