<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PubCampaignsRepository")
 */
class PubCampaigns
{
    public const STATUS_ONGOING = 'ONGOING';
    public const STATUS_PENDING = 'PENDING';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="json")
     */
    private $pictures;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $uid;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pub", inversedBy="pubCampaigns")
     */
    private $pub;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="pubCampaigns")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" = "1970-01-01"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" = "1970-01-01"})
     */
    private $updatedAt;

    public function __construct(?User $user = null)
    {
        $this->user = $user;
        $this->pictures = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPictures(): array
    {
        return $this->pictures ?? [];
    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    public function getPicture(string $key)
    {
        return $this->pictures[$key] ?? null;
    }

    public function setPictures(array $pictures): self
    {
        $this->pictures = $pictures;

        return $this;
    }

    public function addPictures(array $pictures): self
    {
        $this->pictures = array_replace(
            $this->pictures,
            $pictures
        );

        return $this;
    }

    public function addPicture(string $key, string $value): self
    {
        $this->pictures[$key] = $value;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPub(): ?Pub
    {
        return $this->pub;
    }

    public function setPub(?Pub $pub): self
    {
        $this->pub = $pub;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }
}
