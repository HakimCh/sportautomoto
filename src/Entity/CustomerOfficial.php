<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerOfficialRepository")
 */
class CustomerOfficial extends Customer implements EntityInterface
{
    public const TYPE_LABEL = 'official';
    public const MAX_POST_PER_PAGE = 24;

    /**
     * @var string
     */
    protected $type = self::TYPE_LABEL;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $club;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $job;

    public function getClub(): ?string
    {
        return $this->club;
    }

    public function setClub(?string $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getJob(): ?string
    {
        return $this->job;
    }

    public function setJob(?string $job): self
    {
        $this->job = $job;

        return $this;
    }
}
