<?php

declare(strict_types=1);

namespace App\Entity;

interface EntityInterface
{
    public function getId(): ?int;

    public function getTitle(): ?string;

    public function getPictures(): array;

    public function setPictures(array $pictures);

    public function addPictures(array $pictures);

    public function addPicture(string $key, string $value);
}
