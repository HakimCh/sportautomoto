<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostArticleRepository")
 */
class PostArticle extends Post
{
    public const TYPE_LABEL = 'article';
    public const MAX_POST_PER_PAGE = 24;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="articles")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;
    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $excerpt;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" = 0})
     */
    private $isSticky;

    /**
     * @return mixed
     */
    public function getExcerpt(): ?string
    {
        return $this->excerpt;
    }

    public function setExcerpt(?string $excerpt = null): self
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    public function isSticky(): bool
    {
        return $this->isSticky ?? false;
    }

    public function setIsSticky(bool $isSticky): self
    {
        $this->isSticky = $isSticky;

        return $this;
    }

    public function getType(): string
    {
        return self::TYPE_LABEL;
    }
}
