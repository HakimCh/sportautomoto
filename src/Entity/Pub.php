<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PubRepository")
 */
class Pub
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $page;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PubTypes", inversedBy="pubs")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PubCampaigns", mappedBy="pub")
     */
    private $pubCampaigns;

    public function __construct()
    {
        $this->pubCampaigns = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getPage(): ?string
    {
        return $this->page;
    }

    public function setPage(string $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getType(): ?PubTypes
    {
        return $this->type;
    }

    public function setType(?PubTypes $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPubCampaigns()
    {
        return $this->pubCampaigns;
    }

    /**
     * @param mixed $pubCampaigns
     */
    public function setPubCampaigns($pubCampaigns): void
    {
        $this->pubCampaigns = $pubCampaigns;
    }

    public function addPubCampaign(PubCampaigns $pubCampaign): self
    {
        if (!$this->pubCampaigns->contains($pubCampaign)) {
            $this->pubCampaigns[] = $pubCampaign;
            $pubCampaign->setPub($this);
        }

        return $this;
    }

    public function removePubCampaign(PubCampaigns $pubCampaign): self
    {
        if ($this->pubCampaigns->contains($pubCampaign)) {
            $this->pubCampaigns->removeElement($pubCampaign);
            // set the owning side to null (unless already changed)
            if ($pubCampaign->getPub() === $this) {
                $pubCampaign->setPub(null);
            }
        }

        return $this;
    }
}
