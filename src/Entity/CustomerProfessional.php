<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerProfessionalRepository")
 */
class CustomerProfessional extends Customer implements EntityInterface
{
    public const TYPE_LABEL = 'professional';
    public const MAX_POST_PER_PAGE = 24;

    /**
     * @var string
     */
    protected $type = self::TYPE_LABEL;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $coords;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="customers")
     *
     * @var City
     */
    private $city;

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCoords(): ?string
    {
        return $this->coords;
    }

    public function setCoords(?string $coords): self
    {
        $this->coords = $coords;

        return $this;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    public function setTags(?array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function addTags(array $tags): self
    {
        $this->tags = array_replace($this->tags, $tags);

        return $this;
    }

    public function addTag(string $tag): self
    {
        $this->tags[] = $tag;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }
}
