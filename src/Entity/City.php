<?php

declare(strict_types=1);

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CityRepository")
 * @ORM\HasLifecycleCallbacks
 */
class City
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank
     * @Assert\Length(min="2")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="PostClassified", mappedBy="city")
     */
    private $classifieds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CustomerProfessional", mappedBy="city")
     */
    private $customers;

    /**
     * @ORM\OneToMany(targetEntity="PostDeal", mappedBy="city")
     */
    private $deals;

    /**
     * @ORM\PreFlush
     */
    public function preFlush(): void
    {
        $this->slug = Slugify::create()->slugify($this->title);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return PostClassified[]
     */
    public function getClassifieds()
    {
        return $this->classifieds;
    }

    /**
     * @return PostDeal[]
     */
    public function getDeals()
    {
        return $this->deals;
    }

    /**
     * @return Customer[]
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    public function getContent(): string
    {
        return '';
    }
}
