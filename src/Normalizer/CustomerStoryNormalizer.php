<?php

declare(strict_types=1);

namespace App\Normalizer;

use App\Entity\CustomerStory;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CustomerStoryNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'vehicle' => $object->getVehicle(),
            'year' => $object->getYear(),
            'raking' => $object->getRaking(),
            'victories' => $object->getVictories(),
            'category' => $object->getCategory()->getId(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof CustomerStory;
    }
}
