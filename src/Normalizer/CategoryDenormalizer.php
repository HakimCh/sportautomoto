<?php

declare(strict_types=1);

namespace App\Normalizer;

use App\Repository\CategoryRepository;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class CategoryDenormalizer implements DenormalizerInterface
{
    /**
     * @var CategoryRepository
     */
    private $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (is_numeric($data)) {
            return $this->repository->find($data);
        }

        return $this->repository->findByIdList($data);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return \in_array($type, [
            'App\Entity\Category',
            'App\Entity\Category[]',
        ]);
    }
}
