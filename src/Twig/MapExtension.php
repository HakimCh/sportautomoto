<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MapExtension extends AbstractExtension
{
    public const ENDPOINT = 'https://maps.googleapis.com/maps/api';
    public const API_KEY = 'AIzaSyDRv7cfhg3ZbViTLhmA55CkPzWmZTM_MRY';

    public function getFunctions(): array
    {
        return [
            new TwigFunction('static_map', [$this, 'staticMapFunction']),
        ];
    }

    public function staticMapFunction($address, $width, $height)
    {
        $location = $this->getLocationFromAddress($address);
        if (!$location) {
            return null;
        }

        return sprintf(
            '%s/staticmap?center=%s&zoom=12&size=%dx%d&key=%s',
            self::ENDPOINT,
            implode(',', $location),
            $width,
            $height,
            self::API_KEY
        );
    }

    private function getLocationFromAddress($address)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, sprintf(
            '%s/geocode/json?address=%s+Maroc&key=%s',
            self::ENDPOINT,
            str_replace(' ', '+', $address),
            self::API_KEY
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = json_decode(curl_exec($ch), true);
        curl_close($ch);

        return $output['results'][1]['geometry']['location'] ?? null;
    }
}
