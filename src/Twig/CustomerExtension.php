<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\CustomerDriver;
use App\Entity\CustomerOfficial;
use App\Entity\CustomerProfessional;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CustomerExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('isCustomerDriver', [$this, 'isCustomerDriver']),
            new TwigFunction('isCustomerOfficial', [$this, 'isCustomerOfficial']),
            new TwigFunction('isCustomerProfessional', [$this, 'isCustomerProfessional']),
        ];
    }

    public function isCustomerDriver(string $type): bool
    {
        return CustomerDriver::TYPE_LABEL === $type;
    }

    public function isCustomerOfficial(string $type): bool
    {
        return CustomerOfficial::TYPE_LABEL === $type;
    }

    public function isCustomerProfessional(string $type): bool
    {
        return CustomerProfessional::TYPE_LABEL === $type;
    }
}
