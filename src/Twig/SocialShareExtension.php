<?php

declare(strict_types=1);

namespace App\Twig;

use Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SocialShareExtension extends AbstractExtension
{
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var string
     */
    private $url;

    public function __construct(Environment $twig, RequestStack $requestStack)
    {
        $this->twig = $twig;
        if ($request = $requestStack->getCurrentRequest()) {
            $this->url = $request->getSchemeAndHttpHost().$request->getPathInfo();
        }
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('shareToSocial', [$this, 'shareToSocialFunction']),
        ];
    }

    /**
     * @param string $title
     * @param string $text
     *
     * @return string
     */
    public function shareToSocialFunction(string $title, string $text = ''): string
    {
        try {
            return $this->twig
                ->render('blocs/social-share.html.twig', [
                    'url' => $this->url,
                    'title' => $title,
                    'text' => $text,
                ]);
        } catch (Exception $e) {
            return '';
        }
    }
}
