<?php

declare(strict_types=1);

namespace App\Twig;

use HakimCh\Helpers\ArrayBag;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class HelpersExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('arrayWalker', [$this, 'arrayWalkerFunction']),
            new TwigFunction('is_address', [$this, 'isValidAddressFunction']),
        ];
    }

    /**
     * @param string $path
     * @param array  $array
     *
     * @return mixed
     */
    public function arrayWalkerFunction($array, $path)
    {
        $arrayBag = new ArrayBag($array);

        return $arrayBag->getByPath($path);
    }

    /**
     * @param string $address
     *
     * @return bool
     */
    public function isValidAddressFunction($address): bool
    {
        $address = trim($address);

        return 1 === preg_match('/^([a-zA-Z0-9]+)\s([a-zA-Z0-9\,\s]+)/', $address);
    }
}
