<?php

declare(strict_types=1);

namespace App\Twig;

use DateTime;
use Exception;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AgeExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('age', [$this, 'ageFilter']),
        ];
    }

    /**
     * @param DateTime $dateTime
     *
     * @throws Exception
     *
     * @return string
     */
    public function ageFilter($dateTime): string
    {
        return $dateTime
            ->diff(new DateTime())
            ->format('%y');
    }
}
