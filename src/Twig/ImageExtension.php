<?php

declare(strict_types=1);

namespace App\Twig;

use Exception;
use HakimCh\UploaderBundle\Constraint\ExtensionConstraint;
use HakimCh\UploaderBundle\Services\ImageUploader;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ImageExtension extends AbstractExtension
{
    /**
     * @var ImageUploader
     */
    private $uploader;

    public function __construct(ImageUploader $uploader)
    {
        $this->uploader = $uploader;
        $this->uploader->addConstraint(
            new ExtensionConstraint(['jpg', 'jpeg', 'png', 'tmp'])
        );
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('resize', [$this, 'thumbnailFunction']),
            new TwigFunction('thumbnail', [$this, 'thumbnailFunction']),
            new TwigFunction('resize_width', [$this, 'resizeByWidthFunction']),
            new TwigFunction('resize_height', [$this, 'resizeByHeightFunction']),
        ];
    }

    public function resizeByWidthFunction($name, $width, $destinationFolderName)
    {
        $thumbRelativePath = sprintf('thumbs/%s/%d', $destinationFolderName, $width);
        $pathname = $this->getPathname($name, $destinationFolderName, $thumbRelativePath);
        try {
            return $this->uploader
                ->open($pathname)
                ->resizeByWidth($width)
                ->upload($thumbRelativePath);
        } catch (Exception $e) {
            return $pathname;
        }
    }

    public function resizeByHeightFunction($name, $height, $destinationFolderName)
    {
        $thumbRelativePath = sprintf('thumbs/%s/%d', $destinationFolderName, $height);
        $pathname = $this->getPathname($name, $destinationFolderName, $thumbRelativePath);
        try {
            return $this->uploader
                ->open($pathname)
                ->resizeByHeight($height)
                ->upload($thumbRelativePath);
        } catch (Exception $e) {
            return $pathname;
        }
    }

    public function thumbnailFunction($name, $width, $height, $destinationFolderName, $adaptative = false): string
    {
        $thumbRelativePath = sprintf('thumbs/%s/%dx%d', $destinationFolderName, $width, $height);
        try {
            $pathname = $this->getPathname($name, $destinationFolderName, $thumbRelativePath);

            return $this->uploader
                ->open($pathname)
                ->thumbnail($width, $height, $adaptative)
                ->upload($thumbRelativePath);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function getPathname($name, $destinationFolderName, $thumbRelativePath)
    {
        $defaultPicture = 'users' == $destinationFolderName ? '/images/fb-image.jpg' : '/images/default-bg.jpg';
        if (!$name) {
            throw new Exception($defaultPicture);
        }
        $picture = sprintf('%s/%s/%s', $this->uploader->getPublicPath(), $thumbRelativePath, $name);
        if (file_exists($picture)) {
            throw new Exception(sprintf('/images/%s/%s', $thumbRelativePath, $name));
        }

        return sprintf('%s/%s/%s', $this->uploader->getPublicPath(), $destinationFolderName, $name);
    }
}
