<?php

declare(strict_types=1);

namespace App\Traits;

use App\Entity\Customer;
use App\Entity\CustomerDriver;
use App\Entity\CustomerOfficial;
use App\Entity\CustomerProfessional;
use App\Form\CustomerDriverType;
use App\Form\CustomerOfficialType;
use App\Form\CustomerProfessionalType;

trait CustomerTrait
{
    public function getEntityByType(string $type): Customer
    {
        switch ($type) {
            case CustomerDriver::TYPE_LABEL:
                return new CustomerDriver();
            case CustomerOfficial::TYPE_LABEL:
                return new CustomerOfficial();
            default:
                return new CustomerProfessional();
        }
    }

    public function getEntityByLabel(string $label): Customer
    {
        switch ($label) {
            case 'pilote':
                return new CustomerDriver();
            case 'officiel':
                return new CustomerOfficial();
            default:
                return new CustomerProfessional();
        }
    }

    public function getEntityClassNameByLabel(string $label): string
    {
        switch ($label) {
            case 'pilote':
                return CustomerDriver::class;
            case 'officiel':
                return CustomerOfficial::class;
            default:
                return CustomerProfessional::class;
        }
    }

    public function getCustomerTypeByLabel(string $label): string
    {
        switch ($label) {
            case 'pilote':
                return CustomerDriver::TYPE_LABEL;
            case 'officiel':
                return CustomerOfficial::TYPE_LABEL;
            default:
                return CustomerProfessional::TYPE_LABEL;
        }
    }

    public function getCustomerFormTypeByLabel(string $label): string
    {
        switch ($label) {
            case 'pilote':
                return CustomerDriverType::class;
            case 'officiel':
                return CustomerOfficialType::class;
            default:
                return CustomerProfessionalType::class;
        }
    }
}
