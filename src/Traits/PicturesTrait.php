<?php

declare(strict_types=1);

namespace App\Traits;

trait PicturesTrait
{
    /**
     * @param array $photos
     *
     * @return array
     */
    public static function getFilesBasename(array $photos): array
    {
        return array_map(function ($photo) {
            return basename($photo);
        }, $photos);
    }

    /**
     * @param array $saved
     * @param array $temporary
     *
     * @return array
     */
    public static function getPicturesWithFullPath(array $saved, array $temporary): array
    {
        [$folder, $pictures] = $saved;
        $savedPictures = self::getPictures($pictures, $folder);
        $temporaryPictures = self::getPictures($temporary);

        return array_replace($savedPictures, $temporaryPictures);
    }

    /**
     * @param array  $pictures
     * @param string $folder
     *
     * @return array
     */
    public static function getPictures(array $pictures, string $folder = 'tmp'): array
    {
        return array_map(function ($pictureBasename) use ($folder) {
            return sprintf('/images/%s/%s', $folder, $pictureBasename);
        }, $pictures);
    }

    /**
     * @param array  $pictures
     * @param string $folder
     */
    public static function removePictures(array $pictures, string $folder = 'tmp'): void
    {
        foreach ($pictures as $picture) {
            self::removePicture($picture, $folder);
        }
    }

    /**
     * @param string $picture
     * @param string $folder
     */
    public static function removePicture(string $picture, string $folder = 'tmp'): void
    {
        if (file_exists($path = sprintf('%s/images/%s/%s', ROOT_PATH, $folder, $picture))) {
            unlink($path);
        }
    }
}
