<?php

declare(strict_types=1);

namespace App\Traits;

use Doctrine\ORM\QueryBuilder;

trait RepositoryTrait
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $slug
     * @param string       $field
     *
     * @return QueryBuilder
     */
    public function buildRelatedQuery(QueryBuilder $queryBuilder, string $slug, string $field): QueryBuilder
    {
        $terms = explode('-', $slug);
        if (!empty($terms)) {
            foreach ($terms as $key => $term) {
                if (\strlen($term) < 3 || !\is_string($term)) {
                    continue;
                }
                $fieldKey = 'term_'.$key;
                $queryBuilder->orWhere(sprintf('%s like :%s', $field, $fieldKey));
                $queryBuilder->setParameter($fieldKey, '%'.$term.'%', 'string');
            }
        }

        return $queryBuilder;
    }
}
