<?php

declare(strict_types=1);

namespace App\Security\Authenticator;

use App\Form\UserLoginType;
use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    public const LOGIN_ROUTE = 'login_page';
    public const LOGIN_FORM_NAME = 'user_login';
    public const ON_SUCCESS_REDIRECT_ROUTE_USER = 'user_profile';
    public const ON_SUCCESS_REDIRECT_ROUTE_ADMIN = 'backend_article_show';
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var UserRepository
     */
    private $repository;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    public function __construct(RouterInterface $router, UserRepository $repository, FormFactoryInterface $formFactory, EncoderFactoryInterface $encoderFactory)
    {
        $this->router = $router;
        $this->repository = $repository;
        $this->formFactory = $formFactory;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request)
    {
        // GOOD behavior: only authenticate on a specific route
        if (self::LOGIN_ROUTE == $request->attributes->get('_route') && $request->isMethod('post')) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        $form = $this->formFactory->create(UserLoginType::class);
        $form->handleRequest($request);

        return $form->getData();
    }

    /**
     * {@inheritdoc}
     *
     * @throws NonUniqueResultException
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!\array_key_exists('_username', $credentials) || null === $credentials['_username']) {
            return null;
        }

        return $this->repository->loadUserByUsername($credentials['_username']);
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $encoder = $this->encoderFactory->getEncoder($user);

        return $encoder->isPasswordValid(
            $user->getPassword(),
            $user->getSaltedPassword($credentials['_password']),
            $user->getSalt()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): RedirectResponse
    {
        $userRoles = $token->getUser()->getRoles();
        $route = self::ON_SUCCESS_REDIRECT_ROUTE_USER;

        if (
            \in_array('ROLE_SUPER_ADMIN', $userRoles) ||
            \in_array('ROLE_ADMIN', $userRoles) ||
            \in_array('ROLE_WRITER', $userRoles)
        ) {
            $route = self::ON_SUCCESS_REDIRECT_ROUTE_ADMIN;
        }

        return new RedirectResponse($this->router->generate($route));
    }

    /**
     * {@inheritdoc}
     */
    protected function getLoginUrl(): string
    {
        return $this->router->generate(self::LOGIN_ROUTE);
    }
}
