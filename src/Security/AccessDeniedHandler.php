<?php

declare(strict_types=1);

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, RouterInterface $router)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->router = $router;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
            $route = $this->router->generate('login_page');
        } elseif ($this->authorizationChecker->isGranted('ROLE_USER')) {
            $route = $this->router->generate('user_profile');
        } else {
            $route = $this->router->generate('backend_home');
        }

        return new RedirectResponse($route);
    }
}
