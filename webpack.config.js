const Encore = require('@symfony/webpack-encore');
const Dotenv = require('dotenv-webpack');
const CopyPlugin = require('copy-webpack-plugin');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .enableBuildNotifications()

    .addStyleEntry('css/v2/style', [
        './assets/sass/style.scss',
        './node_modules/ch-select-picker/src/scss/select-picker.scss',
        './assets/sass/blocs/slideshow.scss',
    ])
    .addStyleEntry('css/v2/home', './assets/sass/pages/home.scss')
    .addStyleEntry('css/v2/article', './assets/sass/pages/article.scss')
    .addStyleEntry('css/v2/deal', './assets/sass/pages/deal.scss')
    .addStyleEntry('css/v2/classified', './assets/sass/pages/classified.scss')
    .addStyleEntry('css/v2/classifieds', './assets/sass/pages/classifieds.scss')
    .addStyleEntry('css/v2/prices', './assets/sass/pages/publicity.scss')
    .addStyleEntry('css/v2/contact', './assets/sass/pages/contact.scss')
    .addStyleEntry('css/v2/customers', './assets/sass/pages/customers.scss')
    .addStyleEntry('css/v2/customer', './assets/sass/pages/customer.scss')
    .addStyleEntry('css/v2/register', './assets/sass/pages/register.scss')
    .addStyleEntry('css/v2/login', './assets/sass/pages/login.scss')
    .addStyleEntry('css/v2/user', './assets/sass/pages/user.scss')
    .addStyleEntry('css/v2/directory-register', './assets/sass/pages/directory-register.scss')

    .addEntry('js/app', [
        './node_modules/jquery.countdown/jquery.countdown',
        './node_modules/easy-autocomplete/dist/jquery.easy-autocomplete',
        './node_modules/lightslider/dist/js/lightslider',
        './assets/js/app',
    ])
    .addEntry('js/uploader.jquery', './assets/js/uploader.jquery.js')
    .addEntry('js/experiences', './assets/js/experiences.js')
    .addEntry('js/classifieds', './assets/js/classifieds.js')
    .addEntry('js/classified', './assets/js/classified.js')

    .addStyleEntry('css/admin/app', [
        './assets/sass/admin.scss',
        './node_modules/ch-select-picker/src/scss/select-picker.scss',
    ])
    .addEntry('js/admin/app', './assets/js/admin.js')

    .autoProvidejQuery()
    .enableSassLoader()
    .enableSourceMaps(!Encore.isProduction())
    .cleanupOutputBeforeBuild()
    .enableVersioning(Encore.isProduction())

    .addPlugin(new Dotenv())
    .addPlugin(new CopyPlugin([
        { from: './assets/images', to: 'images' },
    ]))

    .addLoader({ test: /\.handlebars$/, loader: 'handlebars-loader' })
;

module.exports = Encore.getWebpackConfig();
