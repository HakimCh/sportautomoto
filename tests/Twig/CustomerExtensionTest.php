<?php

namespace App\Tests\Twig;

use App\Entity\CustomerDriver;
use App\Entity\CustomerOfficial;
use App\Entity\CustomerProfessional;
use App\Twig\CustomerExtension;
use PHPUnit\Framework\TestCase;

class CustomerExtensionTest extends TestCase
{
    public function testIsCustomerDriverFail()
    {
        $extension = new CustomerExtension();
        $output = $extension->isCustomerDriver('any');

        $this->assertFalse($output);
    }

    public function testIsCustomerDriverSuccess()
    {
        $extension = new CustomerExtension();
        $output = $extension->isCustomerDriver(CustomerDriver::TYPE_LABEL);

        $this->assertTrue($output);
    }

    public function testIsCustomerOfficialFail()
    {
        $extension = new CustomerExtension();
        $output = $extension->isCustomerOfficial('any');

        $this->assertFalse($output);
    }

    public function testIsCustomerOfficialSuccess()
    {
        $extension = new CustomerExtension();
        $output = $extension->isCustomerOfficial(CustomerOfficial::TYPE_LABEL);

        $this->assertTrue($output);
    }

    public function testIsCustomerProfessionalFail()
    {
        $extension = new CustomerExtension();
        $output = $extension->isCustomerProfessional('any');

        $this->assertFalse($output);
    }

    public function testIsCustomerProfessionalSuccess()
    {
        $extension = new CustomerExtension();
        $output = $extension->isCustomerProfessional(CustomerProfessional::TYPE_LABEL);

        $this->assertTrue($output);
    }
}
