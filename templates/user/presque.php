<div class="container">
  <div class="gf-grid-container">
    <div class="gf-grid-col gf-grid-col-12">
      <img src="./Confirmation de compte Golden Frog _ VyprVPN - par Golden Frog _ Golden Frog_files/logo_vyprvpn.png" class="logo-vyprvpn">
      <div class="content-wrapper">
        <div class="main-intro no-bg aligncenter">
          <img alt="Confirmer Golden Frog" src="./Confirmation de compte Golden Frog _ VyprVPN - par Golden Frog _ Golden Frog_files/email_confirmation.png">
          <h1>Vous y êtes presque</h1>
        </div>
        <div class="main-content aligncenter">
          <p>Un email de confirmation a été envoyé à <span>ab.chmimo@gmail.com</span>.</p>
          <p>Suivez les instructions que vous trouverez dans l'e-mail de confirmation pour activer votre compte Golden Frog. Afin de vous assurer de bien le recevoir, veuillez ajouter <a href="mailto:support@goldenfrog.com">support@goldenfrog.com</a> à votre carnet d'adresses.</p>
        </div>
      </div>
      <p class="contact-support aligncenter">Vous n'avez pas reçu d'email ou vous avez des difficultés? <a href="https://www.goldenfrog.com/FR/contact-support">Contacter le service client</a></p>
    </div>
  </div>
</div>