import SelectPicker from 'ch-select-picker';
import CountDown from './CountDown';
import SlideShow from './SlideShow';
import liveUpload from './uploader';
import config from './config';

$('.liveupload').liveUpload({
    responsive: true,
    namespace: 'auto',
    baseUrl: `${config.baseURL}/api/image/`,
    tmpUrl: `${config.baseURL}/images/tmp/`,
    imgUrl: `${config.baseURL}/images/`,
    afterUpload: () => {
        $(this).removeClass('error');
        $(this).parent().find('label.error').remove();
    }
});

SelectPicker.listenAll('select');

document.querySelectorAll('.slider')
    .forEach(element => {
        new SlideShow(element, {
            slideSelector: '.slider-item',
        }).listen();
    });

document
    .querySelectorAll('.dropdown')
    .forEach(element => {
        element.addEventListener('mouseenter', () => {
            element.classList.add('dropdown-open');
        });
        element.addEventListener('mouseleave', () => {
            element.classList.remove('dropdown-open');
        });
    });

CountDown.listen('.countdown');
