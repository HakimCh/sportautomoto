import $ from "jquery"
import validate from "jquery-validation";
import selectpicker from "bootstrap-select";
const eventTemplate = require("../templates/events.handlebars");
const experienceTemplate = require("../templates/experiences.handlebars");

const addEntry 	= $("#addEntry");
const form 	    = addEntry.closest('form');
const entryType = $('#steps').attr('data-role');
const fields	= $('.bselect, input', form);
const experiences = $('#experiences');
let expCount = $('table > tbody > tr', experiences).length;

// Load saved entries
if(typeof currentStepData !== "undefined" && Object.keys(currentStepData).length > 0) {
    beautifyEntries(currentStepData);
}

$(document).on('click', '#addEntry', function(event) {
    event.preventDefault();
    if (form.valid()) {
        const entry = rawEntry();
        if(entry !== false) {
            const id = $(this).attr('rel');
            if(typeof id !== typeof undefined && id !== false) {
                entry.id = id;
            }
            saveEntry(entry, $(this));
        }
    }
});

$(document).on('click', '.loadEntry', function(event) {
    event.preventDefault();
    getEntry($(this).attr('rel'));
});

$(document).on('click', '.deleteEntry', function(event) {
    event.preventDefault();
    deleteEntry($(this));
});

function saveEntry(data, that) {
    $.ajax({
        url: '/api/story/' + entryType + '/save',
        type: 'POST',
        async: true,
        dataType: 'json',
        data: data
    }).done(function(response) {
        if(response.data !== 0) {
            beautifyEntries(response.data);
            fields.css('border-color', '#eee');
            that.text('Ajouter').attr('class', 'palmares-btn-add');
            console.log('Succès: ' + response.status);
            form[0].reset();
        } else {
            fields.css('border-color', 'red');
            console.log('Error: ' + response.status);
            that.text('Réssayer').attr('class', 'palmares-btn-add');
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log(errorThrown);
        fields.css('border-color', 'red');
        that.text('Réssayer').attr('class', 'palmares-btn-add');
    });
}

function getEntry(id) {
    $.ajax({
        url: '/api/story/' + entryType + '/' + id,
        type: 'GET',
        async: true,
        dataType: 'json',
    }).done(function(response) {
        if(response.data !== 0) {
            fields.each(function() {
                const element = $(this);
                const key = element.attr('id');
                if(key in response.data) {
                    element.val(response.data[key]);
                }
                addEntry.attr('rel', id)
                    .text('Editer')
                    .attr('class', 'finish step-btn-finish');
                element.css('border-color', '#9ad09a');
                $('.bselect', form).selectpicker('refresh');
                console.log('Succès: ' + response.status);
            });
        } else {
            console.log('Error: ' + response.status);
        }
    }) .fail(function(jqXHR, textStatus, errorThrown) {
        console.log(errorThrown);
    });
}

function deleteEntry(that) {
    $.ajax({
        url: '/api/story/' + entryType + '/' + that.attr('rel'),
        type: 'DELETE',
        async: true,
        dataType: 'json',
    }).done(function(response) {
        if (expCount > 2) {
            expCount--;
            that.parent().parent().fadeOut().remove();
        } else {
            beautifyEntries(response.data);
        }
        console.log('Succès: ' + response.status);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log(errorThrown);
    });
}

function beautifyEntries(entries) {
    const templateToHtml = entryType === 'pilote' ?
        experienceTemplate({items: entries}) :
        eventTemplate({items: entries});

    $('#experiences').fadeOut('100').html(templateToHtml).fadeIn('100');
}

function rawEntry() {
    let entry = {}, error = 0;
    $('input, select', '#palFields').each(function() {
        const element = $(this);
        const eleId = element.attr('id');
        entry[eleId] = element.val();
        if(eleId === 'category') {
            entry['categoryTitle'] = $(':selected', element).text();
        }
        if(element.valid() === false) error++;
    });
    return error === 0 ? entry : false;
}
