import SelectPicker from 'ch-select-picker';

const DATA_CHAINED_CHILD_SELECTOR = 'data-chained-child';

export default class ChainedSelect {
    constructor(debug, baseUrl = null) {
        this.debug = debug;
        this.baseUrl = baseUrl;
        this.selectors = {};
    }
    add(parentSelector, childSelector, label, url, urlParameters = []) {
        const parent = document.querySelector(parentSelector);
        parent.setAttribute(DATA_CHAINED_CHILD_SELECTOR, childSelector);
        this.selectors[parentSelector] = { childSelector, label, url };
        this.urlParameters = urlParameters;
        return this;
    }
    chain() {
        document
            .querySelectorAll(`[${DATA_CHAINED_CHILD_SELECTOR}]`)
            .forEach(selectElement => this.listen(selectElement));
    }
    listen(selectElement) {
        const id = `#${selectElement.getAttribute('id')}`;
        if (!this.selectors.hasOwnProperty(id)) {
            return;
        }
        selectElement
            .previousSibling
            .querySelectorAll('li')
            .forEach(liElement => {
                liElement
                    .addEventListener('click', () => {
                        const { childSelector, label, url } = this.selectors[id];
                        ChainedSelect.loading(childSelector);
                        fetch(this.generateUrl(url), { method: 'GET', credentials: 'include', headers: {'Content-Type': 'application/json'} })
                        .then(res => {
                            if(res.ok) {
                                return res.json();
                            } else {
                                throw Error(`Request rejected with status ${res.status}`);
                            }
                        })
                        .then(data => this.populate(childSelector, data, label))
                        .catch(err => this.log(err))
                    });
            });
    }
    static loading(selector) {
        const selectElement = document.querySelector(selector);
        selectElement.append(ChainedSelect.createOption('Chargement...', 0));
        SelectPicker.refresh(selectElement);
    }
    populate(selector, data, defaultLabel) {
        const selectElement = document.querySelector(selector);
        selectElement.innerHTML = '';
        selectElement.append(ChainedSelect.createOption(defaultLabel, 0));
        if (data) {
            Object.keys(data).forEach(key => selectElement.append(ChainedSelect.createOption(data[key], key)));
        }
        SelectPicker.refresh(selectElement);
        this.listen(selectElement);
    }
    static createOption(content, value) {
        const option = document.createElement('option');
        option.setAttribute('value', value);
        option.innerText = content;
        return option;
    }
    generateUrl(url) {
        const finalUrl = new URL(url);
        const values = [];
        this.urlParameters.forEach(selector => values.push(document.querySelector(selector).value));
        let pathname = `${finalUrl.pathname.replace(/\/$/, '')}/`;
        const regex = /:([^:]*)\//;
        let key = 0;
        while (true) {
            const match = regex.exec(pathname);
            if (!match) break;
            pathname = pathname.replace(`:${match[1]}`, values[key]);
            key++;
        }
        const generatedUrl = finalUrl.origin + pathname.replace(/\/$/, '');
        if (this.baseUrl) {
            return this.baseUrl + generatedUrl;
        }
        return generatedUrl;
    }
    log(message) {
        if (this.debug) {
            if (message instanceof Error) {
                console.error(message);
            } else {
                console.info(message);
            }
        }
    }
}
