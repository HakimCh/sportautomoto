import Commons from './Commons';
import ChainedSelect from './select-picker/ChainedSelect';
import config from './config';
import Sticky from './Sticky';

const DEBUG = process.env.NODE_ENV === "development";

const { baseUrl } = config;

Sticky.listen({
  stickyClassName: 'sticky-bloc',
  stopperClassName: 'footer',
  topToKeep: 25
}, true);

new ChainedSelect(DEBUG)
  .add('#type', '#brand', 'Toutes les marques', `${baseUrl}api/make/:type`, ['#type'])
  .add('#brand', '#model', 'Tous les modèles', `${baseUrl}api/make/:type/:make`, ['#type', '#brand'])
  .chain();

if (window.innerWidth > 992) {
  const isTruthy = Commons.sameHeight('.sidebar', '.main-content');
  if (isTruthy) {
    const sideBarElement = document.querySelector('.sidebar');
    sideBarElement.classList.remove('sticky-bloc');
    sideBarElement.removeAttribute('style');
  }
}
