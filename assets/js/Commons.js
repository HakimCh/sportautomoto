export default class Commons {
    static sameHeight(selector1, selector2) {
        const contentSelector = document.querySelector(selector2);
        const sideBarHeight = document.querySelector(selector1).offsetHeight;
        const contentHeight = contentSelector.offsetHeight;
        if (sideBarHeight > contentHeight) {
            contentSelector.style.minHeight = `${sideBarHeight}px`;
            return true;
        }
        return false;
    }
    static generateUrl(url, values = {}) {
        const finalUrl = new URL(url);
        let pathname = `${finalUrl.pathname.replace(/\/$/, '')}/`;
        const regex = /:([^:/]*)\//;
        while (true) {
            const match = regex.exec(pathname);
            if (!match) break;
            let value = values[match[1]];
            if (typeof value === 'object') {
                value = document.getElementById(value.id)[value.key];
            }
            pathname = pathname.replace(`:${match[1]}`, value);
        }
        return finalUrl.origin + pathname.replace(/\/$/, '');
    }
    static toggle(selector) {
        const elements = document.querySelectorAll(selector);
        elements.forEach(element => {
            element.addEventListener('click', () => {
                Commons.toggleAll(selector, true);
                element.nextElementSibling.style = 'display: block';
            });
        });
    }
    static toggleAll(selector, hidden) {
        document.querySelectorAll(selector).forEach(element => {
            if (element.nextElementSibling) {
                element.nextElementSibling.style = `display: ${hidden ? 'none' : 'block'}`;
            }
        });
    }
}
