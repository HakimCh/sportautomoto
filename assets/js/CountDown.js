export default class CountDown {
    /**
     * @param {String} selector
     */
    static listen(selector) {
        document
            .querySelectorAll(selector)
            .forEach(element => {
                const datetime = element.getAttribute('data-datetime');
                const interval = setInterval(() => {
                    element.innerHTML = CountDown.parse(datetime, interval);
                }, 1000);
            });
    }

    /**
     * @param {String} datetime
     * @param interval
     */
    static parse(datetime, interval) {
        const distance = new Date(datetime).getTime() - new Date().getTime();
        if (distance < 60000) {
            clearInterval(interval);
            return 'Expiré';
        }
        const days = Math.floor(distance / 86400000);
        const hours = Math.floor((distance % 86400000) / 3600000);
        const minutes = Math.floor((distance % 3600000) / 60000);
        const seconds = Math.floor((distance % 60000) / 1000);
        let value = `${hours|'00'}:${minutes}:${seconds}`;
        if (days) {
            value = `${days}j ${value}`
        }
        return value;
    }

    /**
     * @param {Object} config
     * @param {String} config.expired
     * @param {String} config.selector
     * @returns {Object}
     */
    static getConfig(config) {
        return Object.assign({},
            {
            expired: 'Expired',
            selector: '.countdown',
        }, config);
    }
}

