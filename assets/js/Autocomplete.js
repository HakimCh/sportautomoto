import axios from "axios";
import autocomplete from "autocompleter";
import Commons from './Commons';

export default class Autocomplete {
    static listen(elementId, api, params = {}, onSelect = null) {
        const input = document.getElementById(elementId);
        const hiddenInput = document.getElementsByClassName('autocomplete-value');
        if (!input) {
            return null;
        }
        autocomplete({
           input,
            fetch: (text, callback) => {
               params.text = text;
               axios
                   .get(Commons.generateUrl(api, params) )
                   .then(response => {
                       callback(response.data);
                   })
                   .catch(() => {});
            },
            onSelect: item => {
                input.value = item.label;
                hiddenInput[0].value = item.value;
                if (onSelect) {
                    onSelect(input, item);
                }
            },
        });
    }
}
