import axios from 'axios';

const config = {
    baseURL: 'https://www.sportautomoto.ma',
};

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    config.baseURL = 'https://www.sportautomoto.local';
}

config.client = axios.create({
    baseURL: config.baseURL,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
    }
});

export default config;
