export default class Sticky {
  /**
   * @param {Object} config
   * @param {String} config.stickyClassName
   * @param {String} config.stopperClassName
   * @param {Number} config.topToKeep
   * @param {Boolean} debug
   */
  static listen(config = {}, debug = false) {
    window.addEventListener('scroll', () => {
      try {
        const { sticky, detachPosition, distanceUntilDetach, blocPosition, blocOffset } = Sticky.getStickyData(config, debug);
        const style = Sticky.calculatePosition(
          window.scrollY,
          distanceUntilDetach,
          detachPosition,
          blocPosition,
          blocOffset,
          config.topToKeep,
        );
        if (debug) {
          console.debug(style);
        }
        sticky.setAttribute('style', style);
      } catch (err) {
        if (debug) {
          console.error(err);
        }
      }
    });
  }
  /**
   * @param {Object} config
   * @param {String} config.stickyClassName
   * @param {String} config.stopperClassName
   * @param {Number} config.blocOffset
   * @param {Boolean} debug
   * @returns {Object}
   */
  static getStickyData(config, debug) {
    const stickyClassName = config.stickyClassName || 'sticky-bloc';
    const sticky = document.querySelector(`.${stickyClassName}`);
    if (sticky.length <= 0) {
      throw new Error(`Cannot find the element ${stickyClassName}`);
    }
    const stopperClassName = config.stopperClassName || 'sticky-stopper';
    const stopper = document.querySelector(`.${stopperClassName}`);
    const blocOffset = 0;
    const blocHeight = sticky.offsetHeight;
    const stopperPosition = stopper.offsetTop;
    const data = {
      sticky,
      blocOffset,
      blocHeight,
      stopperPosition,
      blocPosition: sticky.parentElement.offsetTop,
      detachPosition: stopperPosition - blocHeight - blocOffset,
      distanceUntilDetach: stopperPosition + blocOffset,
    };
    if (debug) {
      console.debug(Object.assign({}, data, {stopperPosition, blocHeight}));
    }
    return data;
  }
  /**
   * @param {Number} scrollPosition
   * @param {Number} distanceUntilDetach
   * @param {Number} detachPosition
   * @param {Number} blocPosition
   * @param {Number} blocOffset
   * @param {Number} topToKeep
   * @returns {Object<position, top>}
   */
  static calculatePosition(scrollPosition, distanceUntilDetach, detachPosition, blocPosition, blocOffset, topToKeep) {
    if (scrollPosition >= detachPosition) {
      return `top: auto; bottom: 0`;
    } else if (scrollPosition >= (blocPosition - topToKeep)) {
      return `position: fixed; top: ${topToKeep}px`;
    } else if (scrollPosition < blocPosition) {
      return '';
    }
    /*
    if (detachPosition < scrollPosition) {
      return `top: auto, bottom: 0`;
    } else if (blocPosition < scrollPosition + blocOffset) {
      return `top: ${blocOffset}, bottom: 0`;
    }
    return `top: 25px`;
    */
  }
}
