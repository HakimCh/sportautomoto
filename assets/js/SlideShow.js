export default class SlideShow {
    constructor(element) {
        this.wrapper = element;
        this.element = element.querySelector('.slider-items');
        this.currentSlide = 0;
        this.childElementCount = this.element.childElementCount;
        this.width = element.clientWidth;
    }

    listen() {
        this.element.style.width = `${this.width * this.childElementCount}px`;
        this.element
            .querySelectorAll('.slider-item')
            .forEach(childElement => {
                childElement.style.width = `${this.width}px`;
            });
        this.createPrevButton();
        this.createNextButton();
        this.createDots();
    }

    createPrevButton() {
        const button = this.createButton('prev', 'controller-prev');
        button.addEventListener('click', e => {
            if (this.currentSlide < 1) {
                this.currentSlide = this.childElementCount - 1;
            } else {
                this.currentSlide--;
            }
            this.goTo(this.currentSlide);
        });
    }

    createNextButton() {
        const button = this.createButton('next', 'controller-next');
        button.addEventListener('click', e => {
            if (this.currentSlide >= (this.childElementCount - 1)) {
                this.currentSlide = 0;
            } else {
                this.currentSlide++;
            }
            this.goTo(this.currentSlide);
        });
    }

    createDots() {
        const dotsWrapper = document.createElement('div');
        dotsWrapper.classList.add('slider-dots');
        for(let i = 0; i < this.childElementCount; i++) {
            const dot = document.createElement('button');
            dot.classList.add('slider-dot');
            if (i === 0) {
                dot.classList.add('active');
            }
            dot.setAttribute('data-index', i);
            dot.innerText = i;
            dot.addEventListener('click', e => {
                this.currentSlide = i;
                this.goTo(i);
            });
            dotsWrapper.append(dot);
        }
        this.wrapper.append(dotsWrapper);
    }

    goTo(currentIndex) {
        this.element.style.transform = `translate(-${this.width * currentIndex}px)`;
        this.wrapper
            .querySelectorAll('.slider-dot')
            .forEach(dot => {
                if (currentIndex.toString() === dot.getAttribute('data-index')) {
                    console.log(dot.getAttribute('data-index'));
                    dot.classList.add('active');
                } else {
                    dot.classList.remove('active');
                }
            });
    }

    createButton(label, classname) {
        let button = this.wrapper.querySelector(`.${classname}`);
        if (button) {
            return button;
        }
        button = document.createElement('button');
        button.classList.add('slider-controller');
        button.classList.add(classname);
        button.innerText = label;
        this.wrapper.append(button);
        return button;
    }
}

