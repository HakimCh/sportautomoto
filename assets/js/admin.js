import SelectPicker from 'ch-select-picker';
import Autocomplete from "./Autocomplete";
import filestyle from 'bootstrap-filestyle2';
import liveUpload from './uploader';
import config from './config';
import Commons from './Commons';

$('.liveupload').liveUpload({
    responsive: true,
    namespace: 'auto',
    baseUrl: `${config.baseURL}/api/image/`,
    tmpUrl: `${config.baseURL}/images/tmp/`,
    imgUrl: `${config.baseURL}/images/`,
    afterUpload: () => {
        $(this).removeClass('error');
        $(this).parent().find('label.error').remove();
    }
});

SelectPicker.listenAll('select');

Commons.toggle('.toggle-action');

Autocomplete.listen(
    'classifiedModel',
    `${config.baseURL}/api/brand/autocomplete/:text`
);

Autocomplete.listen(
    'customerCategory',
    `${config.baseURL}/api/category/autocomplete/:text`
);

document
    .querySelectorAll('.dropdown')
    .forEach(element => {
        element.addEventListener('mouseenter', () => {
            element.classList.add('dropdown-open');
        });
        element.addEventListener('mouseleave', () => {
            element.classList.remove('dropdown-open');
        });
    });
