export default class Slider {
    /**
     *
     * @param selector
     * @param config
     */
    listen(selector, config = {}) {
        document
            .querySelectorAll(selector)
            .forEach(element => {
                const clientWidth = element.clientWidth;
                const slides = element.querySelectorAll('img');
                element.style.width = clientWidth * slides.length + "px";
                element.setAttribute('data-current-slide', '0');
                slides.forEach(img => {
                    const parent = img.parentElement;
                    img.style.maxHeight = config.maxHeight;
                    parent.style.width = `${clientWidth}px`;
                    if (parent.getAttribute('data-index') !== element.getAttribute('data-current-slide')) {
                        parent.style.display = 'none';
                    }
                });
                const sliderTrack = document.createElement('div');
                sliderTrack.classList.add('slider-track');
                sliderTrack.innerHTML = element.innerHTML;
                element.innerHTML = '';
                element.append(sliderTrack);
                Slider.createControllers(element, clientWidth, slides.length);
            });
    }

    static createControllers(element, clientWidth, count) {
        const prevController = Slider.createController('Previous', 'prev', element, clientWidth, count);
        const nextController = Slider.createController('Next', 'next', element, clientWidth, count);
        nextController.style.right = `${clientWidth}px`;
        element.append(prevController);
        element.append(nextController);
    }

    static createController(label, direction, element, clientWidth, count) {
        const controller = document.createElement('button');
        controller.innerHTML = label;
        controller.setAttribute('type', 'button');
        controller.setAttribute('data-role', 'none');
        controller.setAttribute('data-label', label);
        controller.classList.add('slider-controller');
        controller.classList.add(`slider-${direction}`);
        controller.addEventListener('click', e => {
            const currentSlide = parseInt(element.getAttribute('data-current-slide'));
            const parentElement = controller.parentElement;
            parentElement.querySelector(`[data-index="${currentSlide}"]`).style.display = 'none';
            if (direction === 'prev' && currentSlide > 0) {
                element.setAttribute('data-current-slide', currentSlide - 1);
                element.style.transform = `translate3d(-${clientWidth * (currentSlide - 1)}px, 0px, 0px)`;
                parentElement.querySelector(`[data-index="${currentSlide - 1}"]`).style.display = 'block';
            } else if (direction === 'next' && currentSlide < count - 1) {
                element.setAttribute('data-current-slide', currentSlide + 1);
                element.style.transform = `translate3d(-${clientWidth * (currentSlide + 1)}px, 0px, 0px)`;
                parentElement.querySelector(`[data-index="${currentSlide + 1}"]`).style.display = 'block';
            }
        });
        return controller;
    }
}
