import axios from 'axios';
import config  from '../config';
import UploaderView from "./UploaderView";

export default class UploaderListener {
    static chooseFile(element) {
        const button = element.parentNode.querySelector('button');
        button.addEventListener('click', e => {
           e.preventDefault();
           const event = new MouseEvent('click');
           element.dispatchEvent(event);
        });
        element.addEventListener('change', e => {
            const data = new FormData();
            const name = element.getAttribute('name');
            const target = element.getAttribute('data-target');
            data.append(name, element.files[0]);
            config.client
                .post(`/api/medias/${target}/${name}`, data)
                .then(response => {
                    const { data } = response.data;
                    element.setAttribute('data-value', data);
                    UploaderView.preview(element);
                    button.innerText = 'Selectioné';
                    button.classList.add('btn-success');
                })
                .catch(error => {
                    console.error(error);
                });
        });
    }
    static edit() {
        button.addEventListener('click', e => {
            e.preventDefault();
            alert('Edit');
        });
    }
    static rotate() {
        button.addEventListener('click', e => {
            e.preventDefault();
            alert('Rotate');
        });
    }
    static delete() {
        button.addEventListener('click', e => {
            e.preventDefault();
            alert('Delete');
        });
    }
}
