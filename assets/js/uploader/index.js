import UploaderView from './UploaderView';
import UploaderListener from "./UploaderListener";

$.fn.liveUpload = function(options) {
    options = $.extend({
        responsive: false,
        deleteClass: 'delPhoto',
        editionClass: 'editPhoto',
        btnStartClass: 'btn-complete',
        btnDownloadingClass: 'btn-warning',
        btnFinishedClass: 'btn-success',
        afterUpload: $.noop,
        errorAlert: $.noop,
        baseUrl: false,
        tmpUrl: false,
        imgUrl: false,
        useBaseUrl: false,
        namespace: false,
        deleteConfirmMsg: 'Êtes-vous sûr de vouloir supprimer cette image ?',
        txtDownloading: 'Téléchargement en cours...',
        txtFinished: 'Changer',
        txtError: 'Réessayer',
        txtStart: 'Parcourir',
        edition: false
    }, options);

    this.each(function() {
        const element = UploaderView.build(this);
        UploaderListener.chooseFile(element);
    });
};
