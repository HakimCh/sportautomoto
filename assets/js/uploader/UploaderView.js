const TEMPLATE = '<div class="lu-container">\n' +
    '        <i class="ion ion-ios-image"></i>\n' +
    '        <div class="lu-button">\n' +
    '            <button class="btn-complete">Parcourir</button>\n' +
    '        </div>\n' +
    '        <div class="action-btn">\n' +
    '        <a href="#" data-id="" class="delPhoto action-btn" data-tooltip="tooltip" title="Supprimer l\'image">\n' +
    '            <i class="ion-close"></i>\n' +
    '        </a>\n' +
    '        <a href="#" class="rotatePhoto action-btn" data-tooltip="tooltip" title="Rotation horaire de 90°">\n' +
    '            <i class="ion-refresh"></i>\n' +
    '        </a>\n' +
    '        <a href="#" class="editPhoto action-btn" data-tooltip="tooltip" title="Recadrer l\'image">\n' +
    '            <i class="ion-android-expand"></i>\n' +
    '        </a>\n' +
    '        </div>\n' +
    '    </div>';

export default class UploaderView {
    static build(element) {
        const wrapper = document.createElement('div');
        element.parentElement.insertBefore(wrapper, element);
        wrapper.classList.add('liveUploads');
        wrapper.innerHTML = TEMPLATE.trim();
        element.hidden = true;
        wrapper.querySelector('.lu-button').appendChild(element);
        UploaderView.preview(element);
        return element;
    }
    static preview(element) {
        const container = element.parentNode.parentNode;
        const image = element.getAttribute('data-value');
        if (image && container) {
            container.querySelector('i').hidden = true;
            container.querySelector('.action-btn').hidden = false;
            container.setAttribute('style', `background-image: url(${image})`);
        } else {
            container.querySelector('i').hidden = false;
            container.querySelector('.action-btn').hidden = true;
            container.removeAttribute('style');
        }
    }
}
