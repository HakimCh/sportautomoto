import axios from 'axios';
import config from '../config';

const client = axios.create({
   baseURL: config.baseUrl,
});

export default class Uploader {
    async upload(files) {
        return client.post(
            'api/v2/image',
            files,
        );
    }
}
