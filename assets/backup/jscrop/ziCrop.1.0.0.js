$(document).ready(function() {

  // Define container
  var container = $('#image');
  var contWidth = container.width();
  var contHeight = container.height();

  // Define image
  var image = $('img', container);
  var originalWidth = image.width();
  var originalHeight = contHeight;

  image.css({
    height: '100%'
  });

  // Define ranges
  var zoom = createZoomRange(image);
  var horizontal = createRange('horizontal', 'Horizontal');
  var vertical = createRange('vertical', 'Vertical');

  updateRanges();

  horizontal.on('change', function(e) {
    image.animate({
      left: $(this).val() + 'px'
    }, 200);
  });

  vertical.on('change', function(e) {
    image.animate({
      top: $(this).val() + 'px'
    }, 200);
  });

  zoom.on('change', function(e) {
    var value = $(this).val();
    image.animate({
      width: value + 'px'
    }, 100, function() {
      $(this).height('auto');
      updateRanges(value, $(this).height());
    });
  });
  
  $('#cropIt').on('click', function(e) {
  	e.preventDefault();
    var ratio = 2 - (contWidth / originalWidth);
    $.ajax({
      url: './image.php',
      type: 'POST',
      dataType: 'json',
      data: { r: image.width() / originalWidth, w: contWidth, h: contHeight, x: Math.abs(horizontal.val()) * ratio, y: Math.abs(vertical.val()) * ratio },
    })
    .done(function() {
      $('.btn-green').show('fast');
      $('.btn-green').on('click', function() {
        $(this).hide('fast');
      });
    });
  });

  /****************************************/
  /************* Functions ****************/
  /****************************************/

  function updateRanges(currentWidth, currentHeight) {
    var currentWidth = currentWidth || image.width();
    var currentHeight = $('#image img').height();
    var minHorizontal = -Math.abs(currentWidth - contWidth);
    var minVertical = -Math.abs(currentHeight - contHeight);
    horizontal.attr('min', minHorizontal);
    vertical.attr('min', minVertical);
    if (horizontal.val() > minHorizontal) {
      horizontal.attr('min', minHorizontal);
    }
    if (vertical.val() > minVertical) {
      vertical.attr('min', minVertical);
    }
  }

  function createZoomRange(image) {
    var imageWidth = image.width();
    var format = imageWidth > image.height() ? 'landscape' : 'portrait';
    var settings = {
      'data-view': format,
      'value': imageWidth,
      'min': imageWidth,
      'max': originalWidth
    };
    return createRange('zoom', 'Zoom', settings, '.bottom-ranges');
  }

  function createRange(id, text, attrs, appendTo) {
    appendTo = appendTo || '.top-ranges';
    $('<label>').attr({
      for: id
    }).text(text).appendTo(appendTo);
    attrs = attrs || {
      min: '0',
      max: '0',
      value: '0'
    };
    attrs.id = id;
    var range = $('<input type="range">').attr(attrs);
    range.appendTo(appendTo);
    return range;
  }

});
