"use strict";

(function($)
{
    $.fn.ziCrop = function(options)
    {
        options = $.extend({
            draggableClass: 'dragscroll',
            mask: $.noop,
            image: $.noop
        }, options);

        this.each(function()
        {
            var mask  = $(this);

            initialize(mask);
            var zoom = initZoomRange();

            zoom.on('change', function() {
                var currentWidth = $(this).val();
                var currentRatio = currentWidth / options.image.origin.width;
                options.image.object.width(currentWidth).height('auto');
                options.image.ratio = currentRatio;
                options.image.current.width = currentWidth;
                options.image.current.height = options.image.origin.height * currentRatio;
            });

            mask.on('scroll', function() {
                options.mask.left = $(this).scrollLeft();
                options.mask.top = $(this).scrollTop();
            });

            $('#cropIt').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: './image.php',
                    type: 'POST',
                    dataType: 'json',
                    data: { r: options.image.ratio, w: options.mask.width, h: options.mask.height, x: options.mask.left, y: options.mask.top },
                }).done(function() {
                    $('.btn-green').show('fast');
                    $('.btn-green').on('click', function() {
                        $(this).hide('fast');
                    });
                });
            });

        });

        function initialize(mask)
        {
            mask.addClass(options.draggableClass);
            var image = $('img', mask);

            options.image.ratio = mask.width() / image.width();
            options.image.ratioCrop = 2 - options.image.ratio;
            options.mask = {
                width: mask.width(),
                height: mask.height(),
                top: 0,
                left: 0
            };
            options.image = {
                object: image,
                origin: {
                    width: image.width(),
                    height: image.height()
                }
            }
            if(image.width() > image.height()) {
                options.image.format = 'landscape';
                options.image.current = {
                    width: image.width() * options.image.ratio,
                    height: mask.height()
                }
            }
            else {
                options.image.format = 'portrait';
                options.image.current = {
                    width: mask.width(),
                    height: mask.height() * options.image.ratio
                }
            }

            image.width(options.image.current.width);
            image.height(options.image.current.height);
        }

        function initZoomRange() {
            return $('#zoom').attr({
                'value': options.image.current.width,
                'min': options.image.current.width,
                'max': options.image.origin.width
            });
        }

        return this;
    };

})(jQuery);