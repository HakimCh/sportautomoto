"use strict";

(function($)
{
    $.fn.ziCrop = function(options)
    {
        options = $.extend({
            mask: $.noop,
            image: $.noop
        }, options);

        this.each(function()
        {
            var mask  = $(this);

            initialize(mask);
        });

        function initialize(mask)
        {
            var image = $('img', mask);

            options.image.ratioWidth = mask.width() / image.width();
            options.image.ratioHeight = mask.height() / image.height();
            options.mask.width = mask.width();
            options.mask.height = mask.height();
            options.image = {
                object: image,
                origin: {
                    width: image.width(),
                    height: image.height()
                }
            }
            if(image.width() > mask.width()) {
                options.image.current = {
                    width: image.width() * options.image.ratioWidth,
                    height: mask.height()
                }
            }
            else {
                options.image.current = {
                    width: mask.width(),
                    height: mask.height() * options.image.ratioHeight
                }
            }

            console.log(options);
        }

        function updateRanges(currentWidth, currentHeight) {
            var currentWidth = currentWidth || image.width();
            var currentHeight = $('#image img').height();
            var minHorizontal = -Math.abs(currentWidth - contWidth);
            var minVertical = -Math.abs(currentHeight - contHeight);
            horizontal.attr('min', minHorizontal);
            vertical.attr('min', minVertical);
            if (horizontal.val() > minHorizontal) {
                horizontal.attr('min', minHorizontal);
            }
            if (vertical.val() > minVertical) {
                vertical.attr('min', minVertical);
            }
        }

        function initZoomRange() {
            var image = options.image.current;
            var format = image.width > image.height ? 'landscape' : 'portrait';
            return $('#zoom').attr({
                'data-view': format,
                'value': image.width,
                'min': image.width,
                'max': options.image.origin.width
            });
        }

        return this;
    };

})(jQuery);