<?php
$filename = __DIR__.'/pilote.jpg';
$percent = $_POST['r'];

// Calcul des nouvelles dimensions
list($width, $height) = getimagesize($filename);
$newwidth = $width * $percent;
$newheight = $height * $percent;

// Chargement
$thumb = imagecreatetruecolor($newwidth, $newheight);
$source = imagecreatefromjpeg($filename);

// Redimensionnement
imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

// Affichage
imagejpeg($thumb, __DIR__.'/pilote-resized.jpg', 100);


$im = imagecreatefromjpeg(__DIR__.'/pilote-resized.jpg');
$size = min(imagesx($im), imagesy($im));
$im2 = imagecrop($im, ['x' => $_POST['x'], 'y' => $_POST['y'], 'width' => $_POST['w'], 'height' => $_POST['h']]);
if ($im2 !== FALSE) {
	if(file_exists($file = __DIR__.'/pilote-cropped.jpg')) {
		unlink($file);
	}
    imagejpeg($im2, __DIR__.'/pilote-cropped.jpg', 100);
	imagedestroy($thumb);
}

echo json_encode(['response' => 1]);