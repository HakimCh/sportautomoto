tinymce.addI18n('en',{
	'HTML source code': 'HTML code source',
	'Start search': 'Démarrer la recherche',
	'Find next': 'Trouver suivant',
	'Find previous': 'Trouver précédent',
	'Replace': 'Remplacer',
	'Replace all': 'Remplacer tout'
});
