(function($)
{
    var dropMenu = $('.dropMenu');
    var $jq = jQuery();

    $("input", "form").on("focusout keyup", function(e){ e.stopPropagation() });

    $('.boxContent', '#metas').each(function() {
        checkedCategories($(this).find('.checkParent input'));
    });

    $('input', '.checkParent').click(function() {
        checkedCategories($(this));
    });

    if($(window).outerWidth() > 767)
    {
        function PopupCenter(url, title, w, h) {
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow
            if (window.focus) {
                newWindow.focus();
            }
        }
        $(document).ready(function($) {
            $('.socials-btns').on('click', 'a', function(){
                PopupCenter($(this).attr('href'), $('h1.title').html(), 400, 600);
                return false;
            });
        });
    }

    /**
     * Category twin
     */

    $('.metaTwin').on('change', 'select', function () {
        liveMetaTwin($(this));
    });
    $('select', '.metaTwin').each(function(){
        liveMetaTwin($(this));
    });

    if($jq.pickadate) {
        $('.datepicker').pickadate({
            min: new Date()
        });
        var minYear = (new Date().getFullYear()) - 6;
        $('.datepickerz').pickadate({
            max: new Date(minYear,12,31),
            selectYears: true,
            selectMonths: true
        });
    }

    if($jq.selectpicker) {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent))
            $('.bselect').selectpicker('mobile');
        else
            $('.bselect').selectpicker();
    }

    if($jq.lightSlider) {
        $('#lightSlider').lightSlider({
            adaptiveHeight:true,
            gallery:true, item:1,
            auto:true, loop:true
        });
    }


    if(winWidth < 750) {
        $('a.dropdown-toggle', 'nav').click(function(e) {
            e.preventDefault();
        });
    }

    var menuToToggle = winWidth < 767 ? '.submenu:not(".submenus")' : '.submenu:not(".submenus-mobile")';

    $("li.dropdownmenu").hover(
        function(){
            $(this).children(menuToToggle).slideDown(200);
        },
        function(){
            $(this).children(menuToToggle).slideUp(200);
        }
    );

    if(dropMenu.length > 0) {
        dropMenu.mouseenter(function() {
            $(this).addClass('open')
        }).mouseleave(function() {
            $(this).removeClass('open')
        });
    }

    /* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('.header__icon').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
    });

    /* Je veux pouvoir masquer le menu si on clique sur le cache */
    $('#site-cache').click(function(e){
        $('body').removeClass('with--sidebar');
    });

})(jQuery);
