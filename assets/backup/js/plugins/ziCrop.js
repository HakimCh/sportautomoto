/**
 * jQuery Plugin: Crop image
 *
 * @author Hakim Chmimo <ab.chmimo@gmail.com>
 * @version 1.1.0
 *
 * Last modification : 15/11/2016
 */

"use strict";

(function($)
{
    $.fn.ziCrop = function(options)
    {
        options = $.extend({
            draggableClass: 'dragscroll',
            mask: $.noop,
            image: $.noop,
            afterUpdate: $.noop
        }, options);

        this.each(function()
        {
            var mask  = $(this);

            var zoom = initialize(mask);

            zoom.on('change', function() {
                var currentWidth = $(this).val();
                var currentRatio = currentWidth / options.image.origin.width;
                options.image.object.width(currentWidth).height('auto');
                options.image.ratio = currentRatio;
                options.image.current.width = currentWidth;
                options.image.current.height = options.image.origin.height * currentRatio;
            });

            mask.on('scroll', function() {
                options.mask.left = $(this).scrollLeft();
                options.mask.top = $(this).scrollTop();
            });

            $('#cropIt').click(function(e) {
                e.preventDefault();
                var modal    = $('.modal-footer', '#editPhoto');
                var cropit   = $('#cropIt');
                var createit = $('#createIt');
                var data = {
                    link: options.image.object.attr('src'),
                    ratio: options.image.ratio,
                    width: options.mask.width,
                    height: options.mask.height,
                    owidth: options.image.origin.width,
                    oheight: options.image.origin.height,
                    left: options.mask.left,
                    top: options.mask.top
                };
                /*$('button', modal).prop('disable', true).fadeOut('fast', function() {
                    $('p', modal).html('Image en cours de traitement...').show();
                });*/
                $.ajax({
                    url: base_url + 'admin/api/images/crop',
                    type: 'POST',
                    dataType: 'json',
                    data: data
                }).done(function() {
                    $('p', modal).fadeOut('fast', function() {
                        $('button', modal).prop('disable', false).fadeIn('fast');
                        cropit.text('Recadré');
                        createit.text('Mettre à jour');
                        createit.on('click', function() {
                            var role = $(this).data('role');
                            $.ajax({
                                url: base_url + 'admin/api/images/update',
                                type: 'POST',
                                dataType: 'json',
                                data: {role: role}
                            }).done(function() {
                                cropit.text('Cadrer la photo');
                                createit.text('Mis à jour!!');
                                afterUpdate(createit, role, mask);
                            });
                        });
                    });
                });
            });

        });

        function afterUpdate(createit, role, mask)
        {
            var id = createit.data('id');
            var url_small_img = $('img', mask).attr('src');
            url_small_img = url_small_img.replace(role + 's/', role + 's/small/') + '?v=' + Date.now();
            $('#' + id).find('img').attr('src', url_small_img);
        }

        function initialize(mask)
        {
            mask.addClass(options.draggableClass);
            var image = $('img', mask);

            var ratio = mask.width() / image.width();

            options.mask = {
                width: mask.width(),
                height: mask.height(),
                top: 0,
                left: 0
            };
            options.image = {
                object: image,
                origin: {
                    width: image.width(),
                    height: image.height()
                }
            };
            if(image.width() > image.height()) {
                options.image.format = 'landscape';
                options.image.current = {
                    width: image.width() * ratio,
                    height: mask.height()
                }
            }
            else {
                options.image.format = 'portrait';
                options.image.current = {
                    width: mask.width(),
                    height: image.height() * ratio
                }
            }

            options.image.ratio = ratio;
            options.image.ratioCrop = 2 - ratio;

            image.width(options.image.current.width);
            image.height(options.image.current.height);

            return initZoomRange(options.image.current.width, options.image.origin.width);
        }

        function initZoomRange(currentWidth, originWidth) {
            return $('#zoom').attr({
                'value': currentWidth,
                'min': currentWidth,
                'max': originWidth
            });
        }

        return this;
    };

})(jQuery);