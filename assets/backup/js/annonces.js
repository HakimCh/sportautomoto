(function($)
{
    var types = $('select#classified_search_t');
    var marks = $('select#classified_search_ma');
    var models = $('select#classified_search_mo');

    function selectTypeListner(markValue)
    {
        var markValue = markValue | false;
        marks.prop('disabled',true).find('option').remove().end().append('<option value="0" selected="selected">Chargement...</option>').selectpicker('refresh');
        $.ajax({
            url: base_url  +"rest/brand/" + types.val(),
            type: 'GET',
            async: true,
            dataType: 'json'
        }).done(function(data){
            marks.find('option').remove().end();
            $.each(data, function(index, val) {
                var option = '<option value="'+index+'"';
                if(markValue == index) {
                    option += ' selected';
                }
                marks.append(option+'>'+val+'</option>');
            });
            if(data.length !== 1) {
                marks.prop('disabled',false).attr('data-live-search',true);
            }
            else {
                models.prop('disabled',true).find('option').remove().end().append('<option value="0" selected="selected">Tous les modèles</option>').selectpicker('refresh');
            }
            marks.selectpicker('refresh');
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        });
    };

    function selectMarksListner(modValue, markValue)
    {
        var modValue = modValue | false;
        var markValue = markValue | marks.val();
        models.prop('disabled',true).find('option').remove().end().append('<option value="0" selected="selected">Chargement...</option>').selectpicker('refresh');
        $.ajax({
            url: base_url+  "rest/model/" + markValue,
            type: 'GET',
            async: true,
            dataType: 'json'
        }).done(function(data){
            models.find('option').remove().end();
            $.each(data, function(index, val) {
                var option = '<option value="'+index+'"';
                if(modValue == index) {
                    option += ' selected';
                }
                models.append(option+'>'+val+'</option>');
            });
            if(data.length !== 1) {
                models.prop('disabled',false).attr('data-live-search',true);
            }
            $('select#marks').val(markValue).selectpicker('refresh');
            models.selectpicker('refresh');
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        });
    }

    $('select#marks, select#models').prop('disabled',true).selectpicker('refresh');

    $('select#classified_search_t').unbind('change').bind('change',function() {
        selectTypeListner();
    });

    marks.unbind('change').bind('change',function() {
        selectMarksListner();
    });

    if(typeof current_marque !== typeof undefined && current_marque.length > 0) {
        selectTypeListner(current_marque);
        selectMarksListner(current_model, current_marque);
    }

})(jQuery);
