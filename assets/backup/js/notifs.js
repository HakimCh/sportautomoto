(function($) {

	$.fn.timeout = function(ms, callback) {
		var self = this;
		setTimeout(function(){ callback.call(self); }, ms);
		return this;
	}

	$.fn.notif = function(options) {

		var settings = {
			html : '<div class="flash flash-alert is-visible {{type}}">\
				<div class="flash_content">\
			  		<ul>{{#messages}}\
			  			<li>{{message}}</li>\
			  		{{/messages}}</ul>\
				</div>\
			</div>',
			delay : 5000
		};

		var options = $.extend(settings, options);

		return this.each(function() {
			var $this 	= $(this);
			var $notifs = $('> .flashholder', this);
			var $notif 	= $(Mustache.render(options.html, options));
			if($notifs.length == 0) {
				$notifs = $('<div class="flashholder" id="flashholder"/>');
				$this.prepend($notifs);
			}
			$notifs.append($notif);
			if(options.delay) {
				setTimeout(function(){
					$notif.trigger('click');
				}, options.delay);
			}
			$notif.click(function(event){
				event.preventDefault();
				$notif.addClass('is-hidden').timeout(500, function() {
					this.remove();
				});
			});
		});
	}

})(jQuery);