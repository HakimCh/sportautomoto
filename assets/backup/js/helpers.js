var winWidth = $(window).outerWidth();

function liveMetaTwin(select)
{
    var parent = select.closest('.metaTwin');
    var value = $(':selected', select).text();
    var twinmeta = $('.twin-meta', parent);
    var twintext = $('.twin-text', parent);
    var responsiveWidth = '100%';
    console.log(value);
    if(value == 'Autre') {
        $('input', twintext).rules("add", "required");
        if(winWidth > 750) {
            responsiveWidth = '33.33333333%';
        }
        twinmeta.animate({
            width: responsiveWidth
        }, 100, function() {
            twintext.fadeIn('fast');
        });
    }
    else {
        $('input', twintext).rules("remove", "required");
        twintext.fadeOut('fast', function() {
            twinmeta.animate({
                width: '100%'
            }, 100);
        });
    }
}

/**
 * Enable/Disable sub-categories
 * by check/uncheck parent categorie
 * @param  jquery object container meta container
 */
function checkedCategories(container) {
    var parent = container.closest('.boxContent');
    if (container.is(':checked')) {
        $('.metas input', parent).prop("disabled", false);
    }
    else {
        $('.metas input', parent)
            .prop("disabled", true)
            .prop("checked", false);
    }
}