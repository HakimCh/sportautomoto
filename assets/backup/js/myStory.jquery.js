"use strict";

(function($)
{
    $.fn.myStory = function(options)
    {
        options = $.extend({
            formulaire: "#formStory",
            responsive: 460,
            specialStep: '',
            currentStep: '',
            debug: false,
            data: {},
            role: 'pilote',
            afterRead: $.noop
        }, options);

        var windowWidth = $(window).outerWidth();
        var addEntry 	= $(this);
        var formulaire 	= $(options.formulaire);
        var entryType 	= options.role;
        var fields		= $('.bselect, input', formulaire);

        // Load saved entries
        if(options.specialStep == options.currentStep && typeof options.data == 'object' && options.data != null && Object.keys(options.data).length > 0) {
            beautifyEntries(options.data);
        }

        addEntry.bind('click', function(event) {
            //event.preventDefault();
            var that = $(this);
            var entry = rawEntry();
            if(entry !== false) {
                var data = { data: entry };
                var id = that.attr('rel');
                if(typeof id !== typeof undefined && id !== false) {
                    data.id = id;
                }
                saveEntry(data, that);
            }
        });

        $(document).on('click', '.loadEntry', function() {
            getEntry($(this).attr('rel'));
        });

        $(document).on('click', '.deleteEntry', function(event) {
            event.preventDefault();
            deleteEntry($(this).attr('rel'));
        });

        /***************************************************************************
         *                        Private functions
         ***************************************************************************/

        /**
         * Save entry to session
         * @return void
         */
        function saveEntry(data, that)
        {
            $.ajax({
                url: base_url + 'api/history/' + entryType + '/storie',
                type: 'POST',
                async: true,
                dataType: 'json',
                data: data
            }).done(function(response) {
                if(response.data != 0) {
                    beautifyEntries(response.data);
                    fields.css('border-color', '#eee');
                    that.html('<i class="ion-plus-round"></i>  Ajouter').attr('class', 'btn btn-complete palmares-btn-add');
                    debug('Succès: ' + response.status);
                    formulaire[0].reset();
                    that.removeAttr('rel');
                    $('.bselect', formulaire).selectpicker('refresh');
                    $('.twin-text input', formulaire).rules("add", "required");
                    $('.twin-meta', formulaire).animate({ width: '33.33333333%' }, 100, function() {
                        $('.twin-text', formulaire).fadeIn('fast');
                    });
                }
                else {
                    debug('Error: ' + response.status);
                    fields.css('border-color', 'red');
                    that.text('Réssayer').attr('class', 'btn btn-danger palmares-btn-add');
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                debug(errorThrown);
                fields.css('border-color', 'red');
                that.text('Réssayer').attr('class', 'palmares-btn-add');
            });
        }

        /**
         * get an entry
         * @return void
         */
        function getEntry(id)
        {
            $.ajax({
                url: base_url + 'api/history/' + entryType + '/storie/' + id,
                type: 'GET',
                async: true,
                dataType: 'json',
            }).done(function(response) {
                if(response.data != 0) {
                    fields.each(function() {
                        var element = $(this);
                        var key = element.attr('id');
                        if(key in response.data) {
                            element.val(response.data[key]);
                        }
                        addEntry.attr('rel', id)
                            .html('<i class="ion-edit"></i> Editer')
                            .attr('class', 'btn btn-success finish step-btn-finish');
                        element.css('border-color', '#9ad09a');
                        $('.bselect', formulaire).selectpicker('refresh');
                        debug('Succès: ' + response.status);
                    });
                    options.afterRead.call();
                }
                else {
                    debug('Error: ' + response.status);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                debug(errorThrown);
            });
        }

        /**
         * delete an entry
         * @return void
         */
        function deleteEntry(id)
        {
            $.ajax({
                url: base_url + 'api/history/' + entryType + '/storie/' + id,
                type: 'DELETE',
                async: true,
                dataType: 'json'
            }).done(function(response) {
                beautifyEntries(response.data);
                debug('Succès: ' + response.status);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                debug(errorThrown);
            });
        }

        function beautifyEntries(entries)
        {
            var newEntries  = options.role == 'pilote' ? formatPalmares(entries) : formatExperience(entries);
            var templateId  = windowWidth <= options.responsive ? '#mobile-template' : '#desktop-template';
            var source      = $(templateId).html();
            var template    = Handlebars.compile(source);
            $('.story-container', '#experiences').html( template({experiences : newEntries}) );
        }

        /**
         * Beautify palmares
         * @return object
         * @param entries
         */
        function formatExperience(entries)
        {
            var newEntries = [];
            $.each(entries, function(index, entry) {
                if(entry.categoryTitle == 'Autre') {
                    entry.categoryTitle = entry.preciser;
                }
                newEntries[index] = entry;
            });
            return newEntries;
        }

        /**
         * Beautify palmares
         * @return object
         * @param entries
         */
        function formatPalmares(entries)
        {
            var newEntries = [];
            $.each(entries, function(index, entry) {
                if(typeof entry === 'object') {
                    var title = entry.paltitle;
                    if(entry.victory > 0) {
                        title += ' <span class="badge">'+ entry.victory +' victoire(s)</span>';
                    }
                    title = '<h4>' + title + '</h4>';
                    if(entry.vehicule != '') {
                        title += '<p>' + entry.vehicule + '</p>';
                    }
                    entry.title = title;
                    if(entry.categoryTitle == 'Autre') {
                        entry.categoryTitle = entry.preciser;
                    }
                    newEntries[index] = entry;
                }
            });
            return newEntries;
        }

        /**
         * Validate inputs & Build entry object
         * @return mixed
         */
        function rawEntry()
        {
            var entry = {}, error = 0;
            $('input, select', '#palFields').each(function() {
                var element = $(this);
                var eleId = element.attr('id');
                entry[eleId] = element.val();
                if(eleId == 'category') {
                    entry['categoryTitle'] = $(':selected', element).text();
                }
                if(element.valid() === false) error++;
            });
            return error == 0 ? entry : false;
        }

        function debug(variable)
        {
            if(options.debug) {
                console.log(variable);
            }
        }

        return this;
    };

})(jQuery);