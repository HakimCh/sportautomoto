(function($){

    var ajaxCheckDB = function(uri, data)
    {
        data = data || {};
        var valid = false;
        $.ajax({
            url: base_url + uri,
            type: "GET", async: false, data: data,
            success: function(response) {
                valid = response.data != 0;
            }
        });
        return valid;
    };

    $.validator.addMethod("requiredImage", (function(value, element) {
        var uri = "api/images/" + element.getAttribute('data-role') + "/" + element.getAttribute('name');
        return ajaxCheckDB(uri);
    }), function(params, element){
        return "Veuillez uploader votre " + element.getAttribute('data-title');
    });

    $.validator.addMethod("checkExist", (function(value, element) {
        var uri = "api/miscs/check/" + element.getAttribute('data-type') + "/" + element.getAttribute('name');
        return ajaxCheckDB(uri, {field:value, role:element.getAttribute('data-role')});
    }), function(params, element) {
        switch (element.getAttribute('name')) {
            case 'email': return 'Cette adresse email est déja prise'; break;
            case 'username': return "Ce nom d'utilisateur est déja pris"; break;
            default: return "Cette valeur est déja pris"; break;
        }
    });

    $.validator.addMethod('filesize', (function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }), function(params, element){
        return "Le fichier doit être moins de " + max_file_size_megas;
    });

    $.validator.addMethod("checkMdp", function(value, element) {
        var mdp = $('#mdp').val();
        return this.optional(element) || value == mdp;
    }, "Veuillez confirmer votre mot de passe");

    $.validator.addMethod("checkTel", function(value, element) {
        return this.optional(element) || value >= 10;
    }, "Le champ doit être égal ou supérieur à 10");

    $.validator.addMethod("superior", function(value, element) {
        return this.optional(element) || value > 0;
    });

})(jQuery);