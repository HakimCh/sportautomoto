"use strict";

(function($)
{
    $.fn.liveUpload = function(options)
    {
        options = $.extend({
            responsive: false,
            deleteClass: 'delPhoto',
            editionClass: 'editPhoto',
            btnStartClass: 'btn-complete',
            btnDownloadingClass: 'btn-warning',
            btnFinishedClass: 'btn-success',
            afterUpload: $.noop,
            errorAlert: $.noop,
            baseUrl: false,
            tmpUrl: false,
            imgUrl: false,
            useBaseUrl: false,
            namespace: false,
            deleteConfirmMsg: 'Êtes-vous sûr de vouloir supprimer cette image ?',
            txtDownloading: 'Téléchargement en cours...',
            txtFinished: 'Changer',
            txtError: 'Réessayer',
            txtStart: 'Parcourir',
            edition: false
        }, options);

        this.each(function()
        {
            var $object  = $(this);
            var objectName = $object.attr('name');

            buildBlocs($object, objectName);

            $object.on('change', function(){
                uploadImage($object, objectName);
            });

            if(options.responsive === true) {
                liveUploaderResponsive($object);
            }
        });

        $('#editPhoto').on('shown.bs.modal', function (e) {
            var imgId = $(e.relatedTarget).data('img-id');
            var background = $('a#' + imgId).parent().css('background-image');
            background = /^url\((['"]?)(.*)\1\)$/.exec(background);
            background = background ? background[2] : "";
            var imageMask = $('.image-mask', '#editPhoto');
            $('img', imageMask).attr('src', background);
            $('#createIt').attr('data-id', 'mini'+imgId);
            $('.image-mask').ziCrop();
            $('.image-mask').kinetic();
        });

        function buildBlocs(ele, name)
        {
            ele.wrap('<div class="liveUploads"><div class="lu-container"></div></div>');

            var container = ele.parent();
            var delBtn = createDeleteBtn(ele, name);
            var rotBtn = rotateBtn(ele, name);

            container.append(delBtn);
            container.append(rotBtn);

            if(ele.data('editable')) {
                var editBtn = createEditionBtn(ele, name);
                var miniature = createMiniature(ele, name);
                container.append(editBtn);
                container.append(miniature);
            }

            ele.attr({
                id: name,
                class: 'filestyle liveupload',
                'data-input' : false,
                'data-buttonText' : options.txtStart
            });

            var dataValue = hasAttr(ele, 'data-value');
            var dataId = hasAttr(ele, 'data-id');
            var imageurl = options.tmpUrl;

            if(dataValue !== false && dataValue != 0) {
                delBtn.show();
                if(options.useBaseUrl === true && dataId) {
                    imageurl = buildImgUrl(ele);
                }
                container.css({backgroundImage: 'url(' + imageurl + dataValue + ')'});
            }
            else {
                container.prepend('<i class="ion-image">');
                delBtn.hide();
            }
        }

        function uploadImage(ele, name)
        {
            ele.rules("remove", "requiredImage");
            if(ele.valid() === false)
                return;

            ele.rules("add", "requiredImage");
            var $form	 = ele.closest('form');
            var formdata = (window.FormData) ? new FormData($form[0]) : null;
            var data     = (formdata !== null) ? formdata : $form.serialize();
            var submit	 = $('.input-group > span label', $form);
            var parent 	 = ele.parent();

            filestyleTextChanger(ele, options.txtDownloading);

            submit.removeClass(options.btnStartClass)
                .addClass(options.btnDownloadingClass)
                .prop('disabled', true)
                .val(options.txtDownloading);

            $.ajax({
                url: buildUrl(ele, name),
                type: 'POST',
                contentType: false,
                processData: false,
                dataType: 'json',
                async: true,
                data: data,
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            if (percentComplete < 100) {
                                filestyleTextChanger(ele, options.txtDownloading + ' (' + percentComplete + '%)');
                            }
                        }
                    }, false);
                    return xhr;
                }
            })
            .done(function(response)
            {
                submit.addClass(options.btnStartClass)
                    .removeClass(options.btnDownloadingClass)
                    .prop('disabled', false);

                if(response.data != 0)
                {
                    var getnow 	= new Date().getTime();
                    parent.css({backgroundImage: 'url(' + options.tmpUrl + response.data + '?' + getnow + ')'});
                    $('.' + options.deleteClass, parent).show('fast');
                    $('> i', parent).hide();
                    filestyleTextChanger(ele, options.txtFinished);
                    submit.val(options.txtFinished);
                    $('span.badge', parent).remove();
                    options.afterUpload.call(ele);
                }
                else {
                    filestyleTextChanger(ele, options.txtError);
                    createError(ele, response.status);
                }
                console.log(response.status);
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                filestyleTextChanger(ele, options.txtError);
                console.log(errorThrown);
            });
        }

        function rotateBtn(ele, name)
        {
            var btn = $('<a>');
            btn.attr({
                href: '#',
                id: 'rotate'+name,
                class: 'rotatePhoto',
                'data-id': hasAttr(ele, 'data-id'),
                'data-img-id': name,
                'data-tooltip':"tooltip",
                'data-placement':"top",
                'title':"Rotation horaire de 90°"
            });
            btn.append('<i class="ion-refresh"></i>');
            btn.on('click', function(e) {
                e.preventDefault();
                var parent = $(this).parent();
                var background = parent.css('background-image');
                parent.append('<div class="lu-loading"></div>');
                background = /^url\((['"]?)(.*)\1\)$/.exec(background);
                background = background ? background[2] : "";
                $.ajax({
                    url: base_url + 'admin/api/images/rotate',
                    type: 'POST',
                    dataType: 'json',
                    data: {link: background}
                }).done(function(response) {
                   parent.attr('style', 'background-image: url(' + background + '?v=' + $.now() + ')');
                    $('.lu-loading', parent).fadeOut('fast', function() {
                        $(this).remove();
                    });
                });
            });
            return btn;
        }

        function createEditionBtn(ele, name)
        {
            var delBtn = $('<a>');
            var target = '#editPhoto';
            delBtn.attr({
                href: '#',
                id: 'edit'+name,
                class: options.editionClass,
                'data-id': hasAttr(ele, 'data-id'),
                'data-toggle': 'modal',
                'data-target':  target,
                'data-img-id': name,
                'data-tooltip':"tooltip",
                'data-placement':"top",
                'title':"Recadrer l'image"
            });
            delBtn.append('<i class="ion-android-expand"></i>');
            return delBtn;
        }

        function createMiniature(ele, name)
        {
            var miniature = $('<div>');

            miniature.attr({
                'class':'ulphotoMini',
                'id': 'mini' + name
            });

            var image = $('<img>');
            image.attr('src', base_url + 'images/' + ele.data('role') + 's/small/' + ele.data('value'));

            image.load(function() {
                miniature.append(image);
            });

            return miniature;
        }

        function createDeleteBtn(ele, name)
        {
            var delBtn = $('<a>');
            delBtn.attr({
                href: '#',
                id: name,
                'data-id': hasAttr(ele, 'data-id'),
                class: options.deleteClass,
                'data-tooltip':"tooltip",
                'data-placement':"top",
                'title':"Supprimer l'image"
            });
            delBtn.append('<i class="ion-close"></i>');
            delBtn.on('click', function() {
                var that = $(this);
                var parent = that.parent();
                if(confirm(options.deleteConfirmMsg)) {
                    var photoId = hasAttr(ele, 'data-id');
                    if(photoId !== false) {
                        name = photoId;
                    }
                    $.ajax({
                        url: buildUrl(ele, name),
                        type: 'DELETE',
                        async: true
                    }).done(function(response) {
                        if(response.data != 0) {
                            that.hide('fast');
                            filestyleTextChanger($(':file', parent));
                            $('> i', parent).show();
                            parent.removeAttr('style');
                        }
                        console.log(response.status);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    });
                }
            });
            return delBtn;
        }

        function createError(ele, message)
        {
            var name  = ele.attr('name');
            var id    = name + '-error';
            var label = $('label#'+id);
            if(label.length > 0) {
                label.html(message);
            } else {
                label = $('<label id="' + id + '" class="error" for="' + name + '">' + message + '</label>');
            }
            var padBtm = ( ( ele.closest('.lu-container').height() - label.height() ) / 2 ) + 50;
            var padTop = padBtm - 75;
            label.css('padding', padTop + 'px 0 ' + padBtm + 'px 0').insertAfter(ele);
        }

        function filestyleTextChanger(ele, text)
        {
            text = text || options.txtStart;
            ele.hide('fast');
            ele.filestyle('destroy');
            ele.filestyle({input: false, buttonText: text, icon:false});
            ele.show('fast');
        }

        function buildImgUrl(ele)
        {
            var role = hasAttr(ele, 'data-role');
            if(role) {
                return options.imgUrl + role + 's/';
            }
            return false;
        }

        function buildUrl(ele, name)
        {
            var liveurl = options.baseUrl + name;
            if(options.namespace == 'auto') {
                var role = hasAttr(ele, 'data-role');
                if(role) {
                    liveurl = options.baseUrl + role + '/' + name;
                }
            }
            return liveurl;
        }

        function hasAttr(ele, attrName)
        {
            var attr = ele.attr(attrName);
            if(typeof attr !== typeof undefined && attr !== false) {
                return attr;
            }
            return false;
        }

        function liveUploaderResponsive(wrapper)
        {
            var container = $('.lu-container', wrapper);
            container.height(container.width());
        }

        return this;
    };

})(jQuery);