"use strict";

(function($)
{
	$.fn.stepByStep = function(options)
	{
		options = $.extend({
			elemTag: 'section',
			titleId: false,
			btnTag: 'a',
			txtNext: 'Suivant <i class="ion-arrow-right-c"></i>',
			txtPrev: '<i class="ion-arrow-left-c"></i> Précedant',
			txtFinish: '<i class="ion-checkmark-round"></i> Finir',
			currentStep: 0,
			debug: false,
			beforeChange: $.noop,
			afterChange: $.noop,
			onFinish: $.noop
		}, options);

		var title, step, steps;

		this.each(function()
		{
			var container  = $(this);

			initialize(container);

			$('.prev, .next', '.sbs-navigation').click(function(event) {
				//event.preventDefault();
				var that = $(this);
				if(that.hasClass('next') !== false) {
					var errors = actionsChange(this, that);
					if(errors == 0)
						options.afterChange.call(this);
					else
						return;
				}
				var section = $('#' + that.attr('rel'));
				that.closest('section').slideUp('fast');
				showSection(section);
			});

			$('#onFinish', '.sbs-navigation').click(function(event) {
				event.preventDefault();
				var errors = actionsChange(this);
					if(errors == 0)
						options.onFinish.call(this);
					else
						return;
			});
		});

		function actionsChange(object, that)
		{
			var errors = 0;
			that = that || $(object);
			var validation = that.closest('section').attr('novalidate');
			if(typeof validation === typeof undefined) {
				errors = options.beforeChange.call(object);
			}
			return errors;
		}

		function initialize(container)
		{
			steps = getSteps(container);
			title = createTitleTag(container);
			step  = options.currentStep.replace('step', '');

			container.addClass('stepByStep');
			$(options.elemTag, container).each(function(index) {
				var section = $(this);
				section.attr('id', 'step' + index);
				if(index == step) {
					showSection(section);
				}
				section.wrapInner('<div class="sbs-wrapper"></div>')
				createNavigation(section, index);
			});
		}

		function showSection(section)
		{
			section.slideDown('fast');
			title.html(section.attr('data-title'));
		}

		function createNavigation(section, index)
		{
			var navContainer = $('<div>', {class: 'sbs-navigation'});
			if(index > 0) {
				var prevStep = index - 1;
				$('<a>', {href: '#wrapper', class: 'prev step-btn-prev', rel: 'step' + prevStep}).html(options.txtPrev).appendTo(navContainer);
			}
			var stepsLength = steps.length - 1;
			if(index == stepsLength) {
				$('<a>', {href: '#wrapper', id: 'onFinish', class: 'finish step-btn-finish'}).html(options.txtFinish).appendTo(navContainer);
			}
			else if(index < stepsLength){
				var nextStep = index + 1;
				$('<a>', {href: '#wrapper', class: 'next step-btn-next', rel: 'step' + nextStep}).html(options.txtNext).appendTo(navContainer);
			}
			section.append(navContainer);
		}

		function createTitleTag(element)
		{
			var title;
			if(options.titleId === false){
				title = $('<div>', {id: 'sbsTitle', class: 'sbs-title'});
				title.prependTo(element);
			}
			else {
				title = $('#' + options.titleId).addClass('sbs-title');
			}
			return title;
		}

		function getSteps(container)
		{
			return $(options.elemTag, container).map(function(index, elem) {
				return $(this).attr('data-title');
			});
		}

		function hasAttr(element, attrName)
		{
			var attr = element.attr(attrName);
			return typeof attr !== typeof undefined && attr !== false;
		}

		return this;
	};

})(jQuery);