(function($)
{
    var $jq = jQuery();

    $('.nav-tabs-sticky').stickyTabs();

    $('.boxContent', '#metas').each(function() {
        checkedCategories($(this).find('.checkParent input'));
    });

    $('input', '.checkParent').click(function() {
        checkedCategories($(this));
    });

    $(document).ready(function() {
        $('body').tooltip({
            selector: "[data-tooltip=tooltip]",
            container: "body"
        });
    });

    /**
     * Category twin
     */
    var twinmeta = $('.twin-meta');
    var twintext = $('.twin-text');

    $('#metaTwin').on('change', 'select', function () {
        var value = $(':selected', $(this)).text();
        liveMetaTwin(value);
    });
    $('select', '#metaTwin').each(function(){
        var value = $(':selected', $(this)).text();
        liveMetaTwin(value);
    });

    function liveMetaTwin(value)
    {
        if(value == 'Autre') {
            $('input', twintext).rules("add", "required");
            console.log(1);
            twinmeta.animate({
                width: '33.33333333%'
            }, 100, function() {
                twintext.fadeIn('fast');
            });
        }
        else {
            $('input', twintext).rules("remove", "required");
            twintext.fadeOut('fast', function() {
                twinmeta.animate({
                    width: '100%'
                }, 100);
            });
        }
    }

    /**
     * Enable/Disable sub-categories
     * by check/uncheck parent categorie
     * @param  jquery object container meta container
     */
    function checkedCategories(container) {
        var parent = container.closest('.boxContent');
        if (container.is(':checked')) {
            $('.metas input', parent).prop("disabled", false);
        }
        else {
            $('.metas input', parent)
                .prop("disabled", true)
                .prop("checked", false);
        }
    }

    if($jq.filestyle) {
        $(".bfile").filestyle({input: false, buttonText: "Parcourir", icon:false});
    }

    if($jq.selectpicker) {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent))
            $('.bselect').selectpicker('mobile');
        else
            $('.bselect').selectpicker();
    }

    if($jq.liveUpload) {
        $('.liveupload').liveUpload({
            responsive: true,
            useBaseUrl: true,
            enableDelete: false,
            namespace: 'auto',
            baseUrl: base_url + 'api/images/',
            tmpUrl: base_url + 'images/tmp/',
            imgUrl: base_url + 'images/',
            afterUpload: function() {
                var $that = $(this);
                $that.removeClass('error');
                $that.parent().find('label.error').remove();
            }
        });
    }

    if($jq.nestable)
    {
        $('#nestMods').nestable({
            maxDepth: 1,
            listNodeName: 'ul'
        }).on('change', function(e) {
            var list = e.length ? e : $(e.target);
            $.post(base_url + 'admin/api/modules/order', {
                ids: window.JSON.stringify(list.nestable('serialize'))
            }, 'json');
        });
        $('#nestPages').nestable({
            maxDepth: 2,
            listNodeName: 'ul'
        }).on('change', function(e) {
            var list = e.length ? e : $(e.target);
            $.post(base_url + 'admin/api/menus/order', {
                ids: window.JSON.stringify(list.nestable('serialize'))
            }, 'json');
        });
    }

    var sidebarmenu = $("#sidebarmenu");
    sidebarmenu.on('click', '.submenu-toggle', function(e) {
        e.preventDefault();
        var parent = $(this).parent();
        var submenu = $('> ul', parent);
        if(!parent.hasClass('active') && !submenu.hasClass('open')) {
            sidebarmenu.find("li:not('.active') > ul").slideUp('fast');
            submenu.addClass('open').slideDown('fast');
        }
    });

    $(document).on('click','#account',function (){
        var topbarmenu = $('#topbar-menu');
        var icoaccount = $('#ico-account');
        if(topbarmenu.hasClass('open')) {
            topbarmenu.removeClass('open').slideUp('fast');
            icoaccount.attr('class', 'ion-chevron-down');
        } else {
            topbarmenu.addClass('open').slideDown('fast');
            icoaccount.attr('class', 'ion-chevron-up');
        }
    });

    $("#GPWD").on('change', function(){
        var that = $(this);
        var pwd  = $('.pwd > input[type="password"]');
        if(that.is(":checked"))
            pwd.prop('readonly',true).val(that.val());
        else
            pwd.prop('readonly',false).val('');
    });

    function makeItExterne(value)
    {
        if(value == 0) {
            $('.postInterne').show();
            $('.postExterne').hide();
            $('.postExterne input').prop('disabled', false);
        }
        else {
            $('.postExterne').show();
            $('.postInterne').hide();
            $('.postExterne input').prop('disabled', true);
            $('.postExterne input[name="post_url"]').prop('disabled', false);
        }
    }

    $("#makeItExterne").on('change', function() {
        makeItExterne($(this).val());
    });

    if (typeof is_external !== 'undefined') {
        makeItExterne(is_external);
    }

})(jQuery);
