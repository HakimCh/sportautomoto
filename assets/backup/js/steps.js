(function($)
{
    var debugStep = false;

    if(typeof currentStep !== typeof undefined)
    {
        /***************************************************************************
         *                        Load saved parametters
         ***************************************************************************/
            // Load current section jquery object
        var stepContainer  = $(currentStep);
        var stepsContainer = $('#steps');
        var stepsRole      = stepsContainer.attr('data-role');

        // Load saved entries
        if(currentStep != specialStep && currentStepDatas.length > 0) {
            populateStep(currentStepDatas);
        }

        /***************************************************************************
         *                        Listning events
         ***************************************************************************/

        stepsContainer.stepByStep({
            titleId: 'title',
            currentStep: currentStep,
            beforeChange: function() {
                return validateSection($(this));
            },
            afterChange: function() {
                return saveStep($(this));
            },
            onFinish: function(){
                return saveSteps($(this));
            }
        });

        /***************************************************************************
         *                        Private functions
         ***************************************************************************/
        // Save and send all steps datas
        function saveSteps(that)
        {
            currentStep  = that.closest('section').attr('id');
            var formId = '#form' + currentStep.replace('s', 'S');
            that.text('Enregistrement en cours...');
            $.ajax({
                url: base_url + 'api/steps/' + stepsRole,
                type: 'POST',
                async: true,
                dataType: 'json',
                data: { data: $(formId).serialize() },
            })
                .done(function(response) {
                    if(response.data != 0) {
                        that.text('Enregistré!');
                        debug('Succès: ' + response.data);
                        stepsContainer.slideUp('fast', function() {
                            $('#title').html('Formulaire envoyé!');
                            $('span', '#orderNumber').html(response.data);
                            $('#orderPage').slideDown('fast');
                        });
                    }
                    else {
                        debug(response.status);
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    debug(errorThrown);
                });
        }

        // Save one step
        function saveStep(that)
        {
            currentStep  = that.closest('section').attr('id');
            var formId = '#form' + currentStep.replace('s', 'S');
            //debug(tinymce.get('content').getContent().serialize());
            debug($(formId).serialize());
            $.ajax({
                url: base_url + 'api/steps/' + stepsRole + '/step/' + currentStep,
                type: 'POST',
                async: true,
                dataType: 'json',
                data: { data: $(formId).serialize() },
            })
                .done(function(response) {
                    debug(response.data);
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    debug(errorThrown);
                });
        }

        // refill step datas
        function populateStep(datas)
        {
            $('input, select', stepContainer).map(function(index, elem) {
                var elementId = elem.attr('id');
                if(elementId in datas) {
                    elem.val(datas[elementId]);
                }
            });
        }

        function hasAttr(element, attrName)
        {
            var attr = element.attr(attrName);
            return typeof attr !== typeof undefined && attr !== false;
        }

        /**
         * Validate section form
         */
        function validateSection(that)
        {
            var errors  = 0;
            var section = that.closest('section');
            $('input, select', section).each(function(index, el) {
                var field = $(el);
                if(typeof field !== typeof undefined) {
                    if(field.valid() === false)
                        errors++;
                }
            });
            return errors;
        }
    }
    else {
        debug('Current step not found');
    }

    function debug(variable)
    {
        if(debugStep) {
            console.log(variable);
        }
    }

})(jQuery);