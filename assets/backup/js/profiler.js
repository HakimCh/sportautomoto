$(function() {
   $('#debug-bar .tab-bar a').click(function(){
      if($(this).attr('id') == 'debugClose') {
         var stat = $('#debug-bar').attr('class');
         if(stat == 'open') {
            $('#debug-bar').removeClass('open');
            $('#debugClose').attr('class','ion-plus');
            $('#footer').attr('class','debug-footer-close');
         } else {
            $('#debug-bar').addClass('open');
            $('#debugClose').attr('class','ion-close');
            $('#footer').attr('class','debug-footer-open');
         }
      }
      else {
         $('#debug-bar').attr('class','open');
         $('#debugClose').attr('class','ion-close');
         $('#footer').attr('class','debug-footer-open');
      }
   });

   $('#debug > a').bind('click', function(){
      $(this).next('ol').show('fast');
   });
});