(function($) {
    var addEntry 	= $("#addEntry");
    var formulaire 	= addEntry.closest('form');
    var entryType 	= $('#steps').attr('data-role');
    var fields		= $('.bselect, input', formulaire);

    // Load saved entries
    if(Object.keys(currentStepDatas).length > 0) {
        beautifyEntries(currentStepDatas);
    }

    addEntry.bind('click', function(event) {
        event.preventDefault();
        var that = $(this);
        var entry = rawEntry();
        if(entry !== false) {
            var data = { data: entry };
            var id = that.attr('rel');
            if(typeof id !== typeof undefined && id !== false) {
                data.id = id;
            }
            saveEntry(data, that);
        }
    });

    $(document).on('click', '.loadEntry', function(event) {
        event.preventDefault();
        getEntry($(this).attr('rel'));
    });

    $(document).on('click', '.deleteEntry', function(event) {
        event.preventDefault();
        deleteEntry($(this).attr('rel'));
    });


    /***************************************************************************
     *                        Private functions
     ***************************************************************************/

    /**
     * Save entry to session
     * @return void
     */
    function saveEntry(data, that)
    {
        $.ajax({
            url: base_url + 'api/history/' + entryType + '/storie',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: data
        })
            .done(function(response) {
                if(response.data != 0) {
                    beautifyEntries(response.data);
                    fields.css('border-color', '#eee');
                    that.text('Ajouter').attr('class', 'palmares-btn-add');
                    console.log('Succès: ' + response.status);
                    formulaire[0].reset();
                }
                else {
                    console.log('Error: ' + response.status);
                    fields.css('border-color', 'red');
                    that.text('Réssayer').attr('class', 'palmares-btn-add');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                fields.css('border-color', 'red');
                that.text('Réssayer').attr('class', 'palmares-btn-add');
            });
    }

    /**
     * get an entry
     * @return void
     */
    function getEntry(id)
    {
        $.ajax({
            url: base_url + 'api/history/' + entryType + '/storie/' + id,
            type: 'GET',
            async: true,
            dataType: 'json',
        })
            .done(function(response) {
                if(response.data != 0) {
                    fields.each(function() {
                        var element = $(this);
                        var key = element.attr('id');
                        if(key in response.data) {
                            element.val(response.data[key]);
                        }
                        addEntry.attr('rel', id)
                            .text('Editer')
                            .attr('class', 'finish step-btn-finish');
                        element.css('border-color', '#9ad09a');
                        $('.bselect', formulaire).selectpicker('refresh');
                        console.log('Succès: ' + response.status);
                    });
                }
                else {
                    console.log('Error: ' + response.status);
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            });
    }

    /**
     * delete an entry
     * @return void
     */
    function deleteEntry(id)
    {
        $.ajax({
            url: base_url + 'api/history/' + entryType + '/storie/' + id,
            type: 'DELETE',
            async: true,
            dataType: 'json',
        })
            .done(function(response) {
                beautifyEntries(response.data);
                console.log('Succès: ' + response.status);
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            });
    }

    function beautifyEntries(entries)
    {
        if(entryType == 'pilote')
            formatPalmares(entries);
        else
            formatExperience(entries);
    }

    /**
     * Beautify palmares
     * @param  object entries
     * @return object
     */
    function formatExperience(entries)
    {
        var source = '{{#experiences}}<tr>' +
            '<td class="annee">{{year}}</td>' +
            '<td class="meta">{{categoryTitle}}</td>' +
            '<td class="titre">{{{paltitle}}}</td>' +
            '<td class="palmActions"><a href="#" rel="{{@index}}" class="loadEntry btn btn-success">Editer</a>' +
            '<a href="#" rel="{{@index}}" class=" deleteEntry btn btn-danger">Supprimer</a></td>' +
            '</tr>{{/experiences}}';
        var template = Handlebars.compile(source);
        $('table > tbody', '#experiences').html( template({experiences : entries}) );
    }

    /**
     * Beautify palmares
     * @param  object entries
     * @return object
     */
    function formatPalmares(entries)
    {
        var newEntries = [];
        console.log(entries);
        $.each(entries, function(index, entry) {
            var title = entry.paltitle;
            if(entry.victory > 0) {
                title += ' <span class="badge">'+ entry.victory +' victoire(s)</span>';
            }
            title = '<h4>' + title + '</h4>';
            if(entry.vehicule != '') {
                title += '<p>' + entry.vehicule + '</p>';
            }
            entry.title = title;
            if(entry.categoryTitle == 'Autre') {
                entry.categoryTitle = entry.preciser;
            }
            newEntries[index] = entry;
        });
        var source = '{{#palmares}}<tr>' +
            '<td class="annee">{{year}}</td>' +
            '<td class="meta">{{categoryTitle}}</td>' +
            '<td class="titre">{{{title}}}</td>' +
            '<td class="place {{result}}">{{result}}</td>' +
            '<td class="palmActions"><a href="#" rel="{{@index}}" class="loadEntry btn btn-success">Editer</a>' +
            '<a href="#" rel="{{@index}}" class=" deleteEntry btn btn-danger">Supprimer</a></td>' +
            '</tr>{{/palmares}}';
        var template = Handlebars.compile(source);
        $('table > tbody', '#palmares').html( template({palmares : newEntries}) );
    }

    /**
     * Validate inputs & Build entry object
     * @return mixed
     */
    function rawEntry()
    {
        var entry = {}, error = 0;
        $('input, select', '#palFields').each(function() {
            var element = $(this);
            var eleId = element.attr('id');
            entry[eleId] = element.val();
            if(eleId == 'category') {
                entry['categoryTitle'] = $(':selected', element).text();
            }
            if(element.valid() === false) error++;
        });
        return error == 0 ? entry : false;
    }
})(jQuery);