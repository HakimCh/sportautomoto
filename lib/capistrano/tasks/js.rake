namespace :js do

    task :yarn do
        on roles(:web) do
            within release_path do
                execute :yarn, :install
                execute :yarn, :build
            end
        end
    end

end
