namespace :php do

    task :composer do
        on roles(:web) do
            within release_path do
                execute :composer, :install
                execute :composer, :update
            end
        end
    end

end
